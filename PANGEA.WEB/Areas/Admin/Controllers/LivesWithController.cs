﻿using AutoMapper;
using Dapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PANGEA.DAL;
using PANGEA.DAL.Models;
using PANGEA.DAL.ViewModels;
using PANGEA.SERVICES;
using PANGEA.SERVICES.Email;
using PANGEA.SERVICES.Extensions;
using PANGEA.SERVICES.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace PANGEA.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class LivesWithController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<UserRole> _roleManager;
        private readonly IEmailSenderEnhance _emailSender;
        public GenericService _genericService;
        public IUserService _userService;
        private readonly ILogService _logService;
        private readonly IConfiguration _config;

        public LivesWithController(
             ApplicationDbContext context, IMapper mapper,
             UserManager<User> userManager,
             SignInManager<User> signInManager,
             RoleManager<UserRole> roleManager,
             IEmailSenderEnhance emailSender,
             GenericService genericService,
             IUserService userService,
             ILogService logService,
             IConfiguration configuration
             )
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _emailSender = emailSender;
            _genericService = genericService;
            _userService = userService;
            _logService = logService;
            _config = configuration;
        }



        [HttpGet]
        public async Task<ActionResult> Index(int? page, ListLookUpsVm model)
        {
            try
            {
                var table = $"LivesWith";
                var pageSize = int.Parse(_config.GetSection("PagingSettings:PageSize").Value);
                var pageNum = page ?? 1;
                var storedProcedure = "Code.usp_GetLookUps";
                using IDbConnection db = new SqlConnection(DBService.DBConnection());
                var data = await db.QueryMultipleAsync(storedProcedure,
                    new { UserId = User.GetUserId(), Table = table },
                    commandType: CommandType.StoredProcedure);

                var filteredLivesWithList = data.Read<LivesWithLookUpVm>().ToPagedList();

                #region Search/Filter
                if (model.IncludeRemoved)
                {
                    filteredLivesWithList = filteredLivesWithList.Where(a => a.Active == true || a.Active == false).ToPagedList();
                }
                else
                {
                    filteredLivesWithList = filteredLivesWithList.Where(a => a.Active == true).ToPagedList();
                }

                if (!string.IsNullOrEmpty(model.Search))
                {
                    filteredLivesWithList = filteredLivesWithList.Where(a => a.Lives_With.Contains(model.Search)).ToPagedList();
                }
                #endregion

                model.Table = table;
                model.LivesWithList = filteredLivesWithList.OrderBy(r => r.Lives_With).ToPagedList(pageNum, pageSize);

            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Create()
        {
            var model = new LivesWithLookUpVm();

            try
            {
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error creating lives with - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(LivesWithLookUpVm model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var spName = "Code.usp_New_Add_Lives_With";
                    var inputParameters = new
                    {
                        User_ID = Guid.NewGuid(),
                        Lives_With = model.Lives_With,
                        Description = model.Description,
                        Public_Description = model.Public_Description,
                        Min_Age = model.Min_Age,
                        // @Chore_Code_ID = null,
                        Active = model.Active,
                    };

                    var result = _genericService.Query<SpIntVm>(spName, inputParameters);
                    if (result.Code == 200)
                    {
                        TempData["MESSAGE"] = "The lives with was added successfully.";
                        TempData["KEY"] = "success";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["MESSAGE"] = $"Error adding lives with - {result.Msg}";
                        TempData["KEY"] = "danger";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error adding lives with - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            try
            {
                var spName = "Code.usp_GetLookUpById";
                var inputParameters = new { Id = id, @Table = $"LivesWith" };
                var model = (_genericService.Query<LivesWithLookUpVm>(spName, inputParameters).Data).FirstOrDefault();
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error loading lives with - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(LivesWithLookUpVm model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var spName = "Code.usp_New_Add_Lives_With";
                    var inputParameters = new
                    {
                        User_ID = Guid.NewGuid(),
                        Lives_With = model.Lives_With,
                        Lives_With_Code_ID = model.Lives_With_Code_ID,
                        Description = model.Description,
                        Public_Description = model.Public_Description,
                        Min_Age = model.Min_Age,
                        Active = model.Active,
                    };

                    var result = _genericService.Query<SpIntVm>(spName, inputParameters);
                    if (result.Code == 200)
                    {
                        TempData["MESSAGE"] = "The lives with was updated successfully.";
                        TempData["KEY"] = "success";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["MESSAGE"] = $"Error updating lives with - {result.Msg}";
                        TempData["KEY"] = "danger";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error updating lives with - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Details(int id)
        {
            try
            {
                var spName = "Code.usp_GetLookUpById";
                var inputParameters = new { Id = id, @Table = $"LivesWith" };
                var model = (_genericService.Query<LivesWithLookUpVm>(spName, inputParameters).Data).FirstOrDefault();

                ViewBag.Title = $"Lives With Details";
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error loading lives with - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }
        }


        [HttpGet]
        public async Task<ActionResult> Remove(int id)
        {
            try
            {
                var spName = "Code.usp_GetLookUpById";
                var inputParameters = new { Id = id, @Table = $"LivesWith" };
                var model = (_genericService.Query<LivesWithLookUpVm>(spName, inputParameters).Data).FirstOrDefault();

                ViewBag.Title = $"Remove Lives With";
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error loading lives with - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var _spName = "Code.usp_GetLookUpById";
                    var _inputParameters = new { Id = id, @Table = $"LivesWith" };
                    var model = (_genericService.Query<LivesWithLookUpVm>(_spName, _inputParameters).Data).FirstOrDefault();

                    var spName = "Code.usp_New_Add_Lives_With";
                    var inputParameters = new
                    {
                        User_ID = Guid.NewGuid(),
                        Lives_With = model.Lives_With,
                        Lives_With_Code_ID = model.Lives_With_Code_ID,
                        Description = model.Description,
                        Public_Description = model.Public_Description,
                        Min_Age = model.Min_Age,
                        Active = false,
                    };

                    var result = _genericService.Query<SpIntVm>(spName, inputParameters);
                    if (result.Code == 200)
                    {
                        TempData["MESSAGE"] = "The lives with was removed successfully.";
                        TempData["KEY"] = "success";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["MESSAGE"] = $"Error removing lives with - {result.Msg}";
                        TempData["KEY"] = "danger";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error removing lives with - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }
    }
}
