﻿using AutoMapper;
using Dapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PANGEA.DAL;
using PANGEA.DAL.Models;
using PANGEA.DAL.ViewModels;
using PANGEA.SERVICES;
using PANGEA.SERVICES.Email;
using PANGEA.SERVICES.Extensions;
using PANGEA.SERVICES.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace PANGEA.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class DashboardController : Controller
    {
        public GenericService GenericService;
        private readonly ILogService LogService;
        private readonly IConfiguration _config;

        public DashboardController(
            GenericService genericService
            , ILogService logService
            , IConfiguration configuration
            )
        {
            GenericService = genericService;
            LogService = logService;
            _config = configuration;
        }
        public async Task<IActionResult> Index()
        {
            try
            {
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error loading the dashboard - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return View();
        }
    }
}
