﻿using AutoMapper;
using Dapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PANGEA.DAL;
using PANGEA.DAL.Models;
using PANGEA.DAL.ViewModels;
using PANGEA.SERVICES;
using PANGEA.SERVICES.Email;
using PANGEA.SERVICES.Extensions;
using PANGEA.SERVICES.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace PANGEA.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class PersonalityTypeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<UserRole> _roleManager;
        private readonly IEmailSenderEnhance _emailSender;
        public GenericService _genericService;
        public IUserService _userService;
        private readonly ILogService _logService;
        private readonly IConfiguration _config;

        public PersonalityTypeController(
             ApplicationDbContext context, IMapper mapper,
             UserManager<User> userManager,
             SignInManager<User> signInManager,
             RoleManager<UserRole> roleManager,
             IEmailSenderEnhance emailSender,
             GenericService genericService,
             IUserService userService,
             ILogService logService,
             IConfiguration configuration
             )
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _emailSender = emailSender;
            _genericService = genericService;
            _userService = userService;
            _logService = logService;
            _config = configuration;
        }



        [HttpGet]
        public async Task<ActionResult> Index(int? page, ListLookUpsVm model)
        {
            try
            {
                var table = $"Personality";
                var pageSize = int.Parse(_config.GetSection("PagingSettings:PageSize").Value);
                var pageNum = page ?? 1;
                var storedProcedure = "Code.usp_GetLookUps";
                using IDbConnection db = new SqlConnection(DBService.DBConnection());
                var data = await db.QueryMultipleAsync(storedProcedure,
                    new { UserId = User.GetUserId(), Table = table },
                    commandType: CommandType.StoredProcedure);

                var filteredPersonalityList = data.Read<PersonalityTypeLookUpVm>().ToPagedList();

                #region Search/Filter
                if (model.IncludeRemoved)
                {
                    filteredPersonalityList = filteredPersonalityList.Where(a => a.Active == true || a.Active == false).ToPagedList();
                }
                else
                {
                    filteredPersonalityList = filteredPersonalityList.Where(a => a.Active == true).ToPagedList();
                }

                if (!string.IsNullOrEmpty(model.Search))
                {
                    filteredPersonalityList = filteredPersonalityList.Where(a => a.Personality_Type.Contains(model.Search)).ToPagedList();
                }
                #endregion

                model.Table = table;
                model.PersonalityTypeList = filteredPersonalityList.OrderBy(r => r.Personality_Type).ToPagedList(pageNum, pageSize);

            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Create()
        {
            var model = new PersonalityTypeLookUpVm();

            try
            {
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error creating personality type - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(PersonalityTypeLookUpVm model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var spName = "Code.usp_New_Add_Personality_Type";
                    var inputParameters = new
                    {
                        User_ID = Guid.NewGuid(),
                        Personality_Type = model.Personality_Type,
                        Description = model.Description,
                        Public_Description = model.Public_Description,
                        Min_Age = model.Min_Age,
                        // @Personality_Type_Code_ID = null,
                        Active = model.Active,
                    };

                    var result = _genericService.Query<SpIntVm>(spName, inputParameters);
                    if (result.Code == 200)
                    {
                        TempData["MESSAGE"] = "The personality type was added successfully.";
                        TempData["KEY"] = "success";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["MESSAGE"] = $"Error adding personality type - {result.Msg}";
                        TempData["KEY"] = "danger";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error adding personality type - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            try
            {
                var spName = "Code.usp_GetLookUpById";
                var inputParameters = new { Id = id, @Table = $"Personality" };
                var model = (_genericService.Query<PersonalityTypeLookUpVm>(spName, inputParameters).Data).FirstOrDefault();
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error loading personality type - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(PersonalityTypeLookUpVm model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var spName = "Code.usp_New_Add_Personality_Type";
                    var inputParameters = new
                    {
                        User_ID = Guid.NewGuid(),
                        Personality_Type = model.Personality_Type,
                        Personality_Type_Code_ID = model.Personality_Type_Code_ID,
                        Description = model.Description,
                        Public_Description = model.Public_Description,
                        Min_Age = model.Min_Age,
                        // @Personality_Type_Code_ID = null,
                        Active = model.Active,
                    };

                    var result = _genericService.Query<SpIntVm>(spName, inputParameters);
                    if (result.Code == 200)
                    {
                        TempData["MESSAGE"] = "The personality type was updated successfully.";
                        TempData["KEY"] = "success";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["MESSAGE"] = $"Error updating personality type - {result.Msg}";
                        TempData["KEY"] = "danger";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error updating personality type - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Details(int id)
        {
            try
            {
                var spName = "Code.usp_GetLookUpById";
                var inputParameters = new { Id = id, @Table = $"Personality" };
                var model = (_genericService.Query<PersonalityTypeLookUpVm>(spName, inputParameters).Data).FirstOrDefault();

                ViewBag.Title = $"Personality Type Details";
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error loading personality type - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }
        }


        [HttpGet]
        public async Task<ActionResult> Remove(int id)
        {
            try
            {
                var spName = "Code.usp_GetLookUpById";
                var inputParameters = new { Id = id, @Table = $"Personality" };
                var model = (_genericService.Query<PersonalityTypeLookUpVm>(spName, inputParameters).Data).FirstOrDefault();

                ViewBag.Title = $"Remove Personality Type";
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error loading personality type - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var _spName = "Code.usp_GetLookUpById";
                    var _inputParameters = new { Id = id, @Table = $"Personality" };
                    var model = (_genericService.Query<PersonalityTypeLookUpVm>(_spName, _inputParameters).Data).FirstOrDefault();

                    var spName = "Code.usp_New_Add_Personality_Type";
                    var inputParameters = new
                    {
                        User_ID = Guid.NewGuid(),
                        Personality_Type = model.Personality_Type,
                        Personality_Type_Code_ID = model.Personality_Type_Code_ID,
                        Description = model.Description,
                        Public_Description = model.Public_Description,
                        Min_Age = model.Min_Age,
                        Active = false,
                    };

                    var result = _genericService.Query<SpIntVm>(spName, inputParameters);
                    if (result.Code == 200)
                    {
                        TempData["MESSAGE"] = "The personality type was removed successfully.";
                        TempData["KEY"] = "success";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["MESSAGE"] = $"Error removing personality type - {result.Msg}";
                        TempData["KEY"] = "danger";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error removing personality type - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }
    }
}
