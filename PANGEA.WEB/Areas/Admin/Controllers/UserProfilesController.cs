﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PANGEA.DAL;
using PANGEA.DAL.Models;
using PANGEA.DAL.ViewModels;
using PANGEA.SERVICES;
using PANGEA.SERVICES.Email;
using PANGEA.SERVICES.Extensions;
using PANGEA.SERVICES.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace PANGEA.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class UserProfilesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<UserRole> _roleManager;
        private readonly IUnitOfWork _uow;
        private readonly IEmailSenderEnhance _emailSender;
        public GenericService _genericService;
        public IUserService _userService;
        private readonly ILogService _logService;
        private readonly IConfiguration _config;
        public IEmailSenderEnhance _emailService;

        public UserProfilesController(ApplicationDbContext context, IMapper mapper, UserManager<User> userManager,
            SignInManager<User> signInManager,
            RoleManager<UserRole> roleManager,
            IUnitOfWork uow,
            IEmailSenderEnhance emailSender,
            GenericService genericService,
            IUserService userService
            , ILogService logService
            , IConfiguration configuration
            , IEmailSenderEnhance emailService
            )
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _uow = uow;
            _emailSender = emailSender;
            _genericService = genericService;
            _userService = userService;
            _logService = logService;
            _config = configuration;
            _emailService = emailService;
        }

        public IActionResult Index(ListUserProfileVm vm)
        {
            try
            {
                var page = vm.Page ?? 1;
                var pageSize = int.Parse(_config.GetSection("PagingSettings:PageSize").Value);

                var profiles = _context.Roles.AsQueryable();
                if (!string.IsNullOrEmpty(vm.ProfileName))
                {
                    profiles = profiles.Where(i => i.Name.Contains(vm.ProfileName));
                }

                var model = new ListUserProfileVm()
                {
                    Profiles = profiles.ToPagedList(page, pageSize),
                };

                return View(model);
            }
            catch (Exception ex)
            {

                return View();
            }
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(UserRoleVm vm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userId = User.GetUserId();
                    var userRole = new UserRole();
                    new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<UserRoleVm, UserRole>();
                    }).CreateMapper().Map(vm, userRole);

                    userRole.CreatedById = User.GetUserId();
                    userRole.Id = Guid.NewGuid().ToString().ToLower();
                    userRole.CreatedOn = DateTime.UtcNow.AddHours(3);
                    await _roleManager.CreateAsync(userRole);

                    TempData["Message"] = "User profile created successfully.";
                    TempData["Key"] = "success";
                    return RedirectToAction("Index");

                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = $"Error creating user profile - {ex.Message}";
                TempData["Key"] = "success";
            } 
            
            return View();
        }

        public IActionResult Edit(string id)
        {
            var role = _roleManager.Roles.Single(i => i.Id == id);
            return View(role);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(UserRole role)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var existingRole = _context.Roles.Find(role.Id);
                    existingRole.Name = role.Name;
                    existingRole.Description = role.Description;
                    existingRole.IsExternal = role.IsExternal;
                    var roleresult = await _roleManager.UpdateAsync(existingRole);

                    if (roleresult.Succeeded)
                    {
                        TempData["MESSAGE"] = "User profile updated successfully.";
                        TempData["KEY"] = "success";
                        return RedirectToAction("Index", "UserProfiles");
                    }

                    TempData["MESSAGE"] = $"Error updating user profile";
                    TempData["KEY"] = "danger";
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error updating user profile - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return View(role);
        }

        public IActionResult Details(string id)
        {
            try
            {
                var role = _roleManager.Roles.Single(i => i.Id == id);

                ViewBag.Title = $"Details";
                return View(role);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }

        public IActionResult Remove(string id)
        {
            try
            {
                var role = _roleManager.Roles.Single(i => i.Id == id);

                ViewBag.Title = $"Confirm Remove";
                return View(role);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Remove(UserRole role)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var existingRole = _context.Roles.Find(role.Id);
                    var roleresult = await _roleManager.DeleteAsync(existingRole);

                    if (roleresult.Succeeded)
                    {
                        TempData["MESSAGE"] = "User profile was removed successfully.";
                        TempData["KEY"] = "success";
                        return RedirectToAction("Index", "UserProfiles");
                    }

                    TempData["MESSAGE"] = $"Error removing user profile";
                    TempData["KEY"] = "danger";
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }


        [Route("admin/manage/profiles/{id?}")]
        public ActionResult Manage(string id)
        {
            try
            {
                var userId = User.GetUserId();
                var rolesArray = _context.Roles.OrderBy(r => r.Name).ToList();

                var vm = new UserProfileVm();
                id = string.IsNullOrEmpty(id) ? rolesArray.First().Id : id;
                ViewBag.RoleId = new SelectList(rolesArray, "Id", "Name", id);
                vm.SystemTasks = _context.SystemTasks
                    .Include(i => i.Children)
                    .OrderBy(i => i.Order).ToList()
                    .Where(t => t.ParentId == null).ToList();
                vm.UserRoleProfileIds = _context.UserRoleProfiles.Where(r => r.RoleId == id).Select(r => r.TaskId).ToList();

                return View(vm);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Manage user roles error - {ex.Message}";
                TempData["KEY"] = "danger";
                return View();
            }
        }


        [HttpPost]
        [Route("admin/userprofiles/save", Name = "save")]
        public async Task<IActionResult> Save(UserProfileVm vm)
        {
            try
            {
                var userId = User.GetUserId();

                _context.UserRoleProfiles.RemoveRange(
                    _context.UserRoleProfiles.Where(d => d.RoleId == vm.UserRoleId));
                _context.SaveChanges();
                foreach (var taskId in vm.Ids)
                {
                    var roleProfile = new UserRoleProfile
                    {
                        TaskId = taskId,
                        RoleId = vm.UserRoleId
                    };
                    _context.UserRoleProfiles.Add(roleProfile);
                }

                _context.SaveChanges();

                TempData["Message"] = "User profile updated successfully.";
                TempData["Key"] = "success";

                return RedirectToAction("Index", new { id = vm.UserRoleId });
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
