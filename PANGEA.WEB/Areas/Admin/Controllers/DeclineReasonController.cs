﻿using AutoMapper;
using Dapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PANGEA.DAL;
using PANGEA.DAL.Models;
using PANGEA.DAL.ViewModels;
using PANGEA.SERVICES;
using PANGEA.SERVICES.Email;
using PANGEA.SERVICES.Extensions;
using PANGEA.SERVICES.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace PANGEA.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class DeclineReasonController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<UserRole> _roleManager;
        private readonly IEmailSenderEnhance _emailSender;
        public GenericService _genericService;
        public IUserService _userService;
        private readonly ILogService _logService;
        private readonly IConfiguration _config;

        public DeclineReasonController(
             ApplicationDbContext context, IMapper mapper,
             UserManager<User> userManager,
             SignInManager<User> signInManager,
             RoleManager<UserRole> roleManager,
             IEmailSenderEnhance emailSender,
             GenericService genericService,
             IUserService userService,
             ILogService logService,
             IConfiguration configuration
             )
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _emailSender = emailSender;
            _genericService = genericService;
            _userService = userService;
            _logService = logService;
            _config = configuration;
        }



        [HttpGet]
        public async Task<ActionResult> Index(int? page, ListLookUpsVm model)
        {
            try
            {
                var table = $"DeclineReason";
                var pageSize = int.Parse(_config.GetSection("PagingSettings:PageSize").Value);
                var pageNum = page ?? 1;
                var storedProcedure = "usp_GetLookUps";
                using IDbConnection db = new SqlConnection(DBService.DBConnection());
                var data = await db.QueryMultipleAsync(storedProcedure,
                    new { UserId = User.GetUserId(), Table = table },
                    commandType: CommandType.StoredProcedure);

                var filteredDeclineReasonList = data.Read<DeclineReasonLookUpVm>().ToPagedList();

                #region Search/Filter
                if (model.IncludeRemoved)
                {
                    filteredDeclineReasonList = filteredDeclineReasonList.Where(a => a.Active == true || a.Active == false).ToPagedList();
                }
                else
                {
                    filteredDeclineReasonList = filteredDeclineReasonList.Where(a => a.Active == true).ToPagedList();
                }

                if (!string.IsNullOrEmpty(model.Search))
                {
                    filteredDeclineReasonList = filteredDeclineReasonList.Where(a => a.Decline_Reason.Contains(model.Search)).ToPagedList();
                }
                #endregion

                model.Table = table;
                model.DeclineReasonList = filteredDeclineReasonList.OrderBy(r => r.Decline_Reason).ToPagedList(pageNum, pageSize);

            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Create()
        {
            var model = new DeclineReasonLookUpVm();

            try
            {
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error creating decline reason - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(DeclineReasonLookUpVm model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var spName = "Code.usp_New_Add_Decline_Reason";
                    var inputParameters = new
                    {
                        User_ID = Guid.NewGuid(),
                        Decline_Reason = model.Decline_Reason,
                        Description = model.Description,
                        // Public_Description = model.Public_Description,
                        // @Decline_Reason_Code_ID = null,
                        Active = model.Active,
                    };

                    var result = _genericService.Query<SpIntVm>(spName, inputParameters);
                    if (result.Code == 200)
                    {
                        TempData["MESSAGE"] = "The decline reason was added successfully.";
                        TempData["KEY"] = "success";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["MESSAGE"] = $"Error adding decline reason - {result.Msg}";
                        TempData["KEY"] = "danger";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error adding decline reason - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            try
            {
                var spName = "dbo.usp_GetLookUpById";
                var inputParameters = new { Id = id, @Table = $"DeclineReason" };
                var model = (_genericService.Query<DeclineReasonLookUpVm>(spName, inputParameters).Data).FirstOrDefault();
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error loading decline reason - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(DeclineReasonLookUpVm model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var spName = "Code.usp_New_Add_Decline_Reason";
                    var inputParameters = new
                    {
                        User_ID = Guid.NewGuid(),
                        Decline_Reason = model.Decline_Reason,
                        Decline_Reason_Code_ID = model.Decline_Reason_Code_ID,
                        Description = model.Description,
                        // Public_Description = model.Public_Description,
                        Active = model.Active,
                    };

                    var result = _genericService.Query<SpIntVm>(spName, inputParameters);
                    if (result.Code == 200)
                    {
                        TempData["MESSAGE"] = "The decline reason was updated successfully.";
                        TempData["KEY"] = "success";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["MESSAGE"] = $"Error updating decline reason - {result.Msg}";
                        TempData["KEY"] = "danger";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error updating decline reason - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Details(int id)
        {
            try
            {
                var spName = "dbo.usp_GetLookUpById";
                var inputParameters = new { Id = id, @Table = $"DeclineReason" };
                var model = (_genericService.Query<DeclineReasonLookUpVm>(spName, inputParameters).Data).FirstOrDefault();

                ViewBag.Title = $"Decline Reason Details";
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error loading decline reason - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }
        }


        [HttpGet]
        public async Task<ActionResult> Remove(int id)
        {
            try
            {
                var spName = "dbo.usp_GetLookUpById";
                var inputParameters = new { Id = id, @Table = $"DeclineReason" };
                var model = (_genericService.Query<DeclineReasonLookUpVm>(spName, inputParameters).Data).FirstOrDefault();

                ViewBag.Title = $"Remove Decline Reason";
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error loading decline reason - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var _spName = "dbo.usp_GetLookUpById";
                    var _inputParameters = new { Id = id, @Table = $"DeclineReason" };
                    var model = (_genericService.Query<DeclineReasonLookUpVm>(_spName, _inputParameters).Data).FirstOrDefault();

                    var spName = "Code.usp_New_Add_Decline_Reason";
                    var inputParameters = new
                    {
                        User_ID = Guid.NewGuid(),
                        Decline_Reason = model.Decline_Reason,
                        Decline_Reason_Code_ID = model.Decline_Reason_Code_ID,
                        Description = model.Description,
                        // Public_Description = model.Public_Description,
                        Active = false,
                    };

                    var result = _genericService.Query<SpIntVm>(spName, inputParameters);
                    if (result.Code == 200)
                    {
                        TempData["MESSAGE"] = "The decline reason was removed successfully.";
                        TempData["KEY"] = "success";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["MESSAGE"] = $"Error removing decline reason - {result.Msg}";
                        TempData["KEY"] = "danger";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error removing decline reason - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }
    }
}
