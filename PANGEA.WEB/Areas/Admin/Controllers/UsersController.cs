﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PANGEA.DAL;
using PANGEA.DAL.Models;
using PANGEA.DAL.ViewModels;
using PANGEA.SERVICES;
using PANGEA.SERVICES.Email;
using PANGEA.SERVICES.Extensions;
using PANGEA.SERVICES.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace PANGEA.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class UsersController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<UserRole> _roleManager;
        private readonly IUnitOfWork _uow;
        private readonly IEmailSenderEnhance _emailSender;
        public GenericService _genericService;
        public IUserService _userService;
        private readonly ILogService _logService;
        private readonly IConfiguration _config;
        public IEmailSenderEnhance _emailService;

        public UsersController(ApplicationDbContext context, IMapper mapper, UserManager<User> userManager,
            SignInManager<User> signInManager,
            RoleManager<UserRole> roleManager,
            IUnitOfWork uow,
            IEmailSenderEnhance emailSender,
            GenericService genericService,
            IUserService userService
            , ILogService logService
            , IConfiguration configuration
            , IEmailSenderEnhance emailService
            )
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _uow = uow;
            _emailSender = emailSender;
            _genericService = genericService;
            _userService = userService;
            _logService = logService;
            _config = configuration;
            _emailService = emailService;
        }

        public async Task<ActionResult> Index(ListUsersVm vm)
        {
            try
            {
                var userId = User.GetUserId();
                var page = vm.Page ?? 1;
                var pageSize = int.Parse(_config.GetSection("PagingSettings:PageSize").Value);

                var users = _context.Users.Include(x => x.Role).AsQueryable();
                if (!string.IsNullOrEmpty(vm.Email))
                {
                    users = users.Where(i => i.UserName.Contains(vm.Email));
                }


                var model = new ListUsersVm()
                {
                    Users = users.ToPagedList(page, pageSize),
                };

                var rolesArray = _roleManager.Roles.OrderBy(i => i.Name).ToList();
                ViewData["Role"] = new SelectList(rolesArray, "Name", "Name");

                return View(model);

            }
            catch(Exception ex)
            {
                return View();
            }
        }

        public async Task<ActionResult> Create()
        {
            try 
            {
                var userLocations = new List<UserLocationVm>();

                List<LookUps> countriesLookUps = await GetLookUpsAsync("Country");

                foreach (var countryLookUp in countriesLookUps)
                {
                    userLocations.Add(new UserLocationVm()
                    {
                        Id = countryLookUp.Lkp_ID,
                        Location = countryLookUp.Lkp_Lookup,
                        Display = countryLookUp.Lkp_Lookup,
                        IsSelected = false
                    });
                }

                var model = new UserVm
                {
                    Locations = userLocations
                };

                var rolesArray = _roleManager.Roles.OrderBy(i => i.Name).ToList();
                ViewData["Roles"] = new SelectList(rolesArray, "Name");

                return View(model);
            } 
            catch (Exception ex)
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(UserVm vm)
        {
            #region Validate Locations
            List<UserLocationVm> filteredLocationsList = vm.Locations.Where(x => x.IsSelected ==  true).ToList();
            if (filteredLocationsList.Count == 0)
                ModelState.AddModelError("Locations", "The Countries field is required.");
            #endregion

            try
            {
                if (ModelState.IsValid)
                {
                    var user = new User();
                    new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<UserVm, User>();
                    }).CreateMapper().Map(vm, user);
                    user.PasswordChangedOn = null;
                    user.CreatedOn = DateTime.Now;
                    user.LockoutEnabled = true;
                    user.PhoneNumberConfirmed = true;
                    user.TwoFactorEnabled = false;
                    user.EmailConfirmed = true;
                    user.CreatedById = User.GetUserId();
                    user.UserName = vm.Email;

                    var password = $"AHdy2V3/xoXVIi4g4SY9N41nv4e/rXoa7YEwY8KN7OhEXShT1IdiX5azjHRBZBpz8A==";
                    // Password : Password123!@#
                    var result = await _userManager.CreateAsync(user, password);
                    if (result.Succeeded)
                    {
                        await _userManager.AddToRoleAsync(user, vm.RoleName);
                        user.RoleId = _context.Roles.Single(i => i.Name == vm.RoleName).Id;
                        _context.SaveChanges();

                        var selectedLocs = vm.Locations.Where(x => x.IsSelected == true).ToList();
                        var locs = new List<UserLocation>();
                        if (selectedLocs.Any())
                        {
                            foreach (var item in selectedLocs)
                            {
                                locs.Add(new UserLocation
                                {
                                    User = user,
                                    Location = _context.LookUps.Where(i => i.Lkp_ID == item.Id).FirstOrDefault(),
                                    CreatedOn = DateTime.Now,
                                    CreatedById = User.GetUserId()
                                });
                            }

                            if (locs.Any())
                            {
                                _genericService.AddRange(locs);
                            }
                        }


                        TempData["MESSAGE"] = "The user was created successfully.";
                        TempData["KEY"] = "success";

                        return RedirectToAction("Index");
                    }
                    AddErrors(result);
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = $"Error creating user - {ex.Message}";
                TempData["Key"] = "success";
            }

            var userLocations = new List<UserLocationVm>();
            List<LookUps> countriesLookUps = await GetLookUpsAsync("Country");
            foreach (var countryLookUp in countriesLookUps)
            {
                userLocations.Add(new UserLocationVm()
                {
                    Id = countryLookUp.Lkp_ID,
                    Location = countryLookUp.Lkp_Lookup,
                    Display = countryLookUp.Lkp_Lookup,
                    IsSelected = false
                });
            }
            

            vm.Locations = userLocations;
            var rolesArray = _roleManager.Roles.OrderBy(i => i.Name).ToList();
            ViewData["Roles"] = new SelectList(rolesArray, "Name");

            return View(vm);
        }

        public async Task<ActionResult> Edit(string id)
        {
            try
            {
                User user = _context.Users.Include(i => i.Role).SingleOrDefault(i => i.Id == id);

                var model = new UserVm();
                new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<User, UserVm>();
                }).CreateMapper().Map(user, model);

                var rolesArray = _context.Roles.OrderBy(i => i.Name).ToList();
                var userRoles = await _userManager.GetRolesAsync(user);
                if (userRoles.Any())
                {
                    var userRole = userRoles[0];
                    ViewBag.Roles = new SelectList(rolesArray, "Name", "Name", userRole);
                }
                else
                {
                    ViewBag.Roles = new SelectList(rolesArray, "Name", "Name");
                }

                var userLocations = new List<UserLocationVm>();
                List<LookUps> countriesLookUps = await GetLookUpsAsync("Country");
                foreach (var countryLookUp in countriesLookUps)
                {
                    userLocations.Add(new UserLocationVm()
                    {
                        Id = countryLookUp.Lkp_ID,
                        Location = countryLookUp.Lkp_Lookup,
                        Display = countryLookUp.Lkp_Lookup,
                        IsSelected = _context.UserLocations.Any(x => x.Location.Lkp_ID == countryLookUp.Lkp_ID)
                    });
                }

                model.Locations = userLocations;

                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error loading user for update - {ex.Message}";
                TempData["KEY"] = "danger";
            } 
            
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(UserVm vm)
        {
            var user = _context.Users.Include(i => i.Role).SingleOrDefault(i => i.UserId == vm.UserId);
            var oldRoleId = user.RoleId;
            var oldRole = _context.Roles.Find(oldRoleId);
            try
            {
                if (ModelState.IsValid)
                {
                    if (user == null)
                    {
                        return NotFound();
                    }
                    user.UserId = vm.UserId;
                    user.Title = vm.Title;
                    user.FirstName = vm.FirstName;
                    user.MiddleName = vm.MiddleName;
                    user.Surname = vm.Surname;
                    user.Email = vm.Email;
                    user.LockoutEnabled = false;
                    user.LockoutEnd = null;
                    user.IsLocked = false; 
                    user.Department = vm.Department;
                    user.ModifiedById = User.GetUserId();
                    user.ModifiedOn = DateTime.Now;

                    if (oldRole == null || !oldRole.Name.Equals(vm.RoleName))
                    {
                        if (oldRole != null)
                        {
                            await _userManager.RemoveFromRoleAsync(user, oldRole.Name);
                        }
                        user.RoleId = _context.Roles.Single(i => i.Name == vm.RoleName).Id;
                        await _userManager.AddToRoleAsync(user, vm.RoleName);
                    }
                    await _userManager.UpdateAsync(user).ConfigureAwait(true);

                    var userLocation = await _genericService.GetAsync<UserLocation>(x => x.User.Id == user.Id).ConfigureAwait(true);
                    _genericService.RemoveRange(userLocation);

                    var selectedLocs = vm.Locations.Where(x => x.IsSelected == true).ToList();
                    var locs = new List<UserLocation>();
                    if (selectedLocs.Any())
                    {
                        foreach (var item in selectedLocs)
                        {
                            locs.Add(new UserLocation
                            {
                                User = user,
                                Location = _context.LookUps.Where(i => i.Lkp_ID == item.Id).FirstOrDefault(),
                                ModifiedOn = DateTime.Now,
                                ModifiedById = User.GetUserId()
                            });
                        }

                        if (locs.Any())
                        {
                            _genericService.AddRange(locs);
                        }
                    }

                    TempData["MESSAGE"] = "User was updated successfully.";
                    TempData["KEY"] = "success";
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"User updating failed - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserVm>();
            }).CreateMapper().Map(user, vm);
            vm.UserRole = oldRole;
            var rolesArray = _context.Roles.OrderBy(i => i.Name).ToList();

            var userRoles = await _userManager.GetRolesAsync(user);
            if (userRoles.Any())
            {
                var userRole = userRoles[0];
                ViewBag.Roles = new SelectList(rolesArray, "Name", "Name", userRole);
            }
            else
            {
                ViewBag.Roles = new SelectList(rolesArray, "Name", "Name");
            }

            var userLocations = new List<UserLocationVm>();
            List<LookUps> countriesLookUps = await GetLookUpsAsync("Country");
            foreach (var countryLookUp in countriesLookUps)
            {
                userLocations.Add(new UserLocationVm()
                {
                    Id = countryLookUp.Lkp_ID,
                    Location = countryLookUp.Lkp_Lookup,
                    Display = countryLookUp.Lkp_Lookup,
                    IsSelected = _context.UserLocations.Any(x => x.Location.Lkp_ID == countryLookUp.Lkp_ID)
                });
            }
            vm.Locations = userLocations;

            return View(model: vm);
        }

        public async Task<IActionResult> Details(string id)
        {
            try
            {
                var user = await _genericService.GetOneAsync<User>(x => x.Id == id, "Role")
                    .ConfigureAwait(true);

                ViewBag.Title = $"Details";
                return View(user);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"User details loading failed - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Deactivate(string id)
        {
            try
            {
                var user = await _genericService.GetOneAsync<User>(x => x.Id == id, "Role")
                    .ConfigureAwait(true);

                ViewBag.Title = $"Deactivate";
                return View(user);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"User details loading failed - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Deactivate(User model)
        {
            try
            {
                var user = await _genericService.GetOneAsync<User>(x => x.Id == model.Id);

                var newId = _userService.DeactivateUser(model.Id);
                user.IsActive = false;
                _genericService.AddOrUpdate(user);

                TempData["MESSAGE"] = "User deactivation was completed successfully.";
                TempData["KEY"] = "success";
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"User deactivation failed - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }


        public async Task<IActionResult> Activate(string id)
        {
            try
            {
                var user = await _genericService.GetOneAsync<User>(x => x.Id == id, "Role")
                    .ConfigureAwait(true);

                ViewBag.Title = $"Activate";
                return View(user);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"User details loading failed - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Activate(User model)
        {
            try
            {
                var user = await _genericService.GetOneAsync<User>(x => x.Id == model.Id);

                var newId = _userService.ActivateUser(model.Id);
                user.LockoutEnabled = false;
                user.LockoutEnd = null;
                user.IsLocked = false;
                user.IsActive = true;
                user.IsLocked = false;
                _genericService.AddOrUpdate(user);

                TempData["MESSAGE"] = "User activation was completed successfully.";
                TempData["KEY"] = "success";
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"User activation failed - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }

        [NonAction]
        public async Task<List<LookUps>> GetLookUpsAsync(string parentLookUp)
        {
            var childLookUpsList = new List<LookUps>();
            var childLookUps = (await _genericService.GetSearchableQueryable<LookUps>(x => x.Lkp_Lookup == parentLookUp, null).ConfigureAwait(false)).ToList<LookUps>();

            foreach (var childLookUp in childLookUps)
            {
                if (childLookUp.Lkp_IsParent)
                {
                    var childSubLookUps = (await _genericService.GetAsync<LookUps>(x => x.Lkp_ParentID == childLookUp.Lkp_ID, null).ConfigureAwait(false)).ToList<LookUps>();
                    foreach (var childSubLookUp in childSubLookUps)
                    {
                        childLookUpsList.Add(childSubLookUp);
                    }

                }
                else
                { 
                    childLookUpsList.Add(childLookUp);
                }
            }

            return childLookUpsList;
        }

        [NonAction]
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }
    }
}
