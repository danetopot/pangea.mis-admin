﻿using AutoMapper;
using Dapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PANGEA.DAL;
using PANGEA.DAL.Models;
using PANGEA.DAL.ViewModels;
using PANGEA.SERVICES;
using PANGEA.SERVICES.Email;
using PANGEA.SERVICES.Extensions;
using PANGEA.SERVICES.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace PANGEA.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class LocationController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<UserRole> _roleManager;
        private readonly IEmailSenderEnhance _emailSender;
        public GenericService _genericService;
        public IUserService _userService;
        private readonly ILogService _logService;
        private readonly IConfiguration _config;

        public LocationController(
             ApplicationDbContext context, IMapper mapper,
             UserManager<User> userManager,
             SignInManager<User> signInManager,
             RoleManager<UserRole> roleManager,
             IEmailSenderEnhance emailSender,
             GenericService genericService,
             IUserService userService,
             ILogService logService,
             IConfiguration configuration
             )
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _emailSender = emailSender;
            _genericService = genericService;
            _userService = userService;
            _logService = logService;
            _config = configuration;
        }



        [HttpGet]
        public async Task<ActionResult> Index(int? page, ListLookUpsVm model)
        {
            try
            {
                var table = $"Location";
                var pageSize = int.Parse(_config.GetSection("PagingSettings:PageSize").Value);
                var pageNum = page ?? 1;
                var storedProcedure = "usp_GetLookUps";
                using IDbConnection db = new SqlConnection(DBService.DBConnection());
                var data = await db.QueryMultipleAsync(storedProcedure,
                    new { UserId = User.GetUserId(), Table = table },
                    commandType: CommandType.StoredProcedure);

                var filteredLocationList = data.Read<LocationLookUpVm>().ToPagedList();

                #region Search/Filter
                if (model.IncludeRemoved)
                {
                    filteredLocationList = filteredLocationList.Where(a => a.Active == true || a.Active == false).ToPagedList();
                }
                else
                {
                    filteredLocationList = filteredLocationList.Where(a => a.Active == true).ToPagedList();
                }

                if (!string.IsNullOrEmpty(model.Search))
                {
                    filteredLocationList = filteredLocationList.Where(a => a.Location_Name.Contains(model.Search)).ToPagedList();
                } 
                #endregion

                model.Table = table;
                model.LocationList = filteredLocationList.OrderBy(r => r.Location_Name).ToPagedList(pageNum, pageSize);

            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Create()
        {
            var model = new LocationLookUpVm();

            try
            {
                var table = $"Country";
                var storedProcedure = "Code.usp_GetLookUps";
                using IDbConnection db = new SqlConnection(DBService.DBConnection());
                var data = await db.QueryMultipleAsync(storedProcedure,
                    new { UserId = User.GetUserId(), Table = table },
                    commandType: CommandType.StoredProcedure);

                var countryList = data.Read<CountryLookUpVm>().ToList();
                ViewBag.Countries = new SelectList(countryList, "Country_Code_ID", "Country_Name");

                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error creating location - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(LocationLookUpVm model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var spName = "Code.usp_New_Add_Location";
                    var inputParameters = new
                    {
                        User_ID = Guid.NewGuid(),
                        Location_Name = model.Location_Name,
                        Country_Code_ID = model.Country_Code_ID,
                        Sponsorship_Site = model.Sponsorship_Site? $"1" : $"0",
                        Default_Language = model.Default_Language,
                        // @Location_Code_ID = null,
                        Active = model.Active,
                    };

                    var result = _genericService.Query<SpIntVm>(spName, inputParameters);
                    if(result.Code == 200)
                    {
                        TempData["MESSAGE"] = "The location was added successfully.";
                        TempData["KEY"] = "success";
                        return RedirectToAction("Index");
                    } 
                    else
                    {
                        TempData["MESSAGE"] = $"Error adding location - {result.Msg}";
                        TempData["KEY"] = "danger";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error adding location - {ex.Message}";
                TempData["KEY"] = "danger";
            }



            var table = $"Country";
            var storedProcedure = "usp_GetLookUps";
            using IDbConnection db = new SqlConnection(DBService.DBConnection());
            var data = await db.QueryMultipleAsync(storedProcedure,
                new { UserId = User.GetUserId(), Table = table },
                commandType: CommandType.StoredProcedure);

            var countryList = data.Read<CountryLookUpVm>().ToList();
            ViewBag.Countries = new SelectList(countryList, "Country_Code_ID", "Country_Name");

            return View(model);
        }


        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            try
            {
                var spName = "Code.usp_GetLookUpById";
                var inputParameters = new  { Id = id, @Table = $"Location" };
                var model = (_genericService.Query<LocationLookUpVm>(spName, inputParameters).Data).FirstOrDefault();

                var table = $"Country";
                var storedProcedure = "usp_GetLookUps";
                using IDbConnection db = new SqlConnection(DBService.DBConnection());
                var data = await db.QueryMultipleAsync(storedProcedure,
                    new { UserId = User.GetUserId(), Table = table },
                    commandType: CommandType.StoredProcedure);

                var countryList = data.Read<CountryLookUpVm>().ToList();
                ViewBag.Countries = new SelectList(countryList, "Country_Code_ID", "Country_Name");

                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error loading location - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(LocationLookUpVm model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var spName = "Code.usp_New_Add_Location";
                    var inputParameters = new
                    {
                        User_ID = Guid.NewGuid(),
                        Location_Name = model.Location_Name,
                        Location_Code_ID = model.Location_Code_ID,
                        Country_Code_ID = model.Country_Code_ID,
                        Sponsorship_Site = model.Sponsorship_Site ? $"1" : $"0",
                        Default_Language = model.Default_Language,
                        Active = model.Active,
                    };

                    var result = _genericService.Query<SpIntVm>(spName, inputParameters);
                    if (result.Code == 200)
                    {
                        TempData["MESSAGE"] = "The location was updated successfully.";
                        TempData["KEY"] = "success";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["MESSAGE"] = $"Error updating location - {result.Msg}";
                        TempData["KEY"] = "danger";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error updating location - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Details(int id)
        {
            try
            {
                var spName = "Code.usp_GetLookUpById";
                var inputParameters = new { Id = id, @Table = $"Location" };
                var model = (_genericService.Query<LocationLookUpVm>(spName, inputParameters).Data).FirstOrDefault();

                ViewBag.Title = $"Location Details";
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error loading loaction - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }
        }


        [HttpGet]
        public async Task<ActionResult> Remove(int id)
        {
            try
            {
                var spName = "Code.usp_GetLookUpById";
                var inputParameters = new { Id = id, @Table = $"Location" };
                var model = (_genericService.Query<LocationLookUpVm>(spName, inputParameters).Data).FirstOrDefault();

                ViewBag.Title = $"Remove Location";
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error loading location - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var _spName = "Code.usp_GetLookUpById";
                    var _inputParameters = new { Id = id, @Table = $"Location" };
                    var model = (_genericService.Query<LocationLookUpVm>(_spName, _inputParameters).Data).FirstOrDefault();

                    var spName = "Code.usp_New_Add_Location";
                    var inputParameters = new
                    {
                        User_ID = Guid.NewGuid(),
                        Location_Name = model.Location_Name,
                        Location_Code_ID = model.Location_Code_ID,
                        Country_Code_ID = model.Country_Code_ID,
                        Sponsorship_Site = model.Sponsorship_Site ? $"1" : $"0",
                        Default_Language = model.Default_Language,
                        Active = false,
                    };

                    var result = _genericService.Query<SpIntVm>(spName, inputParameters);
                    if (result.Code == 200)
                    {
                        TempData["MESSAGE"] = "The location was removed successfully.";
                        TempData["KEY"] = "success";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["MESSAGE"] = $"Error removing location - {result.Msg}";
                        TempData["KEY"] = "danger";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error removing location - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }
    }
}
