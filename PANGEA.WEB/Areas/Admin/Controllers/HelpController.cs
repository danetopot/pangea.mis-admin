﻿using AutoMapper;
using Dapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PANGEA.DAL;
using PANGEA.DAL.Models;
using PANGEA.DAL.ViewModels;
using PANGEA.SERVICES;
using PANGEA.SERVICES.Email;
using PANGEA.SERVICES.Extensions;
using PANGEA.SERVICES.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace PANGEA.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class HelpController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<UserRole> _roleManager;
        private readonly IEmailSenderEnhance _emailSender;
        public GenericService _genericService;
        public IUserService _userService;
        private readonly ILogService _logService;
        private readonly IConfiguration _config;

        public HelpController(
             ApplicationDbContext context, IMapper mapper,
             UserManager<User> userManager,
             SignInManager<User> signInManager,
             RoleManager<UserRole> roleManager,
             IEmailSenderEnhance emailSender,
             GenericService genericService,
             IUserService userService,
             ILogService logService,
             IConfiguration configuration
             )
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _emailSender = emailSender;
            _genericService = genericService;
            _userService = userService;
            _logService = logService;
            _config = configuration;
        }



        [HttpGet]
        public async Task<ActionResult> Index()
        {
            try
            {

            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }

            return View();
        }

        [HttpGet]
        public async Task<ActionResult> About()
        {
            try
            {

            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }

            return View();
        }

        [HttpGet]
        public async Task<ActionResult> ContactUs()
        {
            try
            {

            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }

            return View();
        }
    }
}
