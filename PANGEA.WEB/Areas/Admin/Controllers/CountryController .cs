﻿using AutoMapper;
using Dapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PANGEA.DAL;
using PANGEA.DAL.Models;
using PANGEA.DAL.ViewModels;
using PANGEA.SERVICES;
using PANGEA.SERVICES.Email;
using PANGEA.SERVICES.Extensions;
using PANGEA.SERVICES.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace PANGEA.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class CountryController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<UserRole> _roleManager;
        private readonly IEmailSenderEnhance _emailSender;
        public GenericService _genericService;
        public IUserService _userService;
        private readonly ILogService _logService;
        private readonly IConfiguration _config;

        public CountryController(
             ApplicationDbContext context, IMapper mapper,
             UserManager<User> userManager,
             SignInManager<User> signInManager,
             RoleManager<UserRole> roleManager,
             IEmailSenderEnhance emailSender,
             GenericService genericService,
             IUserService userService,
             ILogService logService,
             IConfiguration configuration
             )
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _emailSender = emailSender;
            _genericService = genericService;
            _userService = userService;
            _logService = logService;
            _config = configuration;
        }



        [HttpGet]
        public async Task<ActionResult> Index(int? page, ListLookUpsVm model)
        {
            try
            {
                var table = $"Country";
                var pageSize = int.Parse(_config.GetSection("PagingSettings:PageSize").Value);
                var pageNum = page ?? 1;
                var storedProcedure = "Code.usp_GetLookUps";
                using IDbConnection db = new SqlConnection(DBService.DBConnection());
                var data = await db.QueryMultipleAsync(storedProcedure,
                    new { UserId = User.GetUserId(), Table = table },
                    commandType: CommandType.StoredProcedure);

                var filteredCountryList = data.Read<CountryLookUpVm>().ToPagedList();

                #region Search/Filter
                if (model.IncludeRemoved)
                {
                    filteredCountryList = filteredCountryList.Where(a => a.Active == true || a.Active == false).ToPagedList();
                }
                else
                {
                    filteredCountryList = filteredCountryList.Where(a => a.Active == true).ToPagedList();
                }

                if (!string.IsNullOrEmpty(model.Search))
                {
                    filteredCountryList = filteredCountryList.Where(a => a.Country_Name.Contains(model.Search)).ToPagedList();
                } 
                #endregion

                model.Table = table;
                model.CountryList = filteredCountryList.OrderBy(r => r.Region_Name).ToPagedList(pageNum, pageSize);

            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Create()
        {
            var model = new CountryLookUpVm();

            try
            {
                var table = $"Region";
                var storedProcedure = "Code.usp_GetLookUps";
                using IDbConnection db = new SqlConnection(DBService.DBConnection());
                var data = await db.QueryMultipleAsync(storedProcedure,
                    new { UserId = User.GetUserId(), Table = table },
                    commandType: CommandType.StoredProcedure);

                var regionList = data.Read<RegionLookUpVm>().ToList();
                ViewBag.Regions = new SelectList(regionList, "Region_Code_ID", "Region_Name");

                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error creating region - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CountryLookUpVm model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var spName = "Code.usp_New_Add_Country";
                    var inputParameters = new
                    {
                        User_ID = Guid.NewGuid(),
                        Country_Code = model.Country_Code, 
                        Country_Name = model.Country_Name,
                        Region_Code_ID = model.Region_Code_ID,
                        Default_Language = model.Default_Language,
                        Min_School_Age = model.Min_School_Age,
                        // @Country_Code_ID = null,
                        Active = model.Active,
                    };

                    var result = _genericService.Query<SpIntVm>(spName, inputParameters);
                    if(result.Code == 200)
                    {
                        TempData["MESSAGE"] = "The country was added successfully.";
                        TempData["KEY"] = "success";
                        return RedirectToAction("Index");
                    } 
                    else
                    {
                        TempData["MESSAGE"] = $"Error country region - {result.Msg}";
                        TempData["KEY"] = "danger";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error adding country - {ex.Message}";
                TempData["KEY"] = "danger";
            }



            var table = $"Region";
            var storedProcedure = "usp_GetLookUps";
            using IDbConnection db = new SqlConnection(DBService.DBConnection());
            var data = await db.QueryMultipleAsync(storedProcedure,
                new { UserId = User.GetUserId(), Table = table },
                commandType: CommandType.StoredProcedure);

            var regionList = data.Read<RegionLookUpVm>().ToList();
            ViewBag.Regions = new SelectList(regionList, "Region_Code_ID", "Region_Name");
            return View(model);
        }


        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            try
            {
                var spName = "Code.usp_GetLookUpById";
                var inputParameters = new  { Id = id, @Table = $"Country" };
                var model = (_genericService.Query<CountryLookUpVm>(spName, inputParameters).Data).FirstOrDefault();

                var table = $"Region";
                var storedProcedure = "usp_GetLookUps";
                using IDbConnection db = new SqlConnection(DBService.DBConnection());
                var data = await db.QueryMultipleAsync(storedProcedure,
                    new { UserId = User.GetUserId(), Table = table },
                    commandType: CommandType.StoredProcedure);
                var regionList = data.Read<RegionLookUpVm>().ToList();

                ViewBag.Regions = new SelectList(regionList, "Region_Code_ID", "Region_Name");

                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error loading country - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(CountryLookUpVm model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var spName = "Code.usp_New_Add_Country";
                    var inputParameters = new
                    {
                        User_ID = Guid.NewGuid(),
                        Country_Code_ID = model.Country_Code_ID,
                        Country_Code = model.Country_Code,
                        Country_Name = model.Country_Name,
                        Region_Code_ID = model.Region_Code_ID,
                        Default_Language = model.Default_Language,
                        Min_School_Age = model.Min_School_Age,
                        Active = model.Active,
                    };

                    var result = _genericService.Query<SpIntVm>(spName, inputParameters);
                    if (result.Code == 200)
                    {
                        TempData["MESSAGE"] = "The country was updated successfully.";
                        TempData["KEY"] = "success";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["MESSAGE"] = $"Error updating country - {result.Msg}";
                        TempData["KEY"] = "danger";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error updating country - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Details(int id)
        {
            try
            {
                var spName = "Code.usp_GetLookUpById";
                var inputParameters = new { Id = id, @Table = $"Country" };
                var model = (_genericService.Query<CountryLookUpVm>(spName, inputParameters).Data).FirstOrDefault();

                ViewBag.Title = $"Country Details";
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error loading country - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }
        }


        [HttpGet]
        public async Task<ActionResult> Remove(int id)
        {
            try
            {
                var spName = "Code.usp_GetLookUpById";
                var inputParameters = new { Id = id, @Table = $"Country" };
                var model = (_genericService.Query<CountryLookUpVm>(spName, inputParameters).Data).FirstOrDefault();

                ViewBag.Title = $"Remove Country";
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error loading country - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var _spName = "Code.usp_GetLookUpById";
                    var _inputParameters = new { Id = id, @Table = $"Country" };
                    var model = (_genericService.Query<CountryLookUpVm>(_spName, _inputParameters).Data).FirstOrDefault();

                    var spName = "Code.usp_New_Add_Country";
                    var inputParameters = new
                    {
                        User_ID = Guid.NewGuid(),
                        Country_Code_ID = model.Country_Code_ID,
                        Country_Code = model.Country_Code,
                        Country_Name = model.Country_Name,
                        Region_Code_ID = model.Region_Code_ID,
                        Default_Language = model.Default_Language,
                        Min_School_Age = model.Min_School_Age,
                        Active = false,
                    };

                    var result = _genericService.Query<SpIntVm>(spName, inputParameters);
                    if (result.Code == 200)
                    {
                        TempData["MESSAGE"] = "The country was removed successfully.";
                        TempData["KEY"] = "success";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["MESSAGE"] = $"Error removing country - {result.Msg}";
                        TempData["KEY"] = "danger";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error removing country - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }
    }
}
