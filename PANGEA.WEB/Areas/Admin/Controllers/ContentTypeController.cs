﻿using AutoMapper;
using Dapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PANGEA.DAL;
using PANGEA.DAL.Models;
using PANGEA.DAL.ViewModels;
using PANGEA.SERVICES;
using PANGEA.SERVICES.Email;
using PANGEA.SERVICES.Extensions;
using PANGEA.SERVICES.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace PANGEA.WEB.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ContentTypeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<UserRole> _roleManager;
        private readonly IEmailSenderEnhance _emailSender;
        public GenericService _genericService;
        public IUserService _userService;
        private readonly ILogService _logService;
        private readonly IConfiguration _config;

        public ContentTypeController(
             ApplicationDbContext context, IMapper mapper,
             UserManager<User> userManager,
             SignInManager<User> signInManager,
             RoleManager<UserRole> roleManager,
             IEmailSenderEnhance emailSender,
             GenericService genericService,
             IUserService userService,
             ILogService logService,
             IConfiguration configuration
             )
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _emailSender = emailSender;
            _genericService = genericService;
            _userService = userService;
            _logService = logService;
            _config = configuration;
        }



        [HttpGet]
        public async Task<ActionResult> Index(int? page, ListLookUpsVm model)
        {
            try
            {
                var table = $"ContentType";
                var pageSize = int.Parse(_config.GetSection("PagingSettings:PageSize").Value);
                var pageNum = page ?? 1;
                var storedProcedure = "Code.usp_GetLookUps";
                using IDbConnection db = new SqlConnection(DBService.DBConnection());
                var data = await db.QueryMultipleAsync(storedProcedure,
                    new { UserId = User.GetUserId(), Table = table },
                    commandType: CommandType.StoredProcedure);

                var filteredContentTypeList = data.Read<ContentTypeLookUpVm>().ToPagedList();

                #region Search/Filter
                if (model.IncludeRemoved)
                {
                    filteredContentTypeList = filteredContentTypeList.Where(a => a.Active == true || a.Active == false).ToPagedList();
                }
                else
                {
                    filteredContentTypeList = filteredContentTypeList.Where(a => a.Active == true).ToPagedList();
                }

                if (!string.IsNullOrEmpty(model.Search))
                {
                    filteredContentTypeList = filteredContentTypeList.Where(a => a.Content_Type.Contains(model.Search)).ToPagedList();
                }
                #endregion

                model.Table = table;
                model.ContentTypeList = filteredContentTypeList.OrderBy(r => r.Content_Type).ToPagedList(pageNum, pageSize);

            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Create()
        {
            var model = new ContentTypeLookUpVm();

            try
            {
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error creating content type - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ContentTypeLookUpVm model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var spName = "Code.usp_New_Add_Content_Type";
                    var inputParameters = new
                    {
                        User_ID = Guid.NewGuid(),
                        Content_Type = model.Content_Type,
                        Description = model.Description,
                        Public_Description = model.Public_Description,
                        Required = model.Required,
                        // Content_Type_Code_ID = model.Content_Type_Code_ID,
                        Workflow_Related = model.Workflow_Related,
                        App_Related = model.App_Related,
                        Default_Status_Code_ID = model.Default_Status_Code_ID,
                        Default_Translation_Required = model.Default_Translation_Required,
                        Default_QC_Required = model.Default_QC_Required,
                        Default_Shippable = model.Default_Shippable,
                        Active = model.Active,
                    };

                    var result = _genericService.Query<SpIntVm>(spName, inputParameters);
                    if (result.Code == 200)
                    {
                        TempData["MESSAGE"] = "The content type was added successfully.";
                        TempData["KEY"] = "success";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["MESSAGE"] = $"Error adding content type - {result.Msg}";
                        TempData["KEY"] = "danger";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error adding content type - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            try
            {
                var spName = "Code.usp_GetLookUpById";
                var inputParameters = new { Id = id, @Table = $"ContentType" };
                var model = (_genericService.Query<ContentTypeLookUpVm>(spName, inputParameters).Data).FirstOrDefault();
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error loading content type - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ContentTypeLookUpVm model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var spName = "Code.usp_New_Add_Content_Type";
                    var inputParameters = new
                    {
                        User_ID = Guid.NewGuid(),
                        Content_Type = model.Content_Type,
                        Content_Type_Code_ID = model.Content_Type_Code_ID,
                        Description = model.Description,
                        Public_Description = model.Public_Description,
                        Required = model.Required,
                        Workflow_Related = model.Workflow_Related,
                        App_Related = model.App_Related,
                        Default_Status_Code_ID = model.Default_Status_Code_ID,
                        Default_Translation_Required = model.Default_Translation_Required,
                        Default_QC_Required = model.Default_QC_Required,
                        Default_Shippable = model.Default_Shippable,
                        Active = model.Active,
                    };

                    var result = _genericService.Query<SpIntVm>(spName, inputParameters);
                    if (result.Code == 200)
                    {
                        TempData["MESSAGE"] = "The content type was updated successfully.";
                        TempData["KEY"] = "success";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["MESSAGE"] = $"Error updating content type - {result.Msg}";
                        TempData["KEY"] = "danger";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error updating content type - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Details(int id)
        {
            try
            {
                var spName = "Code.usp_GetLookUpById";
                var inputParameters = new { Id = id, @Table = $"ContentType" };
                var model = (_genericService.Query<ContentTypeLookUpVm>(spName, inputParameters).Data).FirstOrDefault();

                ViewBag.Title = $"Content Type Details";
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error loading content type - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }
        }


        [HttpGet]
        public async Task<ActionResult> Remove(int id)
        {
            try
            {
                var spName = "Code.usp_GetLookUpById";
                var inputParameters = new { Id = id, @Table = $"ContentType" };
                var model = (_genericService.Query<ContentTypeLookUpVm>(spName, inputParameters).Data).FirstOrDefault();

                ViewBag.Title = $"Remove Content Type";
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error loading content type - {ex.Message}";
                TempData["KEY"] = "danger";
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var _spName = "Code.usp_GetLookUpById";
                    var _inputParameters = new { Id = id, @Table = $"ContentType" };
                    var model = (_genericService.Query<ContentTypeLookUpVm>(_spName, _inputParameters).Data).FirstOrDefault();

                    var spName = "Code.usp_New_Add_Content_Type";
                    var inputParameters = new
                    {
                        User_ID = Guid.NewGuid(),
                        Content_Type = model.Content_Type,
                        Content_Type_Code_ID = model.Content_Type_Code_ID,
                        Description = model.Description,
                        Public_Description = model.Public_Description,
                        Required = model.Required,
                        Workflow_Related = model.Workflow_Related,
                        App_Related = model.App_Related,
                        Default_Status_Code_ID = model.Default_Status_Code_ID,
                        Default_Translation_Required = model.Default_Translation_Required,
                        Default_QC_Required = model.Default_QC_Required,
                        Default_Shippable = model.Default_Shippable,
                        Active = false,
                    };

                    var result = _genericService.Query<SpIntVm>(spName, inputParameters);
                    if (result.Code == 200)
                    {
                        TempData["MESSAGE"] = "The content type was removed successfully.";
                        TempData["KEY"] = "success";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["MESSAGE"] = $"Error removing content type - {result.Msg}";
                        TempData["KEY"] = "danger";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"Error removing content type - {ex.Message}";
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }
    }
}
