﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PANGEA.DAL;
using PANGEA.DAL.Models;
using PANGEA.DAL.ViewModels;
using PANGEA.SERVICES;
using PANGEA.SERVICES.Email;
using PANGEA.SERVICES.Extensions;
using PANGEA.SERVICES.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace PANGEA.WEB.Areas.QC.Controllers
{
    [Area("QC")]
    public class QualityControlController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<UserRole> _roleManager;
        private readonly IUnitOfWork _uow;
        private readonly IEmailSenderEnhance _emailSender;
        public GenericService GenericService;
        public IUserService _userService;
        private readonly ILogService LogService;
        private readonly IConfiguration _config;
        public IEmailSenderEnhance _emailService;

        public QualityControlController(ApplicationDbContext context, IMapper mapper, UserManager<User> userManager,
            SignInManager<User> signInManager,
            RoleManager<UserRole> roleManager,
            IUnitOfWork uow,
            IEmailSenderEnhance emailSender,
            GenericService genericService,
            IUserService userService
            , ILogService logService
            , IConfiguration configuration
            , IEmailSenderEnhance emailService
            )
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _uow = uow;
            _emailSender = emailSender;
            GenericService = genericService;
            _userService = userService;
            LogService = logService;
            _config = configuration;
            _emailService = emailService;
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}
