using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PANGEA.DAL;
using PANGEA.DAL.Models;
using PANGEA.SERVICES;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PANGEA.WEB.Areas.Identity.Pages.Account
{
    public class ChangePasswordModel : PageModel
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly ILogger<ChangePasswordModel> _logger;
        private readonly ApplicationDbContext _context;
        private readonly IDBService _dbService;
        private ILogService LogService;
        private IGenericService GenericService;
        private readonly IConfiguration _config;

        public ChangePasswordModel(SignInManager<User> signInManager,
            ILogger<ChangePasswordModel> logger,
            ApplicationDbContext context,
            IDBService dbService,
            IGenericService uow,
            UserManager<User> userManager,
            LogService logService,
            IConfiguration configuration)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _context = context;
            _dbService = dbService;
            LogService = logService;
            GenericService = uow;
            _config = configuration;
        }
        public class ChangePasswordViewModel
        {

            public string Id { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "New password")]
            public string NewPassword { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm new password")]
            [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Current password")]
            public string OldPassword { get; set; }

        }

       
        public void OnGet(string id)
        {
            //Input.Id = id;
            ViewData["Id"] = id;
        }

        [BindProperty]
        public ChangePasswordViewModel Input { get; set; }
        public async Task<IActionResult> OnPostAsync()
        {
            ChangePasswordViewModel model = Input;
            if (model.NewPassword == model.OldPassword)
            {
                ViewData["Error"] = "The New Password must not be the current password!";
                return Page();
            }

            if (!ModelState.IsValid)
            {
                return Page();
            }


            var user = await GenericService.GetOneAsync<User>(x => x.Id == model.Id);
            var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                try
                {
                    var hashedPassword = _userManager.PasswordHasher.HashPassword(user, model.NewPassword);
                    user.PasswordChangedOn = DateTime.Now;
                    user.PasswordHash = hashedPassword;
                    user.IsActive = true;
                    GenericService.AddOrUpdate(user);

                    TempData["KEY"] = "success";
                    TempData["MESSAGE"] = "Password was successfully changed";

                    return Redirect("~/identity/account/login/");

                }
                catch (Exception exc)
                { 
                    TempData["MESSAGE"] = $"Password reset failed - {exc.Message}";
                    TempData["KEY"] = "danger";

                }

                return Page();
            }

            ModelState.AddModelError(string.Empty, "Current password does not match");
            ViewData["Id"] = Input.Id;
            return Page();
        }

    }
}
