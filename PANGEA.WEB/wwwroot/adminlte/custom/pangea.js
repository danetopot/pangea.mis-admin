﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

var popup = '<div class="modal-dialog modal-default">' +
	'<div class="modal-content">' +
	'<div class="modal-header">' +
	'<button type="button" class="close" data-dismiss="modal">&times;</button>' +
	'<h4 class="modal-title text-center">Loading....</h4>' +
	"</div>" +
	'<div class="modal-body">' +
	'<p class="text-center"> <i class="fa  fa-cog fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> </p>' +
	"</div>" +
	'<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div></div></div>';

$(function () {
	$(document).on("click", ".modal-link", function (e) {
		e.preventDefault();
		$("#popup").html(popup);
		$('#popup').load($(this).attr("href"));
		$('#popup').modal('show');
		$('#popup').modal({ keyboard: false });
	});
});