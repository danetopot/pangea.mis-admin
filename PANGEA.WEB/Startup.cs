using DinkToPdf;
using DinkToPdf.Contracts;
using ElmahCore;
using ElmahCore.Mvc;
using Microsoft.AspNetCore.Authentication.Cookies;
//using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Tokens;
using PANGEA.DAL;
using PANGEA.DAL.Models;
using PANGEA.DAL.ViewModels;
using PANGEA.SERVICES;
using PANGEA.SERVICES.Email;
using PANGEA.SERVICES.Extensions;
using PANGEA.SERVICES.Helpers;
using PANGEA.SERVICES.Interfaces;
using PANGEA.SERVICES.Repository;
using Postal;
using Postal.AspNetCore;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Reflection;

namespace PANGEA.WEB
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<ApplicationDbContext>(options => {
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
                options.EnableSensitiveDataLogging(true);
            });
            services.AddIdentity<User, UserRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();


            // OpenIDConnect
            var ClientId = Configuration.GetSection("OpenIdConnectSettings").GetChildren().FirstOrDefault(x => x.Key == "ClientId").Value;
            var ClientSecret = Configuration.GetSection("OpenIdConnectSettings").GetChildren().FirstOrDefault(x => x.Key == "ClientSecret").Value;
            var Tenant = Configuration.GetSection("OpenIdConnectSettings").GetChildren().FirstOrDefault(x => x.Key == "TenantId").Value;
            var RedirectUri = Configuration.GetSection("OpenIdConnectSettings").GetChildren().FirstOrDefault(x => x.Key == "RedirectUri").Value;
            var Authority = string.Format(System.Globalization.CultureInfo.InvariantCulture
                , Configuration.GetSection("OpenIdConnectSettings").GetChildren().FirstOrDefault(x => x.Key == "Authority").Value
                , Tenant);

            //JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = "oidc";
            })
            .AddCookie(options => {
                options.Cookie.SameSite = SameSiteMode.None;
            })
            .AddOpenIdConnect(options =>
            {
                options.SignInScheme = "Cookies";
                options.Authority = $"{Authority}";
                options.ClientId = $"{ClientId}";
                options.ClientSecret = $"{ClientSecret}";
                options.ResponseType = "code";
                options.SaveTokens = true;
                options.ReturnUrlParameter = RedirectUri;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = false,
                };
            });

            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                    builder =>
                    {
                        builder.WithOrigins("http://localhost:64841/")
                                            .AllowAnyHeader()
                                            .AllowAnyMethod();
                    });
            });


            services.AddMvc().AddJsonOptions(options => {
                options.JsonSerializerOptions.PropertyNamingPolicy = null;
            });

            services.AddAuthorization();
            services.AddRazorPages();



            services.AddSession(options => {
                options.IdleTimeout = TimeSpan.FromHours(1);
                options.Cookie.HttpOnly = true;
            });
            services.AddPostal();
            services.AddScoped<IDBService, DBService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<ILogService, LogService>();
            services.AddScoped<IGenericRepository, GenericRepository<ApplicationDbContext>>();
            services.AddScoped<IGenericService, GenericService>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IUserService, UserService>();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IEmailSenderEnhance, EmailSender>();
            services.AddTransient<GenericRepository<ApplicationDbContext>>();
            services.AddTransient<LogService>();
            services.AddTransient<GenericService>();
            services.AddTransient<DBService>();
            services.AddTransient<EmailService>();

            var config = new AutoMapper.MapperConfiguration(cfg => cfg.AddProfile(new AutoMapperProfile()));
            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);

            // This is for linux
            // string filePath = @$"{(($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}/libwkhtmltox.so").Replace(@"\", @"/"))}";
            string filePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\libwkhtmltox.dll";
            CustomAssemblyLoadContext context = new CustomAssemblyLoadContext();
            context.LoadUnmanagedLibrary(filePath);

            //https://stackoverflow.com/questions/55430532/dinktopdf-net-core-not-able-to-load-dll-files
            services.AddSingleton(typeof(IConverter), new SynchronizedConverter(new PdfTools()));


            var serviceProvider = services.BuildServiceProvider();
            var accessor = serviceProvider.GetService<IHttpContextAccessor>();
            ExtensionMethods.SetHttpContextAccessor(accessor);

            services.AddElmah<XmlFileErrorLog>(options =>
            {
                options.LogPath = "~/logs";
            });

            services.AddHttpsRedirection(options =>
            {
                //  options.RedirectStatusCode = StatusCodes.Status307TemporaryRedirect;
                options.HttpsPort = 443;
            });
            services.AddScoped<IUserClaimsPrincipalFactory<User>, MyUserClaimsPrincipalFactory>();
            services.AddRouting(options => options.LowercaseUrls = true);
            services.ConfigureApplicationCookie(options =>
            {
                options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                options.LoginPath = "/Identity/Account/Login";
                options.Cookie.Name = Configuration.GetSection("Settings").GetChildren().FirstOrDefault(x => x.Key == "Cookie_Name")?.Value ?? "PANGEAMIS.Web";
                options.Cookie.HttpOnly = bool.Parse(Configuration.GetSection("Settings").GetChildren().FirstOrDefault(x => x.Key == "Cookie_HttpOnly")?.Value ?? "true");
                options.ExpireTimeSpan = TimeSpan.FromMinutes(int.Parse(Configuration.GetSection("Settings").GetChildren().FirstOrDefault(x => x.Key == "ExpireTimeSpan")?.Value ?? "10"));
                options.SlidingExpiration = bool.Parse(Configuration.GetSection("Settings").GetChildren().FirstOrDefault(x => x.Key == "SlidingExpiration")?.Value ?? "true");
            });

            services.AddSingleton<IAuthorizationHandler, PermissionAuthorizationHandler>();
            services.AddAuthorization(options =>
            {
                options.AddPolicy("Permission", policyBuilder =>
                {
                    policyBuilder.Requirements.Add(new PermissionAuthorizationRequirement());
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();

            app.UseStaticFiles();

            FileExtensionContentTypeProvider contentTypes = new FileExtensionContentTypeProvider();
            contentTypes.Mappings[".apk"] = "application/vnd.android.package-archive";
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Directory.GetCurrentDirectory()),
                ServeUnknownFileTypes = true,
                ContentTypeProvider = contentTypes
            });


            //
            app.UseSession();
            app.UseRouting();
            app.UseCors(); 
            app.UseHttpsRedirection();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "adminRoute",
                    pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    }
}
