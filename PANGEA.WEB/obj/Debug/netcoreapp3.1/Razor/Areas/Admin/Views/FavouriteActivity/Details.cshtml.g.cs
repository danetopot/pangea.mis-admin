#pragma checksum "C:\Users\Danet Opot\Desktop\PROJECTS\FEED\PANGEA MIS (Admin)\PANGEA.WEB\Areas\Admin\Views\FavouriteActivity\Details.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c5cc84b65b484e5436a09dd139a588c214631ee8"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Admin_Views_FavouriteActivity_Details), @"mvc.1.0.view", @"/Areas/Admin/Views/FavouriteActivity/Details.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Danet Opot\Desktop\PROJECTS\FEED\PANGEA MIS (Admin)\PANGEA.WEB\Areas\Admin\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Danet Opot\Desktop\PROJECTS\FEED\PANGEA MIS (Admin)\PANGEA.WEB\Areas\Admin\Views\_ViewImports.cshtml"
using PANGEA.WEB;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Danet Opot\Desktop\PROJECTS\FEED\PANGEA MIS (Admin)\PANGEA.WEB\Areas\Admin\Views\_ViewImports.cshtml"
using PANGEA.WEB.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Danet Opot\Desktop\PROJECTS\FEED\PANGEA MIS (Admin)\PANGEA.WEB\Areas\Admin\Views\_ViewImports.cshtml"
using Microsoft.Extensions.Configuration;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "C:\Users\Danet Opot\Desktop\PROJECTS\FEED\PANGEA MIS (Admin)\PANGEA.WEB\Areas\Admin\Views\FavouriteActivity\Details.cshtml"
using X.PagedList.Mvc.Core;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c5cc84b65b484e5436a09dd139a588c214631ee8", @"/Areas/Admin/Views/FavouriteActivity/Details.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fa43969238d8957ad1fd1532a1d2948fd61b8d0a", @"/Areas/Admin/Views/_ViewImports.cshtml")]
    public class Areas_Admin_Views_FavouriteActivity_Details : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<PANGEA.DAL.ViewModels.FavouriteActivityLookUpVm>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n\r\n");
#nullable restore
#line 5 "C:\Users\Danet Opot\Desktop\PROJECTS\FEED\PANGEA MIS (Admin)\PANGEA.WEB\Areas\Admin\Views\FavouriteActivity\Details.cshtml"
  
    Layout = "~/Views/Shared/_LayoutModal.cshtml"; 

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n<div class=\"modal-body\">\r\n    <table class=\"table table-bordered table-sm\">\r\n        <tbody>\r\n            <tr>\r\n                <th>Favourite Activity</th>\r\n                <td class=\"text-teal-900\"><strong>");
#nullable restore
#line 14 "C:\Users\Danet Opot\Desktop\PROJECTS\FEED\PANGEA MIS (Admin)\PANGEA.WEB\Areas\Admin\Views\FavouriteActivity\Details.cshtml"
                                             Write(Model.Favorite_Activity);

#line default
#line hidden
#nullable disable
            WriteLiteral("</strong></td>\r\n            </tr>\r\n            <tr>\r\n                <th width=\"30%\">Description</th>\r\n                <td class=\"text-teal-900\">");
#nullable restore
#line 18 "C:\Users\Danet Opot\Desktop\PROJECTS\FEED\PANGEA MIS (Admin)\PANGEA.WEB\Areas\Admin\Views\FavouriteActivity\Details.cshtml"
                                     Write(Model.Description);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n            </tr>\r\n            <tr>\r\n                <th width=\"30%\">Public Description</th>\r\n                <td class=\"text-teal-900\">");
#nullable restore
#line 22 "C:\Users\Danet Opot\Desktop\PROJECTS\FEED\PANGEA MIS (Admin)\PANGEA.WEB\Areas\Admin\Views\FavouriteActivity\Details.cshtml"
                                     Write(Model.Public_Description);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n            </tr>\r\n");
            WriteLiteral("            <tr>\r\n                <th width=\"30%\">Is Active?</th>\r\n                <td class=\"text-teal-900\">");
#nullable restore
#line 42 "C:\Users\Danet Opot\Desktop\PROJECTS\FEED\PANGEA MIS (Admin)\PANGEA.WEB\Areas\Admin\Views\FavouriteActivity\Details.cshtml"
                                     Write(Html.DisplayFor(modelItem => Model.Active));

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n            </tr>\r\n        </tbody>\r\n    </table>\r\n    <br />\r\n</div>\r\n\r\n\r\n<div class=\"modal-footer\">\r\n    <button type=\"button\" class=\"btn btn-dark\" data-bs-dismiss=\"modal\"> Close</button>\r\n</div>\r\n\r\n\r\n\r\n");
            DefineSection("scripts", async() => {
                WriteLiteral("\r\n    <script type=\"text/javascript\">\r\n    </script>\r\n");
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public IConfiguration Configuration { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<PANGEA.DAL.ViewModels.FavouriteActivityLookUpVm> Html { get; private set; }
    }
}
#pragma warning restore 1591
