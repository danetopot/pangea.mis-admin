﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using PANGEA.DAL.Models;

namespace PANGEA.SERVICES.Interfaces


{
    public interface IEntity
    {
        [HiddenInput]
        object Id { get; set; }

        [Required]
        string CreatedById { get; set; }

        [Required]
        DateTime CreatedOn { get; set; }

        [Required]
        User CreatedBy { get; set; }

        string ModifiedById { get; set; }

        DateTime? ModifiedDate { get; set; }

        User ModifiedBy { get; set; }
    }

    public interface IEntity<T> : IEntity
    {
        new T Id { get; set; }
    }
}