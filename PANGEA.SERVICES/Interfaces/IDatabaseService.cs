﻿using System;

namespace PANGEA.SERVICES.Interfaces

{
    public interface IDatabaseService : IDisposable
    {
        string GetComputerName(string clientIP);

        string GetExternalIP();
    }
}