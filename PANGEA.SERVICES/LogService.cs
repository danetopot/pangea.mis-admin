﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using PANGEA.DAL;
using PANGEA.DAL.Models;
using PANGEA.DAL.ViewModels;

namespace PANGEA.SERVICES
{
    public interface ILogService
    {
        void DBLog(string type, string exc, string category, string item, string UserId);
        void FileLog(string dataType, string data);
        void FileLog(string dataType, string dataType2, string data);
        void FileLog(string dataType, string childInfo, string childChoreInfo, string childFavouriteActivityInfo, string childDocumentInfo, string childMLEInfo, string deviceInfo, string UserId);
        void LogWrite(string type, string exc, string category, string item, string UserId);
        void FPLog(string data, string childid);
    }

    public class LogService : ILogService
    {
        private string path = "";
        private readonly IWebHostEnvironment _hostingEnvironment;

        private string dBPath = "";
        readonly IGenericService GenericService;
        public LogService(IWebHostEnvironment hostingEnvironment, GenericService genericService)
        {
            GenericService = genericService;
            _hostingEnvironment = hostingEnvironment;
            string webRootPath = _hostingEnvironment.WebRootPath;
            path = webRootPath + "/Logs/";
            dBPath = webRootPath + "/Uploads/";
        }

        public void DBLog(string type, string exc, string category, string item, string UserId)
        {
        }


        public void FileLog(string dataType, string data)
        {
            string t;
            int seconds;
            string todaydate, hour;
            var dt = DateTime.Now;
            seconds = dt.Second;
            todaydate = dt.Date.ToString("yyyy-MM-dd");
            var minute = dt.Date.ToString("mm");
            hour = dt.TimeOfDay.Hours.ToString();
            if (!Equals(seconds, dt.Second))
            {
                seconds = dt.Second;
            }
            t = dt.ToString("T");
            var fs = new FileStream($"{this.path}{dataType}-{todaydate}-{hour}-00.txt", FileMode.OpenOrCreate, FileAccess.Write);
            using (var mStreamWriter = new StreamWriter(fs))
            {
                mStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                mStreamWriter.WriteLine("||            Date: {0}   Time : {1}", todaydate, t);

                // mStreamWriter.WriteLine("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
                mStreamWriter.WriteLine(
                    " ****************************************************************************************************************************");
                mStreamWriter.WriteLine(" POSTED DATA   " + data);
                mStreamWriter.WriteLine(
                    " ****************************************************************************************************************************");

                mStreamWriter.Flush();
                mStreamWriter.Close();
            }
        }

        public void FileLog(string dataType, string childInfo, string childChoreInfo, string childFavouriteActivityInfo, string childDocumentInfo, string childMLEInfo, string deviceInfo, string UserId)
        {
            string t;
            int seconds;
            string todaydate, hour;
            var dt = DateTime.Now;
            seconds = dt.Second;
            todaydate = dt.Date.ToString("yyyy-MM-dd");
            var minute = dt.Date.ToString("mm");
            hour = dt.TimeOfDay.Hours.ToString();
            if (!Equals(seconds, dt.Second))
            {
                seconds = dt.Second;
            }
            t = dt.ToString("T");
            var dateposted = $"Date: {todaydate}  Time : {t}".ToUpper();
            var fs = new FileStream($"{this.path}{dataType}-{todaydate}-{hour}-00.txt", FileMode.OpenOrCreate, FileAccess.Write);
            using (var mStreamWriter = new StreamWriter(fs))
            {
                mStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                mStreamWriter.WriteLine("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
                mStreamWriter.WriteLine("POSTED DATA  " + dateposted);
                mStreamWriter.WriteLine("USER DATA  " + UserId + "\n");
                mStreamWriter.WriteLine($"Child  ".ToUpper() + $"{childInfo}");
                mStreamWriter.WriteLine($"Chores  ".ToUpper() + $"{childChoreInfo}");
                mStreamWriter.WriteLine($"Favourite Activities  ".ToUpper() + $"{childFavouriteActivityInfo}");
                mStreamWriter.WriteLine($"Documents  ".ToUpper() + $"{childDocumentInfo}");
                mStreamWriter.WriteLine($"MLEs  ".ToUpper() + $"{childMLEInfo}");
                mStreamWriter.WriteLine($"Device  ".ToUpper() + $"{deviceInfo}");
                mStreamWriter.WriteLine("****************************************************************************************************************************\n");
                mStreamWriter.Flush();
                mStreamWriter.Close();
            }
        }

        public void FileLog(string dataType, string dataType2, string data)
        {
            string t;
            int seconds;
            string todaydate, hour;
            var dt = DateTime.Now;
            seconds = dt.Second;
            todaydate = dt.Date.ToString("yyyy-MM-dd");
            var minute = dt.Date.ToString("mm");
            hour = dt.TimeOfDay.Hours.ToString();
            if (!Equals(seconds, dt.Second))
            {
                seconds = dt.Second;
            }
            t = dt.ToString("T");
            var dateposted = $"Date: {todaydate}  Time : {t}".ToUpper();
            var fs = new FileStream($"{this.path}{dataType}-{todaydate}-{hour}-00.txt", FileMode.OpenOrCreate, FileAccess.Write);
            using (var mStreamWriter = new StreamWriter(fs))
            {
                mStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                mStreamWriter.WriteLine("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
                mStreamWriter.WriteLine(dateposted + "   " + data.ToUpper() );
                mStreamWriter.WriteLine("POSTED DATA  " + dataType2);
                mStreamWriter.WriteLine(
                   " ****************************************************************************************************************************");
                mStreamWriter.Flush();
                mStreamWriter.Close();
            }
        }
        public void LogWrite(string type, string exc, string category, string item, string UserId)
        {
            string t;
            int seconds;
            string todaydate, hour;
            var dt = DateTime.Now;
            seconds = dt.Second;
            todaydate = dt.Date.ToString("yyyy-MM-dd");
            var minute = dt.Date.ToString("mm");
            hour = dt.TimeOfDay.Hours.ToString();
            if (!Equals(seconds, dt.Second))
            {
                seconds = dt.Second;
            }

            t = dt.ToString("T");
            var fs = new FileStream(
                $"{this.path}-{todaydate}-{hour}-00.txt",
                FileMode.OpenOrCreate,
                FileAccess.Write);
            using (var mStreamWriter = new StreamWriter(fs))
            {
                mStreamWriter.BaseStream.Seek(0, SeekOrigin.End);

                mStreamWriter.WriteLine("");
                mStreamWriter.WriteLine(exc);

                mStreamWriter.WriteLine("");
                mStreamWriter.WriteLine(exc);

                mStreamWriter.Flush();
                mStreamWriter.Close();
            }
        }

        public void FPLog(string data, string childid)
        {

            var fs = new FileStream($"{dBPath}-{childid}.json", FileMode.OpenOrCreate, FileAccess.Write);
            using (var mStreamWriter = new StreamWriter(fs))
            {
                mStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                mStreamWriter.WriteLine(data);
                mStreamWriter.Flush();
                mStreamWriter.Close();
            }
        }


        public void MailLog(string type, string exc, string category, string item, string UserId)
        {
        }



        public string GetMacAddress(string ipAddress)
        {
            string macAddress;
            System.Diagnostics.Process pProcess = new System.Diagnostics.Process
            {
                StartInfo =
                {
                    FileName = "arp",
                    Arguments = "-a " + ipAddress,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };
            pProcess.Start();
            string strOutput = pProcess.StandardOutput.ReadToEnd();
            string[] substrings = strOutput.Split('-');
            if (substrings.Length >= 8)
            {
                macAddress = substrings[3].Substring(Math.Max(0, substrings[3].Length - 2))
                             + "-" + substrings[4] + "-" + substrings[5] + "-" + substrings[6]
                             + "-" + substrings[7] + "-"
                             + substrings[8].Substring(0, 2);
                return macAddress;
            }
            else
            {
                return "";
            }
        }

        private string GetMacAddress()
        {

            string MacAddress = string.Empty;

            try
            {
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

                MacAddress = Convert.ToString(nics[0].GetPhysicalAddress());

                return MacAddress;
            }
            catch (ArgumentNullException)
            {
                //  lblErrorMsg.Text = "Application Error : " + Exc.Message;
                return MacAddress;
            }
            catch (InvalidCastException)
            {

                return MacAddress;
            }
            catch (InvalidOperationException)
            {

                return MacAddress;
            }
            catch (NullReferenceException)
            {
                return MacAddress;
            }
            catch (Exception)
            {
                return MacAddress;
            }

        }

        private string GetComputerName(string clientIP)
        {
            try
            {
                var hostEntry = Dns.GetHostEntry(clientIP);
                return hostEntry.HostName;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        private static string GetExternalIP()
        {
            try
            {
                string externalIP;
                externalIP = (new WebClient()).DownloadString("http://checkip.dyndns.org/");
                externalIP = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"))
                    .Matches(externalIP)[0].ToString();
                return externalIP;
            }
            catch { return null; }
        }
    }
}
