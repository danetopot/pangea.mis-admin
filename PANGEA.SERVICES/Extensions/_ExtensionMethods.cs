﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using Dapper;
using PANGEA.DAL;
using Microsoft.AspNetCore.Identity;
using PANGEA.DAL.Models;
using System.Threading.Tasks;

namespace PANGEA.SERVICES.Extensions
{

    public static partial class ExtensionMethods
    {
        private static ApplicationDbContext _context;
        private static IHttpContextAccessor _http;
        private static UserManager<User> _userManager;


        public static void SetHttpContextAccessor(IHttpContextAccessor accessor)
        {
            _http = accessor;
        }
        public static string GetUserId(this ClaimsPrincipal user)
        {
            //if (!user.Identity.IsAuthenticated)
            //    return null;

            //ClaimsPrincipal currentUser = user;
            //return currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (!user.Identity.IsAuthenticated)
                return null;

            ClaimsPrincipal currentUser = user;
            var UserId = currentUser.FindFirstValue("UserId");
            return UserId;
        }
        
        public static string GetDisplayName(this ClaimsPrincipal user)
        {
            if (!user.Identity.IsAuthenticated)
                return null;

            ClaimsPrincipal currentUser = user;
            var displayName = currentUser.FindFirstValue("DisplayName");
            return displayName;
        }
        public static string GetProfile(this ClaimsPrincipal user)
        {
            if (!user.Identity.IsAuthenticated)
                return null;

            ClaimsPrincipal currentUser = user;
            var profile = currentUser.FindFirst("UserProfile").Value;
            return profile;
        }
        public static UserSummary GetUserSummary(this ClaimsPrincipal user)
        {
            var HttpContext = _http.HttpContext;
            if (!HttpContext.User.Identity.IsAuthenticated)
                return new UserSummary();
            //var userId = int.Parse(HttpContext.User.GetUserId());
            var userId = HttpContext.User.GetUserId();
            if (HttpContext.Session.GetString($"USER_SUMMARY_{userId}") != null)
            {

                return JsonConvert.DeserializeObject<UserSummary>(HttpContext.Session.GetString($"USER_SUMMARY_{userId}")) ;
            }

            UserSummary data;
            using (IDbConnection db = new SqlConnection(DBService.DBConnection()))
            {
                 data = db.Query<UserSummary>("GetUserProfile ",
                        new
                        {
                            UserId = userId
                        },
                        commandType: System.Data.CommandType.StoredProcedure)
                    .FirstOrDefault();
                 var strUserSummary = JsonConvert.SerializeObject(data);
                 HttpContext.Session.SetString($"USER_SUMMARY_{userId}", strUserSummary);
            }
           
          
            return data;
        }
        

    }
   public static partial class ExtensionMethods
    {
        public static string GetUserEmail(this ClaimsPrincipal principal)
        {
            if (!principal.Identity.IsAuthenticated)
                return "";
            return principal.FindFirstValue(ClaimTypes.Email);
        }

       
        public static string GetUserName(this ClaimsPrincipal principal)
        {
            if (!principal.Identity.IsAuthenticated)
                return null;
            return principal.FindFirstValue(ClaimTypes.Email);
        }

        public static bool IsCurrentUser(this ClaimsPrincipal principal, string id)
        {
            if (!principal.Identity.IsAuthenticated)
                return false;
            var currentUserId = GetUserId(principal);

            return string.Equals(currentUserId, id, StringComparison.OrdinalIgnoreCase);
        }

         

        public static string GetDisplayCode(this ClaimsPrincipal user)
        {
            if (!user.Identity.IsAuthenticated)
                return null;

            ClaimsPrincipal currentUser = user;
            return $"{currentUser.FindFirst(ClaimTypes.Anonymous).Value}";
        }

        public static int? GetPspId(this ClaimsPrincipal user)
        {
            if (!user.Identity.IsAuthenticated)
                return null;
            ClaimsPrincipal currentUser = user;

            string d = currentUser.FindFirst("PspId")?.Value;

            try
            {
                return int.Parse(d);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public static string GetPspCode(this ClaimsPrincipal user)
        {
            if (!user.Identity.IsAuthenticated)
                return null;
            ClaimsPrincipal currentUser = user;

            string d = currentUser.FindFirst("PspCode")?.Value;

            try
            {
                return d;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static bool IsUserActive(this ClaimsPrincipal user)
        {
            if (!user.Identity.IsAuthenticated)
                return false;

            ClaimsPrincipal currentUser = user;

            bool i = false;
            bool d = (bool.TryParse(currentUser.FindFirst("IsActive").Value, out i) && i);

            try
            {
                return d;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static long? GetUserRegionId(this ClaimsPrincipal user)
        {
            if (!user.Identity.IsAuthenticated)
                return null;

            ClaimsPrincipal currentUser = user;

            int i = 0;
            int? d = (Int32.TryParse(currentUser.FindFirst("RegionId").Value, out i) ? i : (int?)null);

            try
            {
                return d;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static string GetUserIPAddress(this ClaimsPrincipal user)
        {
            Ping ping = new Ping();
            var replay = ping.Send(Dns.GetHostName());

            if (replay.Status == IPStatus.Success)
            {
                return replay.Address.ToString();
            }
            return null;
        }

        public static string GetUserAgent(this ClaimsPrincipal user)
        {
            var _http = new HttpContextAccessor();

            return _http.HttpContext.Request.Headers["User-Agent"].ToString();
        }

        public static string GetUserRemoteAddress(this ClaimsPrincipal user)
        {
            var _http = new HttpContextAccessor();

            return _http.HttpContext.Connection.RemoteIpAddress.ToString();
        }

        public static DateTime ConvertFromDateTimeOffset(this ClaimsPrincipal user, DateTimeOffset? dateTime)
        {
            if (dateTime.HasValue)
            {
                if (dateTime.Value.Offset.Equals(TimeSpan.Zero))
                    return dateTime.Value.UtcDateTime;
                else if (dateTime.Value.Equals(TimeZoneInfo.Local.GetUtcOffset(dateTime.Value.DateTime)))
                    return DateTime.SpecifyKind(dateTime.Value.DateTime, DateTimeKind.Local);
                else
                    return dateTime.Value.DateTime;
            }
            else
            {
                return DateTime.UtcNow;
            }
        }
    }

   public class UserSummary
   {
       public string Avatar { get; set; }

       [Display(Name = "Avatar")]
       public string DisplayAvatar => string.IsNullOrEmpty(Avatar) ? "avatar.png" : Avatar;

       [Display(Name = "Display Names")]
       public string DisplayName => $"{FirstName} {MiddleName}";

       public string Email { get; set; }

       public string FirstName { get; set; }

       [Display(Name = "Full names")]
       public string FullName => $"{FirstName} {MiddleName} {Surname}";
       public string MiddleName { get; set; }
       [Display(Name = "Name")]
       public string Name => $"{FirstName}  {MiddleName}";
       public string PhoneNumber { get; set; }
       public List<string> Roles { get; set; }
       public string Surname { get; set; }
       public int UserGroupId { get; set; }
       string UserId { get; set; }
   }
    public static class LinqEx
    {
        public static IEnumerable<TResult> LeftOuterJoin<TOuter, TInner, TKey, TResult>(
            this IEnumerable<TOuter> outer,
            IEnumerable<TInner> inner,
            Func<TOuter, TKey> outerKeySelector,
            Func<TInner, TKey> innerKeySelector,
            Func<TOuter, TInner, TResult> resultSelector)
        {
            return outer
                .GroupJoin(inner, outerKeySelector, innerKeySelector, (a, b) => new {
                    a,
                    b
                })
                .SelectMany(x => x.b.DefaultIfEmpty(), (x, b) => resultSelector(x.a, b));
        }
    }
    public static class EasyMD5
    {
        private static string GetMd5Hash(byte[] data)
        {
            var sBuilder = new StringBuilder();
            for (var i = 0; i < data.Length; i++)
                sBuilder.Append(data[i].ToString("x2"));
            return sBuilder.ToString();
        }
        private static bool VerifyMd5Hash(byte[] data, string hash)
        {
            return 0 == StringComparer.OrdinalIgnoreCase.Compare(GetMd5Hash(data), hash);
        }
        public static string Hash(string data)
        {
            using (var md5 = MD5.Create())
                return GetMd5Hash(md5.ComputeHash(Encoding.UTF8.GetBytes(data)));
        }
        public static string Hash(FileStream data)
        {
            using (var md5 = MD5.Create())
                return GetMd5Hash(md5.ComputeHash(data));
        }
        public static bool Verify(string data, string hash)
        {
            using (var md5 = MD5.Create())
                return VerifyMd5Hash(md5.ComputeHash(Encoding.UTF8.GetBytes(data)), hash);
        }
        public static bool Verify(FileStream data, string hash)
        {
            using (var md5 = MD5.Create())
                return VerifyMd5Hash(md5.ComputeHash(data), hash);
        }
        public static DataTable CreateDataTable(IEnumerable source)
        {
            var table = new DataTable();
            var index = 0;
            var properties = new List<PropertyInfo>();
            foreach (var obj in source)
            {
                if (index == 0)
                {
                    foreach (var property in obj.GetType().GetProperties())
                    {
                        if (Nullable.GetUnderlyingType(property.PropertyType) != null)
                        {
                            continue;
                        }

                        properties.Add(property);
                        table.Columns.Add(new DataColumn(property.Name, property.PropertyType));
                    }
                }

                var values = new object[properties.Count];
                for (var i = 0; i < properties.Count; i++)
                {
                    values[i] = properties[i].GetValue(obj);
                }
                table.Rows.Add(values);
                index++;
            }
            return table;
        }
    }
    public static class Statics
    {
        public static DataTable ToDataTable(IEnumerable source)
        {
            var table = new DataTable();
            var index = 0;
            var properties = new List<PropertyInfo>();
            foreach (var obj in source)
            {
                if (index == 0)
                {
                    foreach (var property in obj.GetType().GetProperties())
                    {
                        if (Nullable.GetUnderlyingType(property.PropertyType) == null)
                        {
                            properties.Add(property);
                            table.Columns.Add(new DataColumn(property.Name, property.PropertyType));
                        }
                    }
                }
                var values = new object[properties.Count];
                for (var i = 0; i < properties.Count; i++)
                    values[i] = properties[i].GetValue(obj);
                table.Rows.Add(values);
                index++;
            }
            return table;
        }
    }
}
