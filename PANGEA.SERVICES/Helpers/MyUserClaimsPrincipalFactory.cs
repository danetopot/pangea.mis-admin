﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using PANGEA.DAL;
using PANGEA.DAL.Models;

namespace PANGEA.SERVICES.Helpers
{
    public class MyUserClaimsPrincipalFactory : UserClaimsPrincipalFactory<User, UserRole>
    {
        private readonly UserManager<User> _userManager;
        private readonly ApplicationDbContext _context;
        public MyUserClaimsPrincipalFactory(
            UserManager<User> userManager,
            RoleManager<UserRole> roleManager,
            ApplicationDbContext context,
            IOptions<IdentityOptions> optionsAccessor)
            : base(userManager, roleManager, optionsAccessor)
        {
            _userManager = userManager;
            _context = context;
        }

        protected override async Task<ClaimsIdentity> GenerateClaimsAsync(User user)
        {
            var identity = await base.GenerateClaimsAsync(user);

            var userRoles = await _userManager.GetRolesAsync(user);
            var role = userRoles.FirstOrDefault();
            var userProfiles = "";
            if (role != null)
            {
                var userRoleId = _context.Roles.Single(i => i.Name == role).Id;
                var userprofiles = _context.UserRoleProfiles.Where(p => p.RoleId == userRoleId)
                    .Select(p => p.Task.Parent.Name + ":" + p.Task.Name).ToList();

                foreach (var task in userprofiles)
                {
                    userProfiles += $"|{task?.ToUpper()}";
                }
            }

            identity.AddClaim(new Claim("UserId", user.UserId.ToString()));
            identity.AddClaim(new Claim("UserProfile", userProfiles));
            identity.AddClaim(new Claim("DisplayName", user.FullName));

            return identity;
        }
    }
}
