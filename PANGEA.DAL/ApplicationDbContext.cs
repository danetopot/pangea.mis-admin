﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using PANGEA.DAL.Models;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PANGEA.DAL
{
    public class ApplicationDbContext : IdentityDbContext<User, UserRole, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<LookUps> LookUps { get; set; }
        public DbSet<SystemTask> SystemTasks { get; set; }
        public DbSet<UserRoleProfile> UserRoleProfiles { get; set; }
        public DbSet<UserLocation> UserLocations { get; set; }

        public override int SaveChanges()
        {
            var entities = from e in ChangeTracker.Entries()
                           where e.State == EntityState.Added
                                 || e.State == EntityState.Modified
                           select e.Entity;
            foreach (var entity in entities)
            {
                var validationContext = new ValidationContext(entity);
                Validator.ValidateObject(entity, validationContext);
            }

            return base.SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LookUps>().HasKey(p => new { p.Lkp_ID });
            modelBuilder.Entity<User>().HasIndex(mr => new { mr.UserName }).IsUnique();
            //modelBuilder.Entity<User>().Property(u => u.UserId).ValueGeneratedOnUpdate().Metadata.SetAfterSaveBehavior(PropertySaveBehavior.Ignore);
            modelBuilder.Entity<UserRoleProfile>().HasKey(c => new { c.RoleId, c.TaskId });

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }


            #region FIX decimal property
            // this will mean we do not neeed to decorate all the decimals
            var fixDecimalDatas = new List<Tuple<Type, Type, string>>();
            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
                foreach (var property in entityType.GetProperties())
                    if (Type.GetTypeCode(property.ClrType) == TypeCode.Decimal)
                        fixDecimalDatas.Add(new Tuple<Type, Type, string>(entityType.ClrType, property.ClrType,
                            property.GetColumnName()));

            foreach (var item in fixDecimalDatas)
                modelBuilder.Entity(item.Item1).Property(item.Item2, item.Item3).HasColumnType("decimal(18,4)");

            #endregion FIX decimal property

            base.OnModelCreating(modelBuilder);
        }
    }
}
