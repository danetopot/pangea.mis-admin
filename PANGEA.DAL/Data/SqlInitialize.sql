USE [PANGEAMIS]
GO

/******        System Roles      ******/
INSERT INTO [dbo].[AspNetRoles]([Id],[Description],[Name],[NormalizedName],[ConcurrencyStamp],[IsExternal],[CreatedById],[CreatedOn])
VALUES('1222690a-7ba4-453f-9572-36725e6ae9b0','Administrator','Admin','ADMINISTRATOR',NULL,0,NULL,GETDATE())

INSERT INTO [dbo].[AspNetRoles]([Id],[Description],[Name],[NormalizedName],[ConcurrencyStamp],[IsExternal],[CreatedById],[CreatedOn])
VALUES('1222690a-7ba4-453f-9572-36725e6ae9b1','Guest','Guest','GUEST',NULL,1,NULL,GETDATE())

INSERT INTO [dbo].[AspNetRoles]([Id],[Description],[Name],[NormalizedName],[ConcurrencyStamp],[IsExternal],[CreatedById],[CreatedOn])
VALUES('1222690a-7ba4-453f-9572-36725e6ae9b2','Program Officer','Program Officer','PROGRAM OFFICER',NULL,0,NULL,GETDATE())

INSERT INTO [dbo].[AspNetRoles]([Id],[Description],[Name],[NormalizedName],[ConcurrencyStamp],[IsExternal],[CreatedById],[CreatedOn])
VALUES('1222690a-7ba4-453f-9572-36725e6ae9b3','Program Manager','Program Manager','PROGRAM MANAGER',NULL,0,NULL,GETDATE())

INSERT INTO [dbo].[AspNetRoles]([Id],[Description],[Name],[NormalizedName],[ConcurrencyStamp],[IsExternal],[CreatedById],[CreatedOn])
VALUES('1222690a-7ba4-453f-9572-36725e6ae9b4','Developer','Developer','DEVELOPER',NULL,1,NULL,GETDATE())

/******        Users       ******/
INSERT INTO [dbo].[AspNetUsers]([Id],[UserId],[Title],[FirstName],[MiddleName],[Surname],[Email],[EmailConfirmed],[RoleId],[PasswordChangedOn],[PasswordHash],[SecurityStamp]
           ,[Department],[IsLocked],[IsActive],[CreatedById],[CreatedOn],[ModifiedById],[ModifiedOn]
           ,[DeactivateDate],[UserName],[NormalizedUserName],[NormalizedEmail],[ConcurrencyStamp],[PhoneNumber],[PhoneNumberConfirmed]
           ,[TwoFactorEnabled],[LockoutEnd],[LockoutEnabled],[AccessFailedCount])
VALUES('1222690a-7ba4-453f-9572-36725e6ae9b5',539,'Developer','Danet','Ochieng', 'Opot','danetopot@gmail.com',1,'1222690a-7ba4-453f-9572-36725e6ae9b4',GETDATE(),'AHdy2V3/xoXVIi4g4SY9N41nv4e/rXoa7YEwY8KN7OhEXShT1IdiX5azjHRBZBpz8A==',NULL,
'Feed IT', 0, 1, NULL, GETDATE(), NULL, NULL, NULL, 'danetopot@gmail.com','DANETOPOT@GMAIL.COM','DANETOPOT@GMAIL.COM',NULL, '0729681970', 1, 0, NULL, 0, 0)