USE [PANGEAADMIN]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetLookUpById]    Script Date: 3/4/2022 9:54:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[usp_GetLookUpById]
    @Id int = null, @Table varchar(100) = null
AS
BEGIN 
	/** 3.  REGION **/
	IF @Table = 'Region'
	BEGIN
		SELECT 
		[Active], [Action_ID], [Action_User], [Action_Date], [Action_Reason], [TS], [TSLONG], [TSMAN] ,[Region_Code_ID] 
		,[Region_Code], [Region_Name], [Default_Language], [BlackBaud_Guid], [Blackbaud_Uploaded]
		FROM [Code].[Region] WHERE [Region_Code_ID] =@Id
	END

	/** 3.  COUNTRY **/
	IF @Table = 'Country'
	BEGIN
		SELECT 
		T1.[Active], T1.[Action_ID],T1.[Action_User],T1.[Action_Date],T1.[Action_Reason],T1.[TS]
       ,T1.[TSLONG],T1.[TSMAN],T1.[Country_Code_ID],T1.[Country_Code],T2.Region_Name,T1.[Region_Code_ID],T1.[Country_Name],T1.[Default_Language],T1.[Min_School_Age],T1.[BlackBaud_Guid],T1.[Blackbaud_Uploaded]
	  FROM  [Code].[Country] T1 
	  INNER JOIN [Code].[Region] T2 ON T1.Region_Code_ID = T2.Region_Code_ID WHERE T1.[Country_Code_ID] =@Id
	END

	/** 3.  LOCATION **/
	IF @Table = 'Location'
	BEGIN
		SELECT 
		T1.[Active], T1.[Action_ID], T1.[Action_User], T1.[Action_Date], T1.[Action_Reason], T1.[TS], T1.[TSLONG], T1.[TSMAN], T1.[Location_Code_ID], T1.[Location_Name],
		T2.Country_Name, T1.[Country_Code_ID], T1.[Default_Language], T1.[Sponsorship_Site], T1.[BlackBaud_Guid], T1.[Blackbaud_Uploaded]
		FROM [Code].[Location] T1
		INNER JOIN [Code].Country T2 ON T1.Country_Code_ID = T2.Country_Code_ID
		WHERE [Location_Code_ID] =@Id
	END

	/** 4.  GENDER **/
	IF @Table = 'Gender'
	BEGIN
		SELECT
	 [Active],[Action_ID],[Action_User],[Action_Date],[Action_Reason],[TS]
      ,[TSLONG],[TSMAN],[Gender_Code_ID],[Gender],[Description] ,[Public_Description]
		FROM [Code].[Gender] WHERE [Gender_Code_ID] =@Id
	END

	/** 5.  CHORE **/
	IF @Table = 'Chore'
	BEGIN
		SELECT 
	[Active],[Action_ID],[Action_User],[Action_Date],[Action_Reason],[TS],[TSLONG]
      ,[TSMAN],[Chore_Code_ID],[Chore] ,[Description] ,[Public_Description]
      ,[Min_Age],[BlackBaud_Guid],[Blackbaud_Uploaded]
  FROM [Code].[Chore] WHERE [Chore_Code_ID] =@Id
	END

	/** 6.  LIVESWITH **/
	IF @Table = 'LivesWith'
	BEGIN
		SELECT [Active],[Action_ID],[Action_User],[Action_Date],[Action_Reason],[TS],[TSLONG],[TSMAN],[Lives_With_Code_ID]
      ,[Lives_With],[Description],[Public_Description],[Min_Age],[BlackBaud_Guid] ,[Blackbaud_Uploaded]
  FROM [Code].[Lives_With] WHERE [Lives_With_Code_ID] =@Id
	END


	/** 7.  PERSONALITY TYPE **/
	IF @Table = 'Personality'
	BEGIN
		SELECT [Active],[Action_ID],[Action_User],[Action_Date],[Action_Reason],[TS] ,[TSLONG],[TSMAN]
		,[Personality_Type_Code_ID],[Personality_Type],[Description] ,[Public_Description],[Min_Age],[BlackBaud_Guid],[Blackbaud_Uploaded]
		FROM [Code].[Personality_Type] WHERE [Personality_Type_Code_ID] =@Id
	END

	/** 8.  FAVOURITE LEARNING **/
	IF @Table = 'FavouriteLearning'
	BEGIN
		SELECT [Active],[Action_ID],[Action_User],[Action_Date],[Action_Reason],[TS],[TSLONG],[TSMAN],[Favorite_Learning_Code_ID],[Favorite_Learning]
      ,[Description],[Public_Description],[Min_Age],[BlackBaud_Guid],[Blackbaud_Uploaded]
		FROM [Code].[Favorite_Learning] WHERE [Favorite_Learning_Code_ID] =@Id
	END

	/** 9.  FAVOURITE ACTIVITY **/
	IF @Table = 'FavouriteActivity'
	BEGIN
		SELECT  
		[Active] ,[Action_ID],[Action_User],[Action_Date],[Action_Reason],[TS],[TSLONG],[TSMAN],[Favorite_Activity_Code_ID],[Favorite_Activity]
      ,[Description],[Public_Description],[Min_Age],[BlackBaud_Guid],[Blackbaud_Uploaded]
       FROM [Code].[Favorite_Activity] WHERE [Favorite_Activity_Code_ID] =@Id
	END

	/** 10.  GRADELEVEL **/
	IF @Table = 'GradeLevel'
	BEGIN
		SELECT [Active],[Action_ID],[Action_User],[Action_Date],[Action_Reason],[TS]
		  ,[TSLONG],[TSMAN] ,[Grade_Level_Code_ID],[Grade_Level],[Description],[Public_Description]
		  ,[Min_Age],[BlackBaud_Guid],[Blackbaud_Uploaded]
		FROM [Code].[Grade_Level] WHERE [Grade_Level_Code_ID] =@Id
	END

	/** 11.  CONTENTTYPE **/
	IF @Table = 'ContentType'
	BEGIN
		SELECT [Active],[Action_ID],[Action_User],[Action_Date],[Action_Reason],[TS],[TSLONG],[TSMAN],[Content_Type_Code_ID],[Content_Type],[Description],[Public_Description],[Required]
      ,[Workflow_Related],[App_Related],[Default_Status_Code_ID],[Default_Translation_Required],[Default_QC_Required],[Default_Shippable]
  FROM [Code].[Content_Type] WHERE [Content_Type_Code_ID] =@Id
	END

	/** 12.  DECLINEREASON **/
	IF @Table = 'DeclineReason'
	BEGIN
		SELECT [Action_ID],[Action_User],[Action_Date],[Action_Reason],[TS],[TSLONG],[TSMAN]
				,[Decline_Reason_Code_ID],[Decline_Reason],[Description],[Public_Description]
			FROM [Code].[Decline_Reason] WHERE [Decline_Reason_Code_ID] =@Id
	END
	
	/** 13.  CHILD REMOVE REASON **/
	IF @Table = 'ChildRemoveReason'
	BEGIN
		SELECT [Active],[Action_ID],[Action_User],[Action_Date],[Action_Reason],[TS],[TSLONG],[TSMAN]
      ,[Child_Remove_Reason_Code_ID],[Child_Remove_Reason],[Description],[Public_Description],[Min_Age]
		FROM  [Code].[Child_Remove_Reason] WHERE [Child_Remove_Reason_Code_ID] =@Id
	END
END