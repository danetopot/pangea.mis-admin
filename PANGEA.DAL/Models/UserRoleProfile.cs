﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PANGEA.DAL.Models
{
    public class UserRoleProfile
    {
        [Key, Column(Order = 1)]
        public int TaskId { get; set; }
        [Key, Column(Order = 2)]
        public string RoleId { get; set; }
        public SystemTask Task { get; set; }
    }
}
