﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PANGEA.DAL.Models
{

    public class LookUps
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "LookUp ID")]
        public int Lkp_ID { get; set; }

        [Required, StringLength(100), DataType(DataType.MultilineText)]
        [Display(Name = "LookUp Item")]
        public string Lkp_Lookup { get; set; }

        [Display(Name = "LookUp Parent ID")]
        public int Lkp_ParentID { get; set; }

        [Display(Name = "LookUp Is Parent")]
        public bool Lkp_IsParent { get; set; }

        [Display(Name = "Is Active")]
        public bool Lkp_Active { get; set; }

        [Display(Name = "Created On")]
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime Lkp_Created { get; set; }

        [Display(Name = "Created By")]
        public string Lkp_CreatedBy { get; set; }

        [Display(Name = "Updated On")]
        [DataType(DataType.Text)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime Lkp_Updated { get; set; }

        [Display(Name = "Updated By")]
        public string Lkp_UpdatedBy { get; set; }
    }
}