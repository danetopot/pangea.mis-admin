﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PANGEA.DAL.Models
{
    public class UserLocation
    {
        public int Id { get; set; }

        [Key, Column(Order = 1)]
        public User User { get; set; }

        [Key, Column(Order = 2)]
        public LookUps Location { get; set; }
        public string CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool IsActive { get; set; }
    }
}
