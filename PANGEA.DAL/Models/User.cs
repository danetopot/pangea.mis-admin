﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PANGEA.DAL.Models
{

    public class User : IdentityUser
    {
        public int UserId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Surname { get; set; }
        public string FullName => $"{FirstName} {MiddleName} {Surname}";
        public string Email { get; set; }
        public override bool EmailConfirmed { get; set; }
        public string RoleId { get; set; }

        [ForeignKey("RoleId")]
        public UserRole Role { get; set; }
        public DateTime? PasswordChangedOn { get; set; }
        [StringLength(256)]
        public override string PasswordHash { get; set; }
        [StringLength(256)]
        public override string SecurityStamp { get; set; }
        public string Department { get; set; }
        public bool IsLocked { get; set; }
        public bool IsActive { get; set; }
        public string CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedById { get; set; }          
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeactivateDate { get; set; }
    }
}
