﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PANGEA.DAL.Models
{
    public class SystemTask
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public SystemTask Parent { get; set; }

        [ForeignKey("ParentId")]
        public ICollection<SystemTask> Children { get; set; }
        public int? Order { get; set; }
    }
}
