﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PANGEA.DAL.Models
{
    public class UserRole : IdentityRole
    {

        public UserRole() : base() { }

        public UserRole(string roleName) : base(roleName) { }

        public UserRole(string roleName, string description,
            DateTime createdDate, bool isExternal, string createdById)
            : base(roleName)
        {
            base.Name = roleName;

            this.Description = description;
            this.CreatedOn = createdDate;
            this.IsExternal = isExternal;
            this.CreatedById = createdById;
        }

        public string Description { get; set; }
        [DisplayName("Is External?")]
        public bool IsExternal { get; set; }
        [DisplayName("Date Created")]
        public DateTime CreatedOn { get; set; }

        [DisplayName("Created By")]
        public string CreatedById { get; set; }
    }

}
