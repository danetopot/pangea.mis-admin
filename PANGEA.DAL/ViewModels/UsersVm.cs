﻿using Microsoft.AspNetCore.Mvc.Rendering;
using PANGEA.DAL.Models;
using PANGEA.DAL.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using X.PagedList;

namespace PANGEA.DAL.ViewModels
{
    public class UserVm
    {
        [Required]
        [Display(Name = "Staff Number")]
        public int UserId { get; set; }

        [Required]
        [Display(Name = "Title/Designation")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Required]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [Display(Name = "FullName")]
        public string FullName => $"{FirstName} {MiddleName} {Surname}";

        [Required]
        [EmailAddress]
        [Display(Name = "Email Address")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "User Profile/Group")]
        public string RoleName { get; set; }


        [Required]
        [Display(Name = "Department")]
        public string Department { get; set; }

        //[CustomListRequiredAttribute]
        [Required]
        [Display(Name = "Countries")]
        public List<UserLocationVm> Locations { get; set; }

        public UserRole UserRole { get; set; }
    }

    public class UserLocationVm
    {
        public int Id { get; set; }
        public string Display { get; set; }
        public string Location { get; set; }
        public bool IsSelected { get; set; }
    }

    public class UserProfileVm
    {
        public ICollection<SystemTask> SystemTasks { get; set; }
        public int[] Ids { get; set; }

        [DisplayName("User Role")]
        public string UserRoleId { get; set; }

        public ICollection<int> UserRoleProfileIds { get; set; }
    }

    public class UserRoleVm
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        public string Id { get; set; }
        public bool IsExternal { get; set; }
    }

    public class ListUsersVm
    {  
        public int? Page { get; set; }
        public int? PageSize { get; set; }

        [DisplayName("User Name")]
        public string Name { get; set; }

        public User User { get; set; }
        public IPagedList<User> Users { get; set; }
        public IList<UserRoleVm> Roles { get; set; }
        [DisplayName("Role")]
        public string RoleName { get; set; }
        public int? UserId { get; set; }
        public int? PhoneNumber { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }
    }

    public class ListUserProfileVm
    {
        public IPagedList<UserRole> Profiles { get; set; }
        public string ProfileName { get; set; }
        public bool IsExternal { get; set; }
        public int? Page { get; set; }
        public int? PageSize { get; set; }
    }
}
