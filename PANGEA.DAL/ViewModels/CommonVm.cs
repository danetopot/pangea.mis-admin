﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PANGEA.DAL.ViewModels
{
    public class QueryStateVm<T>
    {
        public int Code { set; get; }
        public string Msg { set; get; }
        public T Data { set; get; }
        public int Count { set; get; }
    }

    public class SpIntVm
    { 
        public int? Id { get; set; }
        public int? NoOfRows { get; set; }
    }
}
