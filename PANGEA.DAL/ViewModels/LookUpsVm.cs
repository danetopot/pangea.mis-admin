﻿using PANGEA.DAL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using X.PagedList;

namespace PANGEA.DAL.ViewModels
{
    #region Pangea Web Portal 2.0.0 Vms
    public enum LookUpType
    {
        Gender,
        GradeLevel,
        LivesWith,
        Chore,
        PersonalityType,
        FavouriteLearning,
        FavouriteActivity,
        DeclineReason,
        DeclineSubReason,
        Status,
        ContentType,
        ChildRemoveReason,
        Country,
        Region,
        Location
    }

    public class ListLookUpsVm
    {
        public string Search { get; set; }
        public string Table { get; set; }
        public int Count { get; set; }
        public int? Page { get; set; }
        public int? PageSize { get; set; }
        public bool IncludeRemoved { get; set; }
        public IPagedList<RegionLookUpVm> RegionList;
        public IPagedList<CountryLookUpVm> CountryList;
        public IPagedList<LocationLookUpVm> LocationList;
        public IPagedList<GenderLookUpVm> GenderList;
        public IPagedList<ChoreLookUpVm> ChoreList;
        public IPagedList<LivesWithLookUpVm> LivesWithList;
        public IPagedList<PersonalityTypeLookUpVm> PersonalityTypeList;
        public IPagedList<FavouriteLearningLookUpVm> FavouriteLearningList; 
        public IPagedList<FavouriteActivityLookUpVm> FavouriteActivityList;  
        public IPagedList<GradeLevelLookUpVm> GradeLevelList;  
        public IPagedList<ContentTypeLookUpVm> ContentTypeList;   
        public IPagedList<DeclineReasonLookUpVm> DeclineReasonList;
        public IPagedList<ChildRemoveReasonLookUpVm> ChildRemoveReasonList; 
    }

    public class ChildRemoveReasonLookUpVm
    {
        public int Id { get; set; }

        [DisplayName("Is Active?")]
        public bool Active { get; set; }
        public int Action_ID { get; set; }
        public Guid Action_User { get; set; }
        public DateTime Action_Date { get; set; }
        public int Action_Reason { get; set; }
        public byte[] TS { get; set; }
        public long TSLONG { get; set; }
        public string TSMAN { get; set; }
        public int Child_Remove_Reason_Code_ID { get; set; }

        [Required]
        [DisplayName("Child Remove Reason")]
        public string Child_Remove_Reason { get; set; }

        [Required]
        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Public Description")]
        public string Public_Description { get; set; }

        [Required]
        [DisplayName("Minimum Age")]
        public int Min_Age { get; set; }

    }

    public class ChoreLookUpVm
    {
        public int Id { get; set; }

        [DisplayName("Is Active?")]
        public bool Active { get; set; }
        public int Action_ID { get; set; }
        public Guid Action_User { get; set; }
        public DateTime Action_Date { get; set; }
        public int Action_Reason { get; set; }
        public byte[] TS { get; set; }
        public long TSLONG { get; set; }
        public string TSMAN { get; set; }
        public int Chore_Code_ID { get; set; }

        [Required]
        [DisplayName("Chore")]
        public string Chore { get; set; }

        [Required]
        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Public Description")]
        public string Public_Description { get; set; }

        [Required]
        [DisplayName("Minimum Age")]
        public int Min_Age { get; set; }
    }

    public class ContentTypeLookUpVm
    {
        public string Id { get; set; }

        [DisplayName("Is Active?")]
        public bool Active { get; set; }
        public int Action_ID { get; set; }
        public Guid Action_User { get; set; }
        public DateTime Action_Date { get; set; }
        public int Action_Reason { get; set; }
        public byte[] TS { get; set; }
        public long TSLONG { get; set; }
        public string TSMAN { get; set; }
        public int Content_Type_Code_ID { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Content Type")]
        public string Content_Type { get; set; }

        [Required]
        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Public Description")]
        public string Public_Description { get; set; }

        [DisplayName("Is Required?")]
        public bool Required { get; set; }
        public bool Workflow_Related { get; set; }
        public bool App_Related { get; set; }
        public int Default_Status_Code_ID { get; set; }
        public bool Default_Translation_Required { get; set; }
        public bool Default_QC_Required { get; set; }
        public bool Default_Shippable { get; set; }
        public Guid BlackBaud_Guid { get; set; }
        public string Blackbaud_Uploaded { get; set; }
    }

    public class CountryLookUpVm
    {
        public string Id { get; set; }

        [DisplayName("Is Active?")]
        public bool Active { get; set; }
        public int Action_ID { get; set; }
        public Guid Action_User { get; set; }
        public DateTime Action_Date { get; set; }
        public int Action_Reason { get; set; }
        public byte[] TS { get; set; }
        public long TSLONG { get; set; }
        public string TSMAN { get; set; }
        public string Country_Code_ID { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Code")]
        public string Country_Code { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Name")]
        public string Country_Name { get; set; }
        public string Region_Name { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Region")]
        public string Region_Code_ID { get; set; }

        [DisplayName("Default Language")]
        public int Default_Language { get; set; }

        public Guid BlackBaud_Guid { get; set; }
       
        [DisplayName("Minimum School Age")]
        public int Min_School_Age { get; set; }

        public string Blackbaud_Uploaded { get; set; }

    }

    public class RegionLookUpVm
    {
        public int Id { get; set; }

        [DisplayName("Is Active?")]
        public bool Active { get; set; }
        public int Action_ID { get; set; }
        public Guid Action_User { get; set; }
        public DateTime Action_Date { get; set; }
        public int Action_Reason { get; set; }
        public byte[] TS { get; set; }
        public long TSLONG { get; set; }
        public string TSMAN { get; set; }
        public string Region_Code_ID { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Code")]
        public string Region_Code { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Name")]
        public string Region_Name { get; set; }

        [DisplayName("Default Language")]
        public int Default_Language { get; set; }
        public Guid BlackBaud_Guid { get; set; }
        public string Blackbaud_Uploaded { get; set; }
    }

    public class LocationLookUpVm
    {
        public string Id { get; set; }

        [DisplayName("Is Active?")]
        public bool Active { get; set; }
        public int Action_ID { get; set; }
        public Guid Action_User { get; set; }
        public DateTime Action_Date { get; set; }
        public int Action_Reason { get; set; }
        public byte[] TS { get; set; }
        public long TSLONG { get; set; }
        public string TSMAN { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Name")]
        public string Location_Name { get; set; }   
        public int Location_Code_ID { get; set; }

        public string Country_Name { get; set; }
        
        [Required]
        [DisplayName("Country")]
        public int Country_Code_ID { get; set; }

        [Required]
        [DisplayName("Is Sponsorship Site?")]
        public bool Sponsorship_Site { get; set; }

        [DisplayName("Default Language")]
        public int Default_Language { get; set; }
        public Guid BlackBaud_Guid { get; set; }
        public string Blackbaud_Uploaded { get; set; }
    }

    public class DeclineReasonLookUpVm
    {
        public int Id { get; set; }

        [DisplayName("Is Active?")]
        public bool Active { get; set; }
        public int Action_ID { get; set; }
        public Guid Action_User { get; set; }
        public DateTime Action_Date { get; set; }
        public int Action_Reason { get; set; }
        public byte[] TS { get; set; }
        public long TSLONG { get; set; }
        public string TSMAN { get; set; }
        public int Decline_Reason_Code_ID { get; set; }

        [Required]
        [DisplayName("Decline Reason")]
        public string Decline_Reason { get; set; }

        [Required]
        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Public Description")]
        public string Public_Description { get; set; }
    }

    public class FavouriteActivityLookUpVm
    {
        public int Id { get; set; }

        [DisplayName("Is Active?")]
        public bool Active { get; set; }
        public int Action_ID { get; set; }
        public Guid Action_User { get; set; }
        public DateTime Action_Date { get; set; }
        public int Action_Reason { get; set; }
        public byte[] TS { get; set; }
        public long TSLONG { get; set; }
        public string TSMAN { get; set; }
        public int Favorite_Activity_Code_ID { get; set; }

        [Required]
        [DisplayName("Favorite Activity")]
        public string Favorite_Activity { get; set; }

        [Required]
        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Public Description")]
        public string Public_Description { get; set; }

        [Required]
        [DisplayName("Minimum Age")]
        public int Min_Age { get; set; }
    }

    public class FavouriteLearningLookUpVm
    {
        public int Id { get; set; }

        [DisplayName("Is Active?")]
        public bool Active { get; set; }
        public int Action_ID { get; set; }
        public Guid Action_User { get; set; }
        public DateTime Action_Date { get; set; }
        public int Action_Reason { get; set; }
        public byte[] TS { get; set; }
        public long TSLONG { get; set; }
        public string TSMAN { get; set; }
        public int Favorite_Learning_Code_ID { get; set; }

        [Required]
        [DisplayName("Favorite Learning")]
        public string Favorite_Learning { get; set; }

        [Required]
        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Public Description")]
        public string Public_Description { get; set; }

        [Required]
        [DisplayName("Minimum Age")]
        public int Min_Age { get; set; }
    }

    public class GenderLookUpVm
    {
        public int Id { get; set; }

        [DisplayName("Is Active?")]
        public bool Active { get; set; }
        public int Action_ID { get; set; }
        public Guid Action_User { get; set; }
        public DateTime Action_Date { get; set; }
        public int Action_Reason { get; set; }
        public byte[] TS { get; set; }
        public long TSLONG { get; set; }
        public string TSMAN { get; set; }
        public int Gender_Code_ID { get; set; }

        [Required]
        [DisplayName("Gender")]
        public string Gender { get; set; }

        [Required]
        [DisplayName("Description")]
        public string Description { get; set; }


        [DisplayName("Public Description")]
        public string Public_Description { get; set; }
    }

    public class GradeLevelLookUpVm
    {
        public int Id { get; set; }

        [DisplayName("Is Active?")]
        public bool Active { get; set; }
        public int Action_ID { get; set; }
        public Guid Action_User { get; set; }
        public DateTime Action_Date { get; set; }
        public int Action_Reason { get; set; }
        public byte[] TS { get; set; }
        public long TSLONG { get; set; }
        public string TSMAN { get; set; }

        public int Grade_Level_Code_ID { get; set; }

        [Required]
        [DisplayName("Grade Level")]
        public string Grade_Level { get; set; }

        [Required]
        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Public Description")]
        public string Public_Description { get; set; }

        [DisplayName("Minimum Age")]
        public int Min_Age { get; set; }
    }

    public class LivesWithLookUpVm
    {
        public int Id { get; set; }

        [DisplayName("Is Active?")]
        public bool Active { get; set; }
        public int Action_ID { get; set; }
        public Guid Action_User { get; set; }
        public DateTime Action_Date { get; set; }
        public int Action_Reason { get; set; }
        public byte[] TS { get; set; }
        public long TSLONG { get; set; }
        public string TSMAN { get; set; }
        public int Lives_With_Code_ID { get; set; }

        [Required]
        [DisplayName("Lives With")]
        public string Lives_With { get; set; }

        [Required]
        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Public Description")]
        public string Public_Description { get; set; }


        [DisplayName("Minimum Age")]
        public int Min_Age { get; set; }
    }

    public class PersonalityTypeLookUpVm
    {
        public int Id { get; set; }

        [DisplayName("Is Active?")]
        public bool Active { get; set; }
        public int Action_ID { get; set; }
        public Guid Action_User { get; set; }
        public DateTime Action_Date { get; set; }
        public int Action_Reason { get; set; }
        public byte[] TS { get; set; }
        public long TSLONG { get; set; }
        public string TSMAN { get; set; }
        public int Personality_Type_Code_ID { get; set; }

        [Required]
        [DisplayName("Personality Type")]
        public string Personality_Type { get; set; }

        [Required]
        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Public Description")]
        public string Public_Description { get; set; }

        [DisplayName("Minimum Age")]
        public int Min_Age { get; set; }
    }

    #endregion

}
