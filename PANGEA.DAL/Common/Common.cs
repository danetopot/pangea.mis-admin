﻿using PANGEA.DAL.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PANGEA.DAL.Common
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class CustomCheckBoxListRequiredAttribute : RequiredAttribute
    {
        public override bool IsValid(object value)
        {
            bool result = false;

            var list = value as IEnumerable<UserLocationVm>;
            if (list != null && list.GetEnumerator().MoveNext())
            {
                foreach (var item in list)
                {
                    if (item.IsSelected)
                    {
                        result = true;
                        break;
                    }
                }
            }

            return result;
        }
    }
}
