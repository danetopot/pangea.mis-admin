﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using PANGEA.DAL;
using PANGEA.DAL.ViewModels;
using PANGEA.SERVICES;
using PANGEA.SERVICES.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using X.PagedList;

namespace PANGEA.API.Controllers.Update
{
    [ApiController]
    public class UpdateController : ControllerBase
    {
        private readonly IGenericService _genericService;
        private readonly ILogService _logService;
        private readonly IMapper _mapper;
        private readonly ApplicationDbContext _context;

        public UpdateController(IGenericService genericService, ILogService logService, IMapper mapper, ApplicationDbContext context)
        {
            _genericService = genericService;
            _logService = logService;
            _mapper = mapper;
            _context = context;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/Update/PostUpdateListings/")]
        [ProducesResponseType(typeof(ApiFeedBack), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult PostUpdateListings([FromBody] TabletUpdatesViewModel model)
        {
            ApiFeedBack apiFeedback;


            #region Logging Raw Http Content
            var deviceInfo = JsonConvert.DeserializeObject<TabEnvironmentViewModel>(model.DeviceInfo);

            var childInfo = JsonConvert.DeserializeObject<ChildInfoViewModel>(model.ChildInfo);

            List<ChoreViewModel> childChoreInfo = JsonConvert.DeserializeObject<List<ChoreViewModel>>(model.ChildChoreInfo);

            List<FavActivityViewModel> childFavouriteActivityInfo = JsonConvert.DeserializeObject<List<FavActivityViewModel>>(model.ChildFavouriteActivityInfo);

            List<DocumentViewModel> childDocumentInfo = JsonConvert.DeserializeObject<List<DocumentViewModel>>(model.ChildDocumentInfo);

            var childLifeEventsInfo = new MLEViewModel
            {
                ChildId = childInfo.Id,
                Event = childInfo.MajorLifeEvent,
                RecordStatus = null,
                RecordStatusId = 0,
                SyncStatus = null,
                SyncStatusId = 0,
                CreatedOn = DateTime.Now.ToString(),
                CreatedById = null,
                ModifiedOn = DateTime.Now.ToString(),
                ModifiedById = null,
            };

            _logService.FileLog(
                ApiKeys.UPDATE_UPLOAD,
                JsonConvert.SerializeObject(childInfo),
                JsonConvert.SerializeObject(childChoreInfo),
                JsonConvert.SerializeObject(childFavouriteActivityInfo),
                JsonConvert.SerializeObject(childDocumentInfo),
                JsonConvert.SerializeObject(childLifeEventsInfo),
                JsonConvert.SerializeObject(deviceInfo),
                $"UserId USERX");
            #endregion

            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Update Data has been rejected";
                apiFeedback = new ApiFeedBack
                {
                    Id = -1,
                    Code = $"ERROR",
                    Description = description
                };
                _logService.FileLog(ApiKeys.ERROR, JsonConvert.SerializeObject(apiFeedback), $"UserId USERX");

                return Ok(apiFeedback);
            }


            try
            {
                var serializerx = new XmlSerializer(typeof(List<TabEnvironmentViewModel>), new XmlRootAttribute("Registrations"));
                var settingsx = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, };
                var deviceInfoXml = string.Empty;
                var listViewModelx = new List<TabEnvironmentViewModel> { deviceInfo };
                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settingsx);
                    serializerx.Serialize(xw, listViewModelx);
                    deviceInfoXml += sw.ToString();
                }

                #region Executing Database Stored Procedure
                var spName = "AcceptPostedUpdates";
                var inputParameters = new
                {
                    ChildId = childInfo.Id,
                    PlanId = 0,
                    UpdatedChild = $"{JsonConvert.SerializeObject(childInfo)}",
                    UpdatedChildChores = $"{JsonConvert.SerializeObject(childChoreInfo)}",
                    UpdatedChildFavActivities = $"{JsonConvert.SerializeObject(childFavouriteActivityInfo)}",
                    UpdatedChildDocuments = $"{JsonConvert.SerializeObject(childDocumentInfo)}",
                    UpdatedChildValidationExceptions = string.Empty,
                    UpdatedChildUpdates = string.Empty,
                    UpdatedMajorLifeEvent = $"{JsonConvert.SerializeObject(childLifeEventsInfo)}",
                    UserId = User.GetUserId()
                };

                var apiDbFeedback = _genericService.QueryFirstOrDefault<ApiFeedBack>(spName, inputParameters);
                if (apiDbFeedback.Code == 200)
                {
                    apiFeedback = apiDbFeedback.Data;
                    apiFeedback.Id = 1;
                    apiFeedback.Code = $"{apiFeedback.Code}";
                    apiFeedback.Description = $"{apiFeedback.Description}";
                }
                else
                {
                    apiFeedback = new ApiFeedBack
                    {
                        Id = -1,
                        Code = apiDbFeedback.Code.ToString(),
                        Description = apiDbFeedback.Msg
                    };
                }
                #endregion

                return Ok(apiFeedback);

            }
            catch (Exception e)
            {
                apiFeedback = new ApiFeedBack
                {
                    Id = -1,
                    Code = "ERROR",
                    Description = "\n" + e.Message + " \n" + e.InnerException?.StackTrace + " \n" + e.InnerException?.Message + " \n"
                };

                return Ok(apiFeedback);
            }
        }

        protected string GetErrorListFromModelState(ModelStateDictionary modelState)
        {

            var message = string.Join(" | ", ModelState.Values
                .SelectMany(v => v.Errors)
                .Select(e => e.ErrorMessage));
            return message;


        }
    }
}
