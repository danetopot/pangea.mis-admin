﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using PANGEA.DAL;
using PANGEA.DAL.ViewModels;
using PANGEA.SERVICES;
using PANGEA.SERVICES.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using X.PagedList;

namespace PANGEA.API.Controllers.Enrolment
{
    [ApiController]
    public class EnrolmentController : ControllerBase
    {
        private readonly IGenericService _genericService;
        private readonly ILogService _logService;
        private readonly IMapper _mapper;
        private readonly ApplicationDbContext _context;

        public EnrolmentController(IGenericService genericService, ILogService logService, IMapper mapper, ApplicationDbContext context)
        {
            _genericService = genericService;
            _logService = logService;
            _mapper = mapper;
            _context = context;
        }

        [HttpPost]
        [Route("api/Enrol/PullEnrolmentListings/")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(ApiFeedBack), StatusCodes.Status200OK)]
        public async Task<ActionResult> PullEnrolmentListings(LoginRequestVm model)
        {
            string email = model.Email;

            return Ok();
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/Enrol/PostEnrolmentListings/")]
        [ProducesResponseType(typeof(ApiFeedBack), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult PostEnrolmentListings([FromBody] TabletRegistrationViewModel model)
        {
            ApiFeedBack apiFeedback;

            #region Logging Raw Http Content

            var childInfo = JsonConvert.DeserializeObject<ChildInfoViewModel>(model.ChildInfo);

            List<ChoreViewModel> childChoreInfo = JsonConvert.DeserializeObject<List<ChoreViewModel>>(model.ChildChoreInfo);
            var choresDict = new { ChildChoreInfo = childChoreInfo };

            List<FavActivityViewModel> childFavouriteActivityInfo = JsonConvert.DeserializeObject<List<FavActivityViewModel>>(model.ChildFavouriteActivityInfo);
            var favActivityDict = new { ChildFavouriteActivityInfo = childFavouriteActivityInfo };

            List<DocumentViewModel> childDocumentInfo = JsonConvert.DeserializeObject<List<DocumentViewModel>>(model.ChildDocumentInfo);
            var documentDict = new { ChildDocumentInfo = childDocumentInfo };

            var deviceInfo = JsonConvert.DeserializeObject<TabEnvironmentViewModel>(model.DeviceInfo);

            _logService.FileLog(
                ApiKeys.ENROLMENT_UPLOAD,
                JsonConvert.SerializeObject(childInfo),
                JsonConvert.SerializeObject(choresDict),
                JsonConvert.SerializeObject(favActivityDict),
                JsonConvert.SerializeObject(documentDict),
                string.Empty,
                JsonConvert.SerializeObject(deviceInfo),
                $"UserId USERX");
            #endregion


            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Registration Data has been rejected";
                apiFeedback = new ApiFeedBack
                {
                    Id = -1,
                    Code = $"ERROR",
                    Description = description
                };
                _logService.FileLog(ApiKeys.ERROR, JsonConvert.SerializeObject(apiFeedback), "User X UserXX");

                return Ok(apiFeedback);
            }


            try
            {

                #region Serializing Raw Htttp Content To XML
                var serializer = new XmlSerializer(typeof(List<ChildInfoViewModel>), new XmlRootAttribute("Registrations"));
                var settings = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, };
                var childInfoXml = string.Empty;
                var listViewModel = new List<ChildInfoViewModel> { childInfo };
                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    serializer.Serialize(xw, listViewModel);
                    childInfoXml += sw.ToString();
                }

                var serializerc = new XmlSerializer(typeof(List<ChoreViewModel>), new XmlRootAttribute("Registrations"));
                var settingsc = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, };
                var childChoreInfoXml = string.Empty;
                foreach(var childChore in childChoreInfo)
                {
                    var listViewModelc = new List<ChoreViewModel> { childChore };
                    using (var sw = new StringWriter())
                    {
                        var xw = XmlWriter.Create(sw, settingsc);
                        serializerc.Serialize(xw, listViewModelc);
                        childChoreInfoXml += sw.ToString();
                    }
                }

                var serializerf = new XmlSerializer(typeof(List<FavActivityViewModel>), new XmlRootAttribute("Registrations"));
                var settingsf = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, };
                var childFavouriteActivityInfoXml = string.Empty;
                foreach (var childFavActivity in childFavouriteActivityInfo)
                {
                    var listViewModelf = new List<FavActivityViewModel> { childFavActivity };
                    using (var sw = new StringWriter())
                    {
                        var xw = XmlWriter.Create(sw, settingsf);
                        serializerf.Serialize(xw, listViewModelf);
                        childFavouriteActivityInfoXml += sw.ToString();
                    }
                }

                var serializerd = new XmlSerializer(typeof(List<DocumentViewModel>), new XmlRootAttribute("Registrations"));
                var settingsd = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, };
                var childDocumentInfoXml = string.Empty;
                foreach (var childDocument in childDocumentInfo)
                {
                    var listViewModeld = new List<DocumentViewModel> { childDocument };
                    using (var sw = new StringWriter())
                    {
                        var xw = XmlWriter.Create(sw, settingsd);
                        serializerd.Serialize(xw, listViewModeld);
                        childDocumentInfoXml += sw.ToString();
                    }
                }

                var serializerx = new XmlSerializer(typeof(List<TabEnvironmentViewModel>), new XmlRootAttribute("Registrations"));
                var settingsx = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, };
                var deviceInfoXml = string.Empty;
                var listViewModelx = new List<TabEnvironmentViewModel> { deviceInfo };
                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settingsx);
                    serializerx.Serialize(xw, listViewModelx);
                    deviceInfoXml += sw.ToString();
                }

                _logService.FileLog(
                   ApiKeys.ENROLMENT_DATA,
                   childInfoXml,
                   childChoreInfoXml,
                   childFavouriteActivityInfoXml,
                   childDocumentInfoXml,
                   string.Empty,
                   deviceInfoXml,
                   $"UserId USERX");
                #endregion

                #region Executing Database Stored Procedure
                var spName = "AcceptPostedEnrolments";
                var inputParameters = new
                {
                    ChildInfoXml = childInfoXml,
                    ChildChoreInfoXml = childChoreInfoXml,
                    ChildFavouriteActivityInfoXml = childFavouriteActivityInfoXml,
                    ChildDocumentInfoXml = childDocumentInfoXml,
                    DeviceInfoXml = deviceInfoXml
                };


                var apiDbFeedback = _genericService.QueryFirstOrDefault<ApiFeedBack>(spName, inputParameters);
                if (apiDbFeedback.Code == 200)
                {
                    apiFeedback = apiDbFeedback.Data;
                    apiFeedback.Id = 1;
                    apiFeedback.Code = $"{apiFeedback.Code}";
                    apiFeedback.Description = $"{apiFeedback.Description}";
                }
                else
                {
                    apiFeedback = new ApiFeedBack
                    {
                        Id = -1,
                        Code = apiDbFeedback.Code.ToString(),
                        Description = apiDbFeedback.Msg
                    };
                }
                #endregion

                return Ok(apiFeedback);

            }
            catch (Exception e)
            {
                apiFeedback = new ApiFeedBack
                {
                    Id = -1,
                    Code = "ERROR",
                    Description = "\n" + e.Message + " \n" + e.InnerException?.StackTrace + " \n" + e.InnerException?.Message + " \n"
                };
                
                return Ok(apiFeedback);
            }
        }

        protected string GetErrorListFromModelState(ModelStateDictionary modelState)
        {

            var message = string.Join(" | ", ModelState.Values
                .SelectMany(v => v.Errors)
                .Select(e => e.ErrorMessage));
            return message;


        }
    }
}
