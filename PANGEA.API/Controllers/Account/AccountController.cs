﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using PANGEA.DAL.ViewModels;

namespace PANGEA.API.Controllers.Account
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        public AccountController()
        {
        }

        [HttpPost]
        [Route("Login")]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginRequestVm model)
        {
            string email = model.Email;

            return Ok();
        }

        [HttpGet]
        [Route("Logout")]
        public async Task<ActionResult> Logout()
        {
            return Ok();
        }
    }
}
