﻿using AutoMapper;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using PANGEA.DAL;
using PANGEA.DAL.ViewModels;
using PANGEA.DAL.ViewModels.ApiViewModels;
using PANGEA.SERVICES;
using PANGEA.SERVICES.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using X.PagedList;


namespace PANGEA.API.Controllers.Data
{
    [ApiController]
    public class DataController : ControllerBase
    {
        private readonly IGenericService _genericService;
        private readonly ILogService _logService;
        private readonly IMapper _mapper;
        private readonly ApplicationDbContext _context;

        public DataController(IGenericService genericService, ILogService logService, IMapper mapper, ApplicationDbContext context)
        {
            _genericService = genericService;
            _logService = logService;
            _mapper = mapper;
            _context = context;
        }

        [HttpPost]
        [Route("api/Data/PullSettingListings/")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(ApiSettupVm), StatusCodes.Status200OK)]
        public async Task<ApiSettupVm> PullSettingListings()
        {
            var userId = "USER X"; // User.GetUserId();
            var returnModel = new ApiSettupVm();

            try
            {
                var storedProcedure = "GetMobileApplicationResourcesReadyForDownload";
                using IDbConnection db = new SqlConnection(DBService.DBConnection());
                var data = await db.QueryMultipleAsync(storedProcedure,
                    new { UserId = userId },
                    commandType: CommandType.StoredProcedure);


                returnModel = new ApiSettupVm
                {
                    RegistrationPlanListings = await data.Read<RegistrationPlanListingVm>().ToListAsync(),
                    UpdatePlanListings = await data.Read<UpdatePlanListingVm>().ToListAsync(),
                    UpdateChildrenListings = await data.Read<ChildVm>().ToListAsync(),
                    UpdateChildrenChoresListings = await data.Read<ChildChoreVm>().ToListAsync(),
                    UpdateChildrenFavActivitiesListings = await data.Read<ChildFavActivityVm>().ToListAsync(),
                    UpdateChildrenDocumentListings = await data.Read<ChildDocumentVm>().ToListAsync(),
                    LocationListings = await data.Read<LocationVm>().ToListAsync(),
                    SystemCodeListings = await data.Read<SystemCodeDetailsVm>().ToListAsync(),
                };
                returnModel.Code = (int)ApiMessages.Success;
                returnModel.Message = $"Pulling settings was successful.";

                _logService.FileLog(ApiKeys.SETTINGS_DOWNLOAD, Newtonsoft.Json.JsonConvert.SerializeObject(returnModel), userId);
            }
            catch (Exception ex)
            {
                returnModel.Code = 0;
                returnModel.Message = $"Pulling settings error - {ex.Message}";
            }

            return returnModel;
        }

        protected string GetErrorListFromModelState(ModelStateDictionary modelState)
        {

            var message = string.Join(" | ", ModelState.Values
                .SelectMany(v => v.Errors)
                .Select(e => e.ErrorMessage));
            return message;


        }
    }
}
