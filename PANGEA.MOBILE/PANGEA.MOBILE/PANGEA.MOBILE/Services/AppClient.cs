﻿using Newtonsoft.Json;
using PANGEA.MOBILE.Database;
using PANGEA.MOBILE.Helpers;
using PANGEA.MOBILE.Interface;
using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.ViewModels.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PANGEA.MOBILE.Services
{
    public class AppClient : IAppClient
    {
        private const string PangeaSSOApiKey = "0c833t3w37jq58dj249dt675a465k6b0rz090zl3jpoa9jw8vz7y6awpj5ox0qmb";
        private readonly HttpClient client;

        public AppClient()
            : this(PangeaSSOApiKey)
        {
        }

        public AppClient(string apiKey)
        {
            this.client = new HttpClient { BaseAddress = new Uri(Constants.BaseApiAddress) };
            this.client.DefaultRequestHeaders.Clear();
            this.client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<ApiResponseViewModel> CreateTokenAsync(string userId)
        {
            return null;
        }

        public async Task LoginAsync(string userId) =>
            await this.CreateTokenAsync(userId);

        public async Task LogoutAsync(string userId)
        {
            await this.PostForm("api/Account/Logout/", null).ConfigureAwait(true);

            await Task.FromResult(0);
        }

        public async Task<ApiSettupViewModel> GetSettingsAsync()
        {
            var form = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("UserId", null),
                };

            var json = await this.PostForm("api/Data/PullSettingListings/", form).ConfigureAwait(true);

            return json == "" ? new ApiSettupViewModel() : JsonConvert.DeserializeObject<ApiSettupViewModel>(json);
        }

        public async Task<ApiStatusViewModel> PostRegistrationAsyc(Children childInfo, 
            List<ChildrenChores> childchoresInfo, 
            List<ChildrenFavouriteActivities> childfavactivitiesInfo, 
            List<ChildrenDocuments> childdocumentsInfo, 
            LocalDeviceInfo deviceInfo)
        {
            var form = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("ChildInfo",JsonConvert.SerializeObject(childInfo,Formatting.None)),
                new KeyValuePair<string, string>("ChildChoreInfo",JsonConvert.SerializeObject(childchoresInfo,Formatting.None)),
                new KeyValuePair<string, string>("ChildFavouriteActivityInfo",JsonConvert.SerializeObject(childfavactivitiesInfo,Formatting.None)),
                new KeyValuePair<string, string>("ChildDocumentInfo",JsonConvert.SerializeObject(childdocumentsInfo,Formatting.None)),
                new KeyValuePair<string, string>("DeviceInfo",JsonConvert.SerializeObject(deviceInfo,Formatting.None)),

            };
            var json = await PostForm("api/Enrol/PostEnrolmentListings/", form).ConfigureAwait(true);

            return json == "" ? new ApiStatusViewModel() : JsonConvert.DeserializeObject<ApiStatusViewModel>(json);
        }

        public async Task<ApiStatusViewModel> PostUpdateAsyc(ChildrenUpdates childInfo,
            List<ChildrenChores> childchoresInfo,
            List<ChildrenFavouriteActivities> childfavactivitiesInfo,
            List<ChildrenUpdatesDocuments> childdocumentsInfo,
            LocalDeviceInfo deviceInfo)
        {
            var form = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("ChildInfo",JsonConvert.SerializeObject(childInfo,Formatting.None)),
                new KeyValuePair<string, string>("ChildChoreInfo",JsonConvert.SerializeObject(childchoresInfo,Formatting.None)),
                new KeyValuePair<string, string>("ChildFavouriteActivityInfo",JsonConvert.SerializeObject(childfavactivitiesInfo,Formatting.None)),
                new KeyValuePair<string, string>("ChildDocumentInfo",JsonConvert.SerializeObject(childdocumentsInfo,Formatting.None)),
                new KeyValuePair<string, string>("DeviceInfo",JsonConvert.SerializeObject(deviceInfo,Formatting.None)),

            };
            var json = await PostForm("api/Update/PostUpdateListings/", form).ConfigureAwait(true);

            return json == "" ? new ApiStatusViewModel() : JsonConvert.DeserializeObject<ApiStatusViewModel>(json);
        }

        private async Task<string> PostForm(string endpoint, List<KeyValuePair<string, string>> keyValues)
        {
            //if (!string.IsNullOrEmpty(Settings.Current.AccessToken))
            //{
            //    if (endpoint != "Token")
            //    {
            //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Settings.Current.AccessToken);
            //    }
            //}

            var result = string.Empty;
            var url = client.BaseAddress + endpoint;

            using (var client = new HttpClient())
            {
                var data = keyValues.ToDictionary(x => x.Key, x => x.Value);
                var payload = JsonConvert.SerializeObject(data);
                var content = new StringContent(payload, Encoding.UTF8, "application/json");

                try
                {
                    var response = await client.PostAsync($"{url}", content);
                    result = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ex)
                {
                    result = ex.InnerException.Message;
                }
            }

            return result;
        }

        public static Dictionary<string, string> ObjectToDictionary(object obj)
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();

            foreach (PropertyInfo prop in obj.GetType().GetProperties())
            {
                string propName = prop.Name;
                var val = obj.GetType().GetProperty(propName).GetValue(obj, null);
                if (val != null)
                {
                    ret.Add(propName, val.ToString());
                }
                else
                {
                    ret.Add(propName, null);
                }
            }

            return ret;
        }

        public static void Merge<TKey, TValue>(IDictionary<TKey, TValue> first, IDictionary<TKey, TValue> second)
        {
            if (second == null || first == null)
            {
                return;
            }

            foreach (var item in second)
            {
                if (!first.ContainsKey(item.Key))
                {
                    first.Add(item.Key, item.Value);
                }
            }
        }
    }

    
}
