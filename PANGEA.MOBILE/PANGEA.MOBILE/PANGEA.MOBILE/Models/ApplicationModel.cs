﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using PANGEA.MOBILE.Database;
using Plugin.DeviceInfo.Abstractions;
using SQLite;

namespace PANGEA.MOBILE.Models
{
    public class RegistrationPlans
    {
        [PrimaryKey, Unique]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string PlanStatus { get; set; }
        public bool IsActive { get; set; }
    }

    public class UpdatePlans
    {
        [PrimaryKey, Unique]
        public int Id { get; set; }
        public string ExerciseName { get; set; }
        public DateTime ExerciseStartDate { get; set; }
        public DateTime ExerciseEndDate { get; set; }
        public string RegistrationPlan { get; set; }
        public string UpdatePlanStatus { get; set; }
        public bool IsActive { get; set; }
    }

    public class Children
    {
        [PrimaryKey, Unique]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Nickname { get; set; }
        public int GenderId { get; set; }
        public string DateOfBirth { get; set; }
        public bool IsDateOfBirthApproximated { get; set; }
        public int GradeLevelId { get; set; }
        public int HealthStatusId { get; set; }
        public int LivesWithId { get; set; }
        public int FavouriteLearningId { get; set; }
        public int PersonalityId { get; set; }
        public int NumberOfBrothers { get; set; }
        public int NumberOfSisters { get; set; }
        public int DisabilityStatusId { get; set; }
        public int LocationId { get; set; }
        public int? RegistrationPlanId { get; set; }
        public string DateOfRegistration { get; set; }
        public string CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string UploadedOn { get; set; }
        public string UploadedBy { get; set; }
        public int? ChildStatusId { get; set; }
        public int? RecordStatusId { get; set; }
        public int? RecordSourceId { get; set; }
        public int? SyncStatusId { get; set; }
        public bool IsActive { get; set; }

        //[Ignore]
        //public ChildrenChores ChildrenChores { get; set; }

        [Ignore]
        public ICollection<ChildrenChores> ChildrenChores { get; set; }

        [Ignore]
        public ICollection<ChildrenDocuments> ChildrenDocuments { get; set; }

        [Ignore]
        public ICollection<ChildrenFavouriteActivities> ChildrenFavouriteActivities { get; set; }

        [Ignore]
        public ICollection<ChildrenLifeEvents> ChildrenLifeEvents { get; set; }

        #region Derived Variables
        // Display on UI
        public string FullName => $"{FirstName} {MiddleName} {LastName}".ToUpper();
        public string FullTags => $"{(string)App.Database.SystemCodeDetailGetById((int)GenderId).SystemDetailCode}   {Age}".ToUpper();
        public string LocationTags => $"{(string)App.Database.GetLocationById(LocationId.ToString()).Name}";
        // public string Age => DateTime.Today.Year > DateTime.Parse(DateOfBirth).Year ? $"{DateTime.Today.Year - DateTime.Parse(DateOfBirth).Year} YRS" : $"DATE OF BIRTH {Convert.ToDateTime(DateOfBirth).ToString("yyyy-MM-dd")}";
        public string Age => DateTime.Today.Year > DateTime.Parse(DateOfBirth).Year ? $"{DateTime.Today.Year - DateTime.Parse(DateOfBirth).Year} YRS" : $"0 YRS";
        public string ProfileImage { get; set; }
        public string TotalAttachments { get; set; }
        public string TotalMLEs { get; set; }
        public string Uploaded { get; set; }
        public string DateOfEnrolment => String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(DateOfRegistration));

        private const string delimiter = " ";
        private string haystack;
        [Newtonsoft.Json.JsonIgnore]
        public string Haystack
        {
            get
            {
                if (haystack != null)
                    return haystack;
                var builder = new StringBuilder();
                builder.Append(delimiter);
                builder.Append(Id);
                builder.Append(delimiter);
                builder.Append(FirstName);
                builder.Append(delimiter);
                builder.Append(MiddleName);
                builder.Append(delimiter);
                builder.Append(LastName);
                builder.Append(delimiter);
                builder.Append(Nickname);
                haystack = builder.ToString();
                return haystack;
            }
        } 
        #endregion
    }

    public class ChildrenUpdates
    {
        [PrimaryKey, Unique]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Surname => $"{LastName}";
        public string Nickname { get; set; }
        public int GenderId { get; set; }
        public string DateOfBirth { get; set; }
        public bool IsDateOfBirthApproximated { get; set; }
        public int GradeLevelId { get; set; }
        public int HealthStatusId { get; set; }
        public int LivesWithId { get; set; }
        public int FavouriteLearningId { get; set; }
        public int PersonalityId { get; set; }
        public int NumberOfBrothers { get; set; }
        public int NumberOfSisters { get; set; }
        public int DisabilityStatusId { get; set; }
        public int LocationId { get; set; }
        public int? RegistrationPlanId { get; set; }
        public string DateOfUpdate { get; set; }
        public string UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public string DownloadedOn { get; set; }
        public string DownloadedBy { get; set; }
        public string UploadedOn { get; set; }
        public string UploadedBy { get; set; }
        public string MajorLifeEvent { get; set; }
        public int? ChildStatusId { get; set; }
        public int? RecordStatusId { get; set; }
        public int? RecordSourceId { get; set; }
        public int? SyncStatusId { get; set; }
        public bool IsActive { get; set; }

        [Ignore]
        public ICollection<ChildrenChores> ChildrenChores { get; set; }

        [Ignore]
        public ICollection<ChildrenDocuments> ChildrenDocuments { get; set; }

        [Ignore]
        public ICollection<ChildrenFavouriteActivities> ChildrenFavouriteActivities { get; set; }

        #region Derived Variables
        // Display on UI
        public string FullName => $"{FirstName} {MiddleName} {LastName}".ToUpper();
        public string FullTags => $"{(string)App.Database.SystemCodeDetailGetById((int)GenderId).SystemDetailCode}   {Age}".ToUpper();
        public string LocationTags => $"{(string)App.Database.GetLocationById(LocationId.ToString()).Name}";
        // public string Age => DateTime.Today.Year > DateTime.Parse(DateOfBirth).Year ? $"{DateTime.Today.Year - DateTime.Parse(DateOfBirth).Year} YRS" : $"DATE OF BIRTH {Convert.ToDateTime(DateOfBirth).ToString("yyyy-MM-dd")}";
        public string Age => DateTime.Today.Year > DateTime.Parse(DateOfBirth).Year ? $"{DateTime.Today.Year - DateTime.Parse(DateOfBirth).Year} YRS" : $"0 YRS";
        public string ProfileImage { get; set; }
        public string TotalAttachments { get; set; }
        public string TotalMLEs { get; set; }
        public string Uploaded { get; set; }
        public string DateOfEnrolment => String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(DateOfUpdate));

        private const string delimiter = " ";
        private string haystack;
        [Newtonsoft.Json.JsonIgnore]
        public string Haystack
        {
            get
            {
                if (haystack != null)
                    return haystack;
                var builder = new StringBuilder();
                builder.Append(delimiter);
                builder.Append(Id);
                builder.Append(delimiter);
                builder.Append(FirstName);
                builder.Append(delimiter);
                builder.Append(MiddleName);
                builder.Append(delimiter);
                builder.Append(LastName);
                builder.Append(delimiter);
                builder.Append(Nickname);
                haystack = builder.ToString();
                return haystack;
            }
        }
        #endregion
    }

    public class ChildrenChores
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int ChildId { get; set; }
        public int ChoreId { get; set; }
        public int RecordStatusId { get; set; }
        public int SyncStatusId { get; set; }
        public bool IsActive { get; set; }
    }

    public class ChildrenFavouriteActivities
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int ChildId { get; set; }
        public int FavouriteActivityId { get; set; }
        public int RecordStatusId { get; set; }
        public int SyncStatusId { get; set; }
        public bool IsActive { get; set; }
    }

    public class ChildrenDocuments
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int ChildId { get; set; }
        public int DocumentTypeId { get; set; }
        public string DocumentName => $"{(string)App.Database.SystemCodeDetailGetById((int)DocumentTypeId).SystemDetailCode}".ToUpper();
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public Byte[] FileBinary { get; set; }
        public bool IsDocumentRequired { get; set; }
        public bool IsTranslationRequired { get; set; }
        public int DocumentStatusId { get; set; }
        public int RecordStatusId { get; set; }
        public int SyncStatusId { get; set; }
        public bool IsActive { get; set; }
    }

    public class ChildrenUpdatesDocuments
    {
        [PrimaryKey, Unique]
        public int Id { get; set; }
        public int ChildId { get; set; }
        public int DocumentTypeId { get; set; }
        public string DocumentName => $"{(string)App.Database.SystemCodeDetailGetById((int)DocumentTypeId).SystemDetailCode}".ToUpper();
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public Byte[] FileBinary { get; set; }
        public bool IsDocumentRequired { get; set; }
        public bool IsTranslationRequired { get; set; }
        public int DocumentStatusId { get; set; }
        public int RecordStatusId { get; set; }
        public int SyncStatusId { get; set; }
        public bool IsActive { get; set; }
    }

    public class ChildrenLifeEvents
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int ChildId { get; set; }
        public string LifeEvent { get; set; }
        public int RecordStatusId { get; set; }
        public int SyncStatusId { get; set; }
        public bool IsActive { get; set; }
    }

    public class Locations
    {
        [PrimaryKey, Unique]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int CountryId { get; set; }
        public bool IsSelected { get; set; }
        public bool IsUserMaintained { get; set; }
        public bool IsActive { get; set; }
    }

    public class SystemCodeDetails
    {
        [PrimaryKey, Unique]
        public int Id { get; set; }
        public string SystemCode { get; set; }
        public string SystemDetailCode { get; set; }
        public string SystemDetailDescription { get; set; }
        public int OrderNo { get; set; }
        public bool IsSelected { get; set; }
        public bool IsUserMaintained { get; set; }
        public bool IsActive { get; set; }
    }

    public class LocalDeviceInfo
    {
        public string DeviceId { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceManufacturer { get; set; }
        public string DeviceName { get; set; }
        public string Version { get; set; }
        public string VersionNumber { get; set; }
        public string AppVersion { get; set; }
        public string AppBuild { get; set; }
        public Platform Platform { get; set; }
        public Idiom Idiom { get; set; }
        public bool IsDevice { get; set; }
    }
    public class Item
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class SelectableItemWrapper<T>
    {
        public bool IsSelected { get; set; }
        public T Item { get; set; }

    }

}
