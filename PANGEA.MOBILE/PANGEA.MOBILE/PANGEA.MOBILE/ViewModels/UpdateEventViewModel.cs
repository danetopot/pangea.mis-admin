﻿using FluentValidation;
using FormsToolkit;
using MvvmHelpers;
using PANGEA.MOBILE.Database;
using PANGEA.MOBILE.Helpers;
using PANGEA.MOBILE.Interface;
using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.Views;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PANGEA.MOBILE.ViewModels
{
    public class UpdateEventViewModel : LocalBaseViewModel
    {
        private IAppClient _appClient;
        private INavigation _navigation;
        private readonly IValidator _validator;
        private SystemCodeDetails SystemCodeDetails;
        private int? _childId;
        private int? _lifeventid;

        public UpdateEventViewModel(INavigation navigation, int? childid, int? lifeventid) : base(navigation)
        {
            _appClient = DependencyService.Get<IAppClient>();
            _navigation = navigation;
            _validator = new RegistrationChildrenLifeEventValidator();
            _childId = childid;
            _lifeventid = lifeventid;

            if (!string.IsNullOrEmpty(_childId.ToString()))
            {
                var registrationData = App.Database.GetTableRow("Children", "Id", _childId.ToString());
                Registration = (Children)registrationData;
                FullName = $"{Registration.FullName}";
            }

            #region LoadDefaultMLE
            UpdateLifeEvent = new ChildrenLifeEvents();
            #endregion
        }

        #region Declarations
        public Children Registration { get; set; }

        public ChildrenLifeEvents UpdateLifeEvent { get; set; }

        private int childid;
        public int ChildId
        {
            get { return childid; }
            set { SetProperty(ref childid, value); }
        }

        private string fullname;
        public string FullName
        {
            get { return fullname; }
            set { SetProperty(ref fullname, value); }
        }

        private int? lifeeventid;
        public int? LifeEventId
        {
            get { return lifeeventid; }
            set { SetProperty(ref lifeeventid, value); }
        }

        private string lifeevent;
        public string LifeEvent
        {
            get { return lifeevent; }
            set { SetProperty(ref lifeevent, value); }
        }

        private string attachedMLEs;
        public string AttachedMLEs
        {
            get { return attachedMLEs; }
            set { SetProperty(ref attachedMLEs, value); }
        }
        #endregion

        #region Methods

        private ICommand newUpdateLifeEventAsyncCommand;
        public ICommand NewUpdateLifeEventAsyncCommand => newUpdateLifeEventAsyncCommand ?? (newUpdateLifeEventAsyncCommand = new Command(async () => await ExecuteUpdateLifeEventAsync()));
        private async Task ExecuteUpdateLifeEventAsync()
        {
            if (IsBusy)
            {
                return;
            }

            try
            {
                await Navigation.PushAsync(new EventPage(_childId));
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Error",
                        Message = $"Details\n{e.Message}",
                        Cancel = "OK"
                    });
                return;
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
                await Task.FromResult(0);
            }
        }
        
        
        private ICommand saveLifeEventAsyncCommand;
        public ICommand SaveLifeEventAsyncCommand => saveLifeEventAsyncCommand ?? (saveLifeEventAsyncCommand = new Command(async () => await ExecuteSaveLifeEventAsync()));
        private async Task ExecuteSaveLifeEventAsync()
        {
            if (IsBusy)
            {
                return;
            }

            try
            {
                IsBusy = true;
                Message = "Saving ... ";
                ErrorMessage = string.Empty;

                var model = this.UpdateLifeEvent;
                model.ChildId = (int)_childId;
                model.LifeEvent = LifeEvent;
                model.RecordStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "RECACTIVE")).Id;
                model.SyncStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "SYNCINSERTED")).Id;
                model.IsActive = true;

                var validationResult = _validator.Validate(model);
                if (validationResult.IsValid && ErrorMessage.Length == 0)
                {
                    App.Database.DeleteMLEByIds($"ChildrenLifeEvents", $"{model.ChildId}", $"{model.LifeEvent}");

                    App.Database.Create(model);

                    var childmles = App.Database.GetTableRows("ChildrenLifeEvents", "ChildId", Registration.Id.ToString());
                    var childdocuments = App.Database.GetTableRows("ChildrenDocuments", "ChildId", Registration.Id.ToString());
                    if (childdocuments.Count == 5 && childmles.Count > 0)
                    {
                        var regmodel = this.Registration;
                        regmodel.SyncStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "SYNCREADY")).Id;
                        App.Database.AddOrUpdate(regmodel);
                    }

                    await Task.Delay(TimeSpan.FromSeconds(1));
                    await Navigation.PopAsync();
                    await Navigation.PushAsync(new EventListingPage(_childId));
                }
                else
                {
                    ValidateMessage = GetErrorListFromValidationResult(validationResult).Replace(" id'", "");

                    if (ErrorMessage.Length > 0 || ValidateMessage.Length > 0)
                    {
                        ValidateMessage = $"{ValidateMessage}\n{ErrorMessage}";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert
                            {
                                Title = "Validation Errors",
                                Message = ValidateMessage,
                                Cancel = "OK"
                            });
                        IsBusy = false;

                    }

                }
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Error",
                        Message = $"Details\n{e.Message}",
                        Cancel = "OK"
                    });
                return;
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        // Children LifeEvents Handler
        public ObservableRangeCollection<ChildrenLifeEvents> RegisteredChildrenLifeEvents { get; } = new ObservableRangeCollection<ChildrenLifeEvents>();
        public ObservableCollection<ChildrenLifeEvents> GetRegisteredChildrenLifeEvents()
        {
            try
            {
                var items = App.Database.GetTableRows("ChildrenLifeEvents", "ChildId", _childId.ToString());
                var registeredChildrenLifeEvents = new ObservableCollection<ChildrenLifeEvents>();
                foreach (var item in items)
                {
                    var regChildLifeEvent = (ChildrenLifeEvents)item;
                    registeredChildrenLifeEvents.Add(regChildLifeEvent);
                }
                return registeredChildrenLifeEvents;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }

            return null;
        }

        private ChildrenLifeEvents selectedChildrenLifeEventCommand;
        public ChildrenLifeEvents SelectedChildrenLifeEventCommand
        {
            get { return selectedChildrenLifeEventCommand; }
            set
            {
                selectedChildrenLifeEventCommand = value;
                OnPropertyChanged();
                if (selectedChildrenLifeEventCommand == null)
                    return;

                IsBusy = true;
                Message = $"Loading . . .";
                Task.Delay(TimeSpan.FromSeconds(1));
                Navigation.PushAsync(new EventDetailPage(selectedChildrenLifeEventCommand.Id));

                selectedChildrenLifeEventCommand = null;
                IsBusy = false;
            }
        }

        #endregion

    }
}
