﻿using FluentValidation;
using FormsToolkit;
using MvvmHelpers;
using PANGEA.MOBILE.Database;
using PANGEA.MOBILE.Helpers;
using PANGEA.MOBILE.Interface;
using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PANGEA.MOBILE.ViewModels
{
    public class SettingsViewModel : LocalBaseViewModel
    {
        private IAppClient _appClient;
        private INavigation _navigation;
        private readonly IValidator _validator;
        private SystemCodeDetails SystemCodeDetails;
        private Random _random;
        public SettingsViewModel(INavigation navigation) : base(navigation)
        {
            _appClient = DependencyService.Get<IAppClient>();
            _navigation = navigation;
            _validator = new SettingsValidator();
            _random =  new Random();

            #region LoadDefaultSetting
            SystemCodeDetail = new SystemCodeDetails { IsActive = true, IsSelected = true, IsUserMaintained = true };
            LoadedLocationOptions.AddRange(App.Database.GetLocations());
            LoadedLanguageOptions.AddRange(App.Database.SystemCodeDetailGetByCode("Languages"));
            LoadedDataServerOptions.AddRange(App.Database.SystemCodeDetailGetByCode("Servers"));
            // LoadData();
            #endregion
        }

        #region Declarations
        public event PropertyChangedEventHandler PropertyChanged;

        public int defaultId = 0;
        public int DefaultId
        {
            get =>  _random.Next(500, 1000); 
        }

        private string defaultLocation = $"NOT SET";
        public string DefaultLocation
        {
            get { return defaultLocation; }
            set
            {
                SetProperty(ref defaultLocation, value);
            }
        }


        private string defaultLanguage = $"NOT SET";
        public string DefaultLanguage
        {
            get { return defaultLanguage; }
            set {
                SetProperty(ref defaultLanguage, value);
            }
        }


        private string defaultDataServer = $"NOT SET";
        public string DefaultDataServer
        {
            get { return defaultDataServer; }
            set
            {
                SetProperty(ref defaultDataServer, value);
            }
        }


        private bool deleteOnSync;
        public bool DeleteOnSync
        {
            get { return deleteOnSync; }
            set { SetProperty(ref deleteOnSync, value); }
        }

        private string deleteOnSyncStatus = $"NOT SET";
        public string DeleteOnSyncStatus
        {
            get { return deleteOnSyncStatus; }
            set { SetProperty(ref deleteOnSyncStatus, value); }
        }

        private bool isRefreshing;
        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { SetProperty(ref isRefreshing, value); }
        }
        #endregion

        #region Methods
        public SystemCodeDetails SystemCodeDetail { get; set; }

        private Locations _selectedLocation;
        public Locations SelectedLocation
        {
            get => _selectedLocation;
            set
            {
                if (this._selectedLocation == value)
                {
                    return;
                }

                this._selectedLocation = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<Locations> LocationOptions = new ObservableRangeCollection<Locations>();
        public ObservableRangeCollection<Locations> LoadedLocationOptions
        {
            get => LocationOptions;
            set => SetProperty(ref LocationOptions, value);
        }

        private SystemCodeDetails _selectedLanguage;
        public SystemCodeDetails SelectedLanguage
        {
            get => _selectedLanguage;
            set
            {
                if (this._selectedLanguage == value)
                {
                    return;
                }

                this._selectedLanguage = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> LanguageOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedLanguageOptions
        {
            get => LanguageOptions;
            set => SetProperty(ref LanguageOptions, value);
        }

        private SystemCodeDetails _selectedDataServer;
        public SystemCodeDetails SelectedDataServer
        {
            get => _selectedDataServer;
            set
            {
                if (this._selectedDataServer == value)
                {
                    return;
                }

                this._selectedDataServer = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> DataServerOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedDataServerOptions
        {
            get => DataServerOptions;
            set => SetProperty(ref DataServerOptions, value);
        }

        private ICommand saveSettingAsyncCommand;
        public ICommand SaveSettingsAsyncCommand => saveSettingAsyncCommand ?? (saveSettingAsyncCommand = new Command(async () => await ExecuteSaveSettingAsync()));
        private async Task ExecuteSaveSettingAsync()
        {
            try
            {
                if (IsBusy)
                {
                    return;
                }


                IsBusy = true;
                ErrorMessage = string.Empty;
                Message = "Processing .. ";

                #region Settings Info
                var model = this.SystemCodeDetail;

                if (SelectedLocation != null)
                {
                    model.Id = GetDefaultSettingId();
                    model.SystemCode = $"DefaultLocation";
                    model.SystemDetailCode = $"{SelectedLocation.Code}";
                    model.SystemDetailDescription = $"{SelectedLocation.Name}";
                    App.Database.DeleteAppSettings($"SystemCodeDetails", model.SystemCode);
                    App.Database.AddOrUpdate(model);
                }

                if (SelectedLanguage != null)
                {
                    model.Id = GetDefaultSettingId();
                    model.SystemCode = $"DefaultLanguage";
                    model.SystemDetailCode = $"{SelectedLanguage.SystemDetailCode}";
                    model.SystemDetailDescription = $"{SelectedLanguage.SystemDetailDescription}";
                    App.Database.DeleteAppSettings($"SystemCodeDetails", model.SystemCode);
                    App.Database.AddOrUpdate(model);
                }

                if (SelectedDataServer != null)
                {
                    model.Id = GetDefaultSettingId();
                    model.SystemCode = $"DefaultDataServer";
                    model.SystemDetailCode = $"{SelectedDataServer.SystemDetailCode}";
                    model.SystemDetailDescription = $"{SelectedDataServer.SystemDetailDescription}";
                    App.Database.DeleteAppSettings($"SystemCodeDetails", model.SystemCode);
                    App.Database.AddOrUpdate(model);
                }

                if (DeleteOnSync)
                {
                    model.Id = GetDefaultSettingId();
                    model.SystemCode = $"DefaultDeleteOnSync";
                    model.SystemDetailCode = $"DELETE ON UPLOAD";
                    model.SystemDetailDescription = $"Delete On Sync";
                    App.Database.DeleteAppSettings($"SystemCodeDetails", model.SystemCode);
                    App.Database.AddOrUpdate(model);
                }
                else
                {
                    model.Id = GetDefaultSettingId();
                    model.SystemCode = $"DefaultDeleteOnSync";
                    model.SystemDetailCode = $"KEEP ON UPLOAD";
                    model.SystemDetailDescription = $"KEEP On Sync";
                    App.Database.DeleteAppSettings($"SystemCodeDetails", model.SystemCode);
                    App.Database.AddOrUpdate(model);
                }


                
                await Task.Delay(TimeSpan.FromSeconds(1));

                #endregion

            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Error",
                        Message = $"Details\n{e.Message}",
                        Cancel = "OK"
                    });
                return;
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        private ICommand refreshSettingsAsyncCommand;
        public ICommand RefreshSettingsAsyncCommand => refreshSettingsAsyncCommand ?? (refreshSettingsAsyncCommand = new Command(async () => await ExecuteRefreshSettingsAsync()));
        private async Task ExecuteRefreshSettingsAsync() 
        {
            try
            {
                IsRefreshing = true;
                LoadData();
                IsRefreshing = false;
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Error",
                        Message = $"Details\n{e.Message}",
                        Cancel = "OK"
                    });
                return;
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        public void LoadData()
        {
            try
            {
                // Load Default Settings
                var defaultlanguages = App.Database.SystemCodeDetailGetByCode($"DefaultLanguage");
                if (defaultlanguages.Count > 0)
                    DefaultLanguage = SelectedLanguage.SystemDetailDescription.ToUpper();

                var defaultlocation = App.Database.GetLocationByCode($"DefaultLocation");
                if (defaultlocation != null)
                    DefaultLocation = SelectedLocation.Name.ToUpper();

                var defaultdeleteonsync = App.Database.SystemCodeDetailGetByCode($"DefaultDeleteOnSync");
                if (defaultdeleteonsync.Count > 0)
                    DeleteOnSync = defaultdeleteonsync[0].SystemCode == "DELETE ON UPLOAD" ? true : false;
                    DeleteOnSyncStatus = defaultdeleteonsync[0].SystemCode;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public int GetDefaultSettingId()
        {
           return  _random.Next(500, 1000);
        }
        #endregion

    }
}
