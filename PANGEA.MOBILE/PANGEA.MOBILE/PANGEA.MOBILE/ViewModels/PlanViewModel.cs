﻿using FormsToolkit;
using MvvmHelpers;
using PANGEA.MOBILE.Database;
using PANGEA.MOBILE.Interface;
using PANGEA.MOBILE.Models;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace PANGEA.MOBILE.ViewModels
{
    public class PlanViewModel : LocalBaseViewModel
    {
        private IAppClient _appClient;
        private INavigation _navigation;

        #region RegistrationPlans
        public PlanViewModel(INavigation navigation) : base(navigation)
        {
            _appClient = DependencyService.Get<IAppClient>();
            _navigation = navigation;

            #region LoadData
            IsRegDataReady = GetRegistrationPlans().Count > 0? true : false;
            IsUpdateDataReady = GetUpdatePlans().Count > 0 ? true : false;

            DownloadedRegistrationPlans.ReplaceRange(GetRegistrationPlans());
            DownloadedUpdatePlans.ReplaceRange(GetUpdatePlans());

            #endregion
        }

        #region Declarations
        private bool isRegDataReady;
        public bool IsRegDataReady
        {
            get { return isRegDataReady; }
            set { SetProperty(ref isRegDataReady, value); }
        }

        private bool isUpdateDataReady;
        public bool IsUpdateDataReady
        {
            get { return isUpdateDataReady; }
            set { SetProperty(ref isUpdateDataReady, value); }
        }
        #endregion


        #region Methods
        public ObservableRangeCollection<RegistrationPlans> DownloadedRegistrationPlans { get; } = new ObservableRangeCollection<RegistrationPlans>();
        public ObservableCollection<RegistrationPlans> GetRegistrationPlans()
        {
            try
            {
                var items = App.Database.GetTableRows("RegistrationPlans");
                var regPlans = new ObservableCollection<RegistrationPlans>();
                foreach (var item in items)
                {
                    var regPlan = (RegistrationPlans)item;
                    regPlans.Add(regPlan);
                }
                return regPlans;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }

            return null;
        }

        public ObservableRangeCollection<UpdatePlans> DownloadedUpdatePlans { get; } = new ObservableRangeCollection<UpdatePlans>();
        public ObservableCollection<UpdatePlans> GetUpdatePlans()
        {
            try
            {
                var items = App.Database.GetTableRows("UpdatePlans");
                var updatePlans = new ObservableCollection<UpdatePlans>();
                foreach (var item in items)
                {
                    var regPlan = (UpdatePlans)item;
                    updatePlans.Add(regPlan);
                }
                return updatePlans;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }

            return null;
        } 
        #endregion
        #endregion
    }
}
