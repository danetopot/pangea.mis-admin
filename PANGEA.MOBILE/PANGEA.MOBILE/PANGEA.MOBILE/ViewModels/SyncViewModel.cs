﻿using CsvHelper;
using FormsToolkit;
using PANGEA.MOBILE.Database;
using PANGEA.MOBILE.Helpers;
using PANGEA.MOBILE.Interface;
using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.ViewModels.API;
using PANGEA.MOBILE.Views;
using Plugin.Connectivity;
using Plugin.DeviceInfo;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;


namespace PANGEA.MOBILE.ViewModels
{
    public class SyncViewModel : LocalBaseViewModel
    {
        private IAppClient _appClient;
        private INavigation _navigation;
        private MainPage RootPage { get => Application.Current.MainPage as MainPage; }

        public SyncViewModel(INavigation navigation) : base(navigation)
        {
            _appClient = DependencyService.Get<IAppClient>();
            _navigation = navigation;

            #region LoadDataSyncSettings
            LoadData();
            #endregion
        }

        private ICommand syncUpSettingsAsyncCommand;
        public ICommand SyncUpSettingsAsyncCommand => syncUpSettingsAsyncCommand ?? (syncUpSettingsAsyncCommand = new Command(async () => await ExecuteSyncUpSettingsAsync()));
        private async Task ExecuteSyncUpSettingsAsync() 
        {
            if (IsBusy)
            {
                return;
            }

            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Failed",
                        Message = "Uh Oh, It looks like you have gone offline. \n" +
                                  "Please check your internet connection and try again.",
                        Cancel = "OK"
                    });
                    return;
                }
                else
                {
                    var enrolStatusId = App.Database.SystemCodeDetailGetByCode(null, $"REGNEW").Id;
                    var updateStatusId = App.Database.SystemCodeDetailGetByCode(null, $"REGUPDATE").Id;
                    var qcReadyStatusId = App.Database.SystemCodeDetailGetByCode(null, $"REGUPDATEQCREADY").Id;
                    var insertSyncStatusId = App.Database.SystemCodeDetailGetByCode(null, $"SYNCINSERTED").Id;
                    var readySyncStatusId = App.Database.SystemCodeDetailGetByCode(null, $"SYNCREADY").Id;
                    var uploadSyncStatusId = App.Database.SystemCodeDetailGetByCode(null, $"SYNCUPLOADED").Id;
                    bool deleteonsync = App.Database.SystemCodeDetailGetByCode($"DefaultDeleteOnSync").Count > 0 ? true : false;

                    IsBusy = true;
                    Message = $"Fetching Data. . .";
                    await Task.Delay(TimeSpan.FromSeconds(1));

                    #region Upload Registration Data
                    var model = new ApiSettupViewModel();
                    var regChildren = App.Database.GetTableRows<Children>($"Children", "SyncStatusId", $"{readySyncStatusId}", "ChildStatusId", $"{enrolStatusId}");
                    if(regChildren.Count > 0)
                    {
                        int record = 1;
                        int uploaded = 0;
                        foreach (var regChild in regChildren)
                        {
                            #region DocumentsInfo
                            var regChildDocuments = App.Database.GetTableRows<ChildrenDocuments>($"ChildrenDocuments", "ChildId", $"{regChild.Id}", "SyncStatusId", $"{insertSyncStatusId}");
                            #endregion

                            #region ChoresInfo
                            var regChildChores = App.Database.GetTableRows<ChildrenChores>($"ChildrenChores", "ChildId", $"{regChild.Id}", "SyncStatusId", $"{insertSyncStatusId}");
                            #endregion

                            #region FavouriteActivitiesInfo
                            var regChildFavActivities = App.Database.GetTableRows<ChildrenFavouriteActivities>($"ChildrenFavouriteActivities", "ChildId", $"{regChild.Id}", "SyncStatusId", $"{insertSyncStatusId}");
                            #endregion

                            #region LocalDeviceInfo
                            var regMobile = new LocalDeviceInfo()
                            {
                                Version = CrossDeviceInfo.Current.Version,
                                AppBuild = CrossDeviceInfo.Current.AppBuild,
                                AppVersion = CrossDeviceInfo.Current.AppVersion,
                                DeviceName = CrossDeviceInfo.Current.DeviceName,
                                DeviceId = CrossDeviceInfo.Current.Id,
                                Idiom = CrossDeviceInfo.Current.Idiom,
                                IsDevice = CrossDeviceInfo.Current.IsDevice,
                                DeviceManufacturer = CrossDeviceInfo.Current.Manufacturer,
                                DeviceModel = CrossDeviceInfo.Current.Model,
                                Platform = CrossDeviceInfo.Current.Platform,
                                VersionNumber = CrossDeviceInfo.Current.VersionNumber.ToString()
                            };
                            #endregion

                            #region UploadingInfo
                            Message = $"Uploading Registrations {record}/{regChildren.Count}. . .";
                            await Task.Delay(TimeSpan.FromSeconds(2));
                            var feedback = await _appClient.PostRegistrationAsyc(regChild, regChildChores, regChildFavActivities, regChildDocuments, regMobile);
                            if (feedback.Id > 0)
                            {
                                if (deleteonsync)
                                {
                                    // App.Database.DeleteTableRowByIds($"ChildrenDocuments", $"ChildId", $"{regChild.Id}");
                                    // App.Database.DeleteTableRowByIds($"ChildrenFavouriteActivities", $"ChildId", $"{regChild.Id}");
                                    // App.Database.DeleteTableRowByIds($"ChildrenChores", $"ChildId", $"{regChild.Id}");
                                    //App.Database.DeleteTableRowByIds($"Children", $"Id", $"{regChild.Id}");
                                }
                                else
                                {
                                    //regChild.SyncStatusId = uploadSyncStatusId;
                                    //App.Database.AddOrUpdate(regChild);
                                }
                                uploaded++;
                            }
                            else
                            {
                                Message = $"Error Uploading Registration Data {record}/{regChildren.Count}.\n{feedback.Description}.";
                                await Task.Delay(TimeSpan.FromSeconds(2));
                            }
                            record++; 
                            #endregion
                        }

                        Message = $"Upload Complete.{uploaded}/{regChildren.Count} Registrations Pushed.";
                    }
                    else 
                    { 
                        Message = $"No Registration Data Found To Upload.";
                    }
                    await Task.Delay(TimeSpan.FromSeconds(2));
                    #endregion

                    #region Upload Updates Data
                    var modelupdates = new ApiSettupViewModel();
                    //var regChildrenUpdates = App.Database.GetTableRows<ChildrenUpdates>($"ChildrenUpdates", "SyncStatusId", $"{readySyncStatusId}", "ChildStatusId", $"{enrolStatusId}");
                    var regChildrenUpdates = App.Database.GetTableRows($"ChildrenUpdates");

                    if (regChildrenUpdates.Count > 0)
                    {
                        int record = 1;
                        int uploaded = 0;
                        foreach (var regChildUpdate in regChildrenUpdates)
                        {
                            var regChild = (ChildrenUpdates)regChildUpdate;

                            #region DocumentsInfo
                            var regChildDocuments = App.Database.GetTableRows<ChildrenUpdatesDocuments>($"ChildrenUpdatesDocuments", "ChildId", $"{regChild.Id}");
                            foreach(var regChildDocument in regChildDocuments)
                            {
                                regChildDocument.FilePath = $@"/uploads/registration\";
                            }
                            #endregion

                            #region ChoresInfo
                            var regChildChores = App.Database.GetTableRows<ChildrenChores>($"ChildrenChores", "ChildId", $"{regChild.Id}");
                            #endregion

                            #region FavouriteActivitiesInfo
                            var regChildFavActivities = App.Database.GetTableRows<ChildrenFavouriteActivities>($"ChildrenFavouriteActivities", "ChildId", $"{regChild.Id}");
                            #endregion

                            #region LocalDeviceInfo
                            var regMobile = new LocalDeviceInfo()
                            {
                                Version = CrossDeviceInfo.Current.Version,
                                AppBuild = CrossDeviceInfo.Current.AppBuild,
                                AppVersion = CrossDeviceInfo.Current.AppVersion,
                                DeviceName = CrossDeviceInfo.Current.DeviceName,
                                DeviceId = CrossDeviceInfo.Current.Id,
                                Idiom = CrossDeviceInfo.Current.Idiom,
                                IsDevice = CrossDeviceInfo.Current.IsDevice,
                                DeviceManufacturer = CrossDeviceInfo.Current.Manufacturer,
                                DeviceModel = CrossDeviceInfo.Current.Model,
                                Platform = CrossDeviceInfo.Current.Platform,
                                VersionNumber = CrossDeviceInfo.Current.VersionNumber.ToString()
                            };
                            #endregion

                            #region UploadingInfo
                            Message = $"Uploading Updates {record}/{regChildrenUpdates.Count}. . .";
                            await Task.Delay(TimeSpan.FromSeconds(2));
                            var feedback = await _appClient.PostUpdateAsyc(regChild, regChildChores, regChildFavActivities, regChildDocuments, regMobile);
                            if (feedback.Id > 0)
                            {
                                if (deleteonsync)
                                {
                                    // App.Database.DeleteTableRowByIds($"ChildrenDocuments", $"ChildId", $"{regChild.Id}");
                                    // App.Database.DeleteTableRowByIds($"ChildrenFavouriteActivities", $"ChildId", $"{regChild.Id}");
                                    // App.Database.DeleteTableRowByIds($"ChildrenChores", $"ChildId", $"{regChild.Id}");
                                    //App.Database.DeleteTableRowByIds($"Children", $"Id", $"{regChild.Id}");
                                }
                                else
                                {
                                    //regChild.SyncStatusId = uploadSyncStatusId;
                                    //App.Database.AddOrUpdate(regChild);
                                }
                                uploaded++;
                            }
                            else
                            {
                                Message = $"Error Uploading Updates Data {record}/{regChildrenUpdates.Count}.\n{feedback.Description}.";
                                await Task.Delay(TimeSpan.FromSeconds(2));
                            }
                            record++;
                            #endregion
                        }

                        Message = $"Upload Complete.{uploaded}/{regChildrenUpdates.Count} Updates Pushed.";
                    }
                    else
                    {
                        Message = $"No Registration Data Found To Upload.";
                    }
                    await Task.Delay(TimeSpan.FromSeconds(2));
                    #endregion




                    IsBusy = false;
                }
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error",
                    Message = "Unable to Sync!\n Details : " + ex.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        private ICommand syncDownSettingsAsyncCommand;
        public ICommand SyncDownSettingsAsyncCommand => syncDownSettingsAsyncCommand ?? (syncDownSettingsAsyncCommand = new Command(async () => await ExecuteSyncDownSettingsAsync()));
        private async Task ExecuteSyncDownSettingsAsync()
        {
            if (IsBusy)
            {
                return;
            }

            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Failed",
                        Message = "Uh Oh, It looks like you have gone offline. \n" +
                                  "Please check your internet connection and try again.",
                        Cancel = "OK"
                    });
                    return;
                }
                else
                {
                    IsBusy = true;
                    Message = $"Fetching Data. . .";
                    await Task.Delay(TimeSpan.FromSeconds(1));
                    var model = await _appClient.GetSettingsAsync();

                    Message = "Downloading System Settings . . .";
                    await Task.Delay(TimeSpan.FromSeconds(1));
                    if (model.SystemCodeListings != null)
                    {
                        foreach (var item in model.SystemCodeListings)
                        {
                            item.IsActive = true;
                            App.Database.AddOrUpdate(item);
                        }
                    }

                    Message = "Downloading Locations . . .";
                    await Task.Delay(TimeSpan.FromSeconds(1));
                    if (model.LocationListings != null)
                    {
                        foreach (var item in model.LocationListings)
                        {
                            item.IsActive = true;
                            App.Database.AddOrUpdate(item);
                        }
                    }

                    Message = "Downloading Registration Plans . . .";
                    await Task.Delay(TimeSpan.FromSeconds(1));
                    if (model.RegistrationPlanListings != null)
                    {
                        foreach (var item in model.RegistrationPlanListings)
                        {
                            item.IsActive = true;
                            App.Database.AddOrUpdate(item);
                        }
                    }

                    Message = "Downloading Update Plans . . .";
                    await Task.Delay(TimeSpan.FromSeconds(1));
                    if (model.UpdatePlanListings != null)
                    {
                        foreach (var item in model.UpdatePlanListings)
                        {
                            item.IsActive = true;
                            App.Database.AddOrUpdate(item);
                        }
                    }

                    Message = "Downloading Children Ready for Update . . .";
                    await Task.Delay(TimeSpan.FromSeconds(1));
                    if (model.UpdateChildrenListings != null)
                    {
                        var recordstatusid = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "RECACTIVE")).Id;
                        var childstatusid = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "REGUPDATE")).Id;
                        var documentstatusid = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "DOCPENDING")).Id;
                        var syncstatusid = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "SYNCINSERTED")).Id;
                        foreach (var item in model.UpdateChildrenListings)
                        {
                            // Get Child
                            item.MajorLifeEvent = null;
                            item.DownloadedBy = Settings.Current.UserName;
                            item.DownloadedOn = DateTime.UtcNow.ToString();
                            item.IsActive = true;
                            item.ChildStatusId = childstatusid;
                            item.RecordStatusId = recordstatusid;
                            item.SyncStatusId = syncstatusid;
                            App.Database.AddOrUpdate(item);

                            // Get Chores
                            foreach (var itemc in model.UpdateChildrenChoresListings)
                            {
                                if (itemc.ChildId == item.Id)
                                {
                                    itemc.ChildId = item.Id;
                                    itemc.IsActive = true;
                                    itemc.SyncStatusId = syncstatusid;
                                    App.Database.AddOrUpdate(itemc);
                                }
                            }

                            // Get FavouriteActivities
                            foreach (var itemf in model.UpdateChildrenFavActivitiesListings)
                            {
                                if (itemf.ChildId == item.Id)
                                {
                                    itemf.ChildId = item.Id;
                                    itemf.IsActive = true;
                                    itemf.SyncStatusId = syncstatusid;
                                    App.Database.AddOrUpdate(itemf);
                                }
                            }

                            // Get Documents
                            foreach (var itemd in model.UpdateChildrenDocumentListings)
                            {
                                if (itemd.ChildId == item.Id)
                                {
                                    itemd.ChildId = item.Id;
                                    itemd.IsActive = true;
                                    itemd.DocumentStatusId = documentstatusid;
                                    itemd.SyncStatusId = syncstatusid;
                                    App.Database.AddOrUpdate(itemd);
                                }
                            }
                        }

                        
                    }

                    Message = $"Download Complete";
                    await Task.Delay(TimeSpan.FromSeconds(2));
                    IsBusy = false;
                    IsNotDataReady = false;
                }
            }
            catch(Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error",
                    Message = "Unable to Sync!\n Details : " + ex.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        public void LoadData()
        {
            #region LoadSettings
            var SystemCodeDetail = App.Database.SystemCodeDetailGetByCode($"DefaultDeleteOnSync");
            string deleteonsync = $"DELETE ON UPLOAD";
            DeleteOnSyncStatus = SystemCodeDetail.Count > 0 ? SystemCodeDetail[0].SystemDetailCode : string.Empty;
            DeleteOnSync = (!string.IsNullOrEmpty(DeleteOnSyncStatus) & DeleteOnSyncStatus.Equals($"{deleteonsync}")) ? true : false;
            #endregion
        }
    }
}
