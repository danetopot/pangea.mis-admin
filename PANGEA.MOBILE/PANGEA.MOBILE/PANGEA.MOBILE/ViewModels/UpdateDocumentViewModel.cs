﻿using FluentValidation;
using FormsToolkit;
using MvvmHelpers;
using PANGEA.MOBILE.Database;
using PANGEA.MOBILE.Helpers;
using PANGEA.MOBILE.Interface;
using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.Views;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PANGEA.MOBILE.ViewModels
{
    class UpdateDocumentViewModel : LocalBaseViewModel
    {
        private IAppClient _appClient;
        private INavigation _navigation;
        private readonly IValidator _validator;
        private SystemCodeDetails SystemCodeDetails;
        private Random _random;
        private int? _childId;
        private int? _childDocumentId;

        public UpdateDocumentViewModel(INavigation navigation, int? childid) : base(navigation)
        {
            _appClient = DependencyService.Get<IAppClient>();
            _navigation = navigation;
            _validator = new UpdateChildrenDocumentValidator();
            _childId = childid;

            LoadedDocumentTypeOptions.AddRange(App.Database.SystemCodeDetailGetByCode("Documents To Attach"));

            if (!string.IsNullOrEmpty(_childId.ToString()))
            {
                var registrationData = App.Database.GetTableRow("ChildrenUpdates", "Id", _childId.ToString());
                Registration = (ChildrenUpdates)registrationData;
                FullName = $"{Registration.FirstName} {Registration.MiddleName} {Registration.LastName}";
                ChildStatusId = (int)Registration.ChildStatusId;
                EnrolStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "REGNEW")).Id;
                UpdateStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "REGUPDATE")).Id;
                SyncReadyStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "SYNCREADY")).Id;
            }

            #region LoadDefaultDocument
            RegistrationDocument = new ChildrenUpdatesDocuments();
            #endregion
        }

        #region Declarations
        public ChildrenUpdates Registration { get; set; }

        public ChildrenUpdatesDocuments RegistrationDocument { get; set; }

        private int childid;
        public int ChildId
        {
            get { return childid; }
            set { SetProperty(ref childid, value); }
        }

        private int childstatusid;
        public int ChildStatusId
        {
            get { return childstatusid; }
            set { SetProperty(ref childstatusid, value); }
        }

        private int enrolstatusid;
        public int EnrolStatusId
        {
            get { return enrolstatusid; }
            set { SetProperty(ref enrolstatusid, value); }
        }

        private int updatestatusid;
        public int UpdateStatusId
        {
            get { return updatestatusid; }
            set { SetProperty(ref updatestatusid, value); }
        }

        private int syncreadystatusid;
        public int SyncReadyStatusId
        {
            get { return syncreadystatusid; }
            set { SetProperty(ref syncreadystatusid, value); }
        }

        //private int documentid;
        //public int DocumentId
        //{
        //    get { return documentid; }
        //    set { SetProperty(ref documentid, value); }
        //}

        private string fullname;
        public string FullName
        {
            get { return fullname; }
            set { SetProperty(ref fullname, value); }
        }

        private int documentid;
        public int DocumentId
        {
            get { return documentid; }
            set { SetProperty(ref documentid, value); }
        }

        private int documenttypeid;
        public int DocumentTypeId
        {
            get { return documenttypeid; }
            set { SetProperty(ref documenttypeid, value); }
        }

        private byte[] documentimage;
        public byte[] DocumentImage
        {
            get { return documentimage; }
            set { SetProperty(ref documentimage, value); }
        }

        private string documentpath;
        public string DocumentPath
        {
            get { return documentpath; }
            set { SetProperty(ref documentpath, value); }
        }

        private string documentname;
        public string DocumentName
        {
            get { return documentname; }
            set { SetProperty(ref documentname, value); }
        }

        private bool isDocumentLoaded = false;
        public bool IsDocumentLoaded
        {
            get { return isDocumentLoaded; }
            set { SetProperty(ref isDocumentLoaded, value); }
        }
        #endregion

        #region Methods
        // DocumentType Handler
        private SystemCodeDetails _selectedDocumentType;
        public SystemCodeDetails SelectedDocumentType
        {
            get => _selectedDocumentType;
            set
            {
                if (this._selectedDocumentType == value)
                {
                    return;
                }

                this._selectedDocumentType = value;

                this.RegistrationDocument.DocumentTypeId = _selectedDocumentType.Id;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> DocumentTypeOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedDocumentTypeOptions
        {
            get => DocumentTypeOptions;
            set => SetProperty(ref DocumentTypeOptions, value);
        }

        // Save New Photo Page
        private ICommand saveDocumentAsyncCommand;
        public ICommand SaveDocumentAsyncCommand => saveDocumentAsyncCommand ?? (saveDocumentAsyncCommand = new Command(async () => await ExecuteSaveDocumentAsyncCommand()));
        private async Task ExecuteSaveDocumentAsyncCommand()
        {
            if (IsBusy)
            {
                return;
            }

            try
            {
                IsBusy = true;
                ErrorMessage = string.Empty;

                var model = this.RegistrationDocument;
                model.ChildId = Registration.Id;      
                model.FilePath = DocumentPath;
                model.FileName = SelectedDocumentType != null ? 
                    $"{Registration.Id}_{SelectedDocumentType.SystemDetailDescription}_{DateTime.Now.ToString("yyyymmddhhmmss")}.PNG".ToUpper().Trim() 
                    : string.Empty;
                model.FileBinary = DocumentImage;
                model.IsDocumentRequired = true;
                model.IsTranslationRequired = false;
                model.DocumentTypeId = SelectedDocumentType != null ? SelectedDocumentType.Id : 0;
                model.DocumentStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "DOCQCREADY")).Id;
                model.RecordStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "RECACTIVE")).Id;
                model.SyncStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "SYNCREADY")).Id;
                model.IsActive = true;

                var validationResult = _validator.Validate(model);
                if (validationResult.IsValid && ErrorMessage.Length == 0)
                {
                    model.Id = App.Database.GetTableRows<ChildrenUpdatesDocuments>($"ChildrenUpdatesDocuments", "ChildId", $"{model.ChildId}", "DocumentTypeId", $"{model.DocumentTypeId}").FirstOrDefault().Id;
                    App.Database.AddOrUpdate(model);

                    Message = "Processing .. ";
                    IsDocumentLoaded = true;
                    await Task.Delay(TimeSpan.FromSeconds(2));
                    await Navigation.PopAsync();

                    if (Settings.Current.Module == $"Registrations")
                        await Navigation.PushAsync(new RegistrationEditPage(Registration.Id));

                    if (Settings.Current.Module == $"Updates")
                        await Navigation.PushAsync(new UpdatePage(Registration.Id));
                }
                else
                {
                    ValidateMessage = GetErrorListFromValidationResult(validationResult).Replace(" id'", "");

                    if (ErrorMessage.Length > 0 || ValidateMessage.Length > 0)
                    {
                        ValidateMessage = $"{ValidateMessage}\n{ErrorMessage}";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert
                            {
                                Title = "Validation Errors",
                                Message = ValidateMessage,
                                Cancel = "OK"
                            });
                        IsBusy = false;

                    }

                }
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Error",
                        Message = $"Details\n{e.Message}",
                        Cancel = "OK"
                    });
                return;
            }
            finally
            {
                IsBusy = false;
                await Task.FromResult(0);
            }
        }
        #endregion
    }
}
