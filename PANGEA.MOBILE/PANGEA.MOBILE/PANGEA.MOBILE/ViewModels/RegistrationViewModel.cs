﻿using FluentValidation;
using FormsToolkit;
using MvvmHelpers;
using PANGEA.MOBILE.Database;
using PANGEA.MOBILE.Helpers;
using PANGEA.MOBILE.Interface;
using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PANGEA.MOBILE.ViewModels
{

    #region Create Registration
    public class RegistrationViewModel : LocalBaseViewModel
    {
        private IAppClient _appClient;
        private INavigation _navigation;
        private readonly IValidator _validator;
        private SystemCodeDetails SystemCodeDetails;

        public RegistrationViewModel(INavigation navigation) : base(navigation)
        {
            _appClient = DependencyService.Get<IAppClient>();
            _navigation = navigation;
            _validator = new RegistrationChildrenValidator();


            LoadedSexOptions.AddRange(App.Database.SystemCodeDetailGetByCode("Gender"));
            LoadedDisabilityOptions.AddRange(App.Database.SystemCodeDetailGetByCode("DefaultResponse"));
            LoadedFavouriteLearningOptions.AddRange(App.Database.SystemCodeDetailGetByCode("Favourite Learning"));
            LoadedDisabilityOptions.AddRange(App.Database.SystemCodeDetailGetByCode("DefaultResponse"));
            LoadedSchoolGradeOptions.AddRange(App.Database.SystemCodeDetailGetByCode("Grade Level"));
            LoadedPersonalityOptions.AddRange(App.Database.SystemCodeDetailGetByCode("Child Personality"));
            LoadedLivesWithOptions.AddRange(App.Database.SystemCodeDetailGetByCode("Lives With"));
            LoadedLocationOptions.AddRange(App.Database.GetLocations());

            #region LoadDefaultChild
            RegisteredChild = new Children(); 
            #endregion

            #region LoadedChoreOptions
            var chorelisting = new List<SelectableItemWrapper<SystemCodeDetails>>();
            var _chorelist = App.Database.SystemCodeDetailGetByCode("Chores").ToList();
            foreach (var item in _chorelist)
            {
                chorelisting.Add(new SelectableItemWrapper<SystemCodeDetails>
                { Item = item, IsSelected = false });
            }
            LoadedChores.AddRange(chorelisting);
            #endregion

            #region LoadedFavouriteActivities
            var favactivitylisting = new List<SelectableItemWrapper<SystemCodeDetails>>();
            var _favouriteactivitieslist = App.Database.SystemCodeDetailGetByCode("Favourite Activity").ToList();
            foreach (var item in _favouriteactivitieslist)
            {
                favactivitylisting.Add(new SelectableItemWrapper<SystemCodeDetails>
                { Item = item, IsSelected = false });
            }
            LoadedFavouriteActivities.AddRange(favactivitylisting);
            #endregion

            #region LoadDefaultDates
            DateOfBirth = DateFormatter.ToSQLiteDateTimeString(DateTime.Now);
            DATE_OF_BIRTH = Convert.ToDateTime(DateOfBirth);
            #endregion


        }

        #region Declarations
        public Children RegisteredChild { get; set; }

        private int id;
        public int Id
        {
            get { return id; }
            set { SetProperty(ref id, value); }
        }

        private string firstname;
        public string FirstName
        {
            get { return firstname; }
            set { SetProperty(ref firstname, value); }
        }

        private string middlename;
        public string MiddleName
        {
            get { return middlename; }
            set { SetProperty(ref middlename, value); }
        }

        private string lastname;
        public string LastName
        {
            get { return lastname; }
            set { SetProperty(ref lastname, value); }
        }

        private string nickname;
        public string NickName
        {
            get { return nickname; }
            set { SetProperty(ref nickname, value); }
        }

        private int gender;
        public int Gender
        {
            get { return gender; }
            set { SetProperty(ref gender, value); }
        }

        private string dateofbirth;
        public string DateOfBirth
        {
            get { return dateofbirth; }
            set 
            { 
                SetProperty(ref dateofbirth, value);
                this.OnPropertyChanged();
            }
        }

        private DateTime DOB;
        public DateTime DATE_OF_BIRTH
        {
            get { return DOB; }
            set
            {
                if (this.DOB == value) return;
                DOB = value;
                var dob = DateTime.Parse(DOB.ToString());
                this.OnPropertyChanged();
            }
        }

        private bool isdateofbirthapproximated;
        public bool IsDateOfBirthApproximated
        {
            get { return isdateofbirthapproximated; }
            set { SetProperty(ref isdateofbirthapproximated, value); }
        }

        private int disabilitystatusid;
        public int DisabilityStatusId
        {
            get { return disabilitystatusid; }
            set { SetProperty(ref disabilitystatusid, value); }
        }

        private int numberofbrothers;
        public int NumberOfBrothers
        {
            get { return numberofbrothers; }
            set { SetProperty(ref numberofbrothers, value); }
        }

        private int numberofsisters;
        public int NumberOfSisters
        {
            get { return numberofsisters; }
            set { SetProperty(ref numberofsisters, value); }
        }

        private int favouritelearningid;
        public int FavouriteLearningId
        {
            get { return favouritelearningid; }
            set { SetProperty(ref favouritelearningid, value); }
        }

        private int gradelevelid;
        public int GradeLevelId
        {
            get { return gradelevelid; }
            set { SetProperty(ref gradelevelid, value); }
        }

        private int healthstatusid;
        public int HealthStatusId
        {
            get { return healthstatusid; }
            set { SetProperty(ref healthstatusid, value); }
        }

        private int personalityid;
        public int PersonalityId
        {
            get { return personalityid; }
            set { SetProperty(ref personalityid, value); }
        }

        private int liveswithid;
        public int LivesWithId
        {
            get { return liveswithid; }
            set { SetProperty(ref liveswithid, value); }
        }

        private int locationid;
        public int LocationId
        {
            get { return locationid; }
            set { SetProperty(ref locationid, value); }
        }

        private int registrationplanid;
        public int RegistrationPlanId
        {
            get { return registrationplanid; }
            set { SetProperty(ref registrationplanid, value); }
        }

        private string dateofregistration;
        public string DateOfRegistration
        {
            get { return dateofregistration; }
            set
            {
                SetProperty(ref dateofregistration, value);
                this.OnPropertyChanged();
            }
        }

        private string dateofenrolment;
        public string DateOfEnrolment
        {
            get { return dateofenrolment; }
            set
            {
                SetProperty(ref dateofenrolment, value);
                this.OnPropertyChanged();
            }
        }

        private DateTime REGDATE;
        public DateTime DATE_OF_REGISTRATION
        {
            get { return DOB; }
            set
            {
                if (this.REGDATE == value) return;
                REGDATE = value;
                var dob = DateTime.Parse(REGDATE.ToString());
                this.OnPropertyChanged();
            }
        }

        private string dateofupdate;
        public string DateOfUpdate
        {
            get { return dateofupdate; }
            set
            {
                SetProperty(ref dateofupdate, value);
                this.OnPropertyChanged();
            }
        }

        private DateTime UPDATEDATE;
        public DateTime DATE_OF_UPDATE
        {
            get { return DOB; }
            set
            {
                if (this.UPDATEDATE == value) return;
                UPDATEDATE = value;
                var dob = DateTime.Parse(UPDATEDATE.ToString());
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region Methods
        // Load New Registration Page
        private ICommand newRegistrationAsyncCommand;
        public ICommand NewRegistrationAsyncCommand => newRegistrationAsyncCommand ?? (newRegistrationAsyncCommand = new Command(async () => await ExecuteNewRegistrationAsync()));
        private async Task ExecuteNewRegistrationAsync()
        {
            if (IsBusy)
            {
                return;
            }

            try
            {
                await Navigation.PushAsync(new RegistrationPage(null));
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Error",
                        Message = $"Details\n{e.Message}",
                        Cancel = "OK"
                    });
                return;
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        // Save Registration
        private ICommand saveRegistrationAsyncCommand;
        public ICommand SaveRegistrationAsyncCommand => saveRegistrationAsyncCommand ?? (saveRegistrationAsyncCommand = new Command(async () => await ExecuteSaveRegistrationAsync()));
        private async Task ExecuteSaveRegistrationAsync()
        {
            try
            {
                if (IsBusy)
                {
                    return;
                }


                IsBusy = true;
                Message = "Saving ... ";
                ErrorMessage = string.Empty;

                #region Enrolment Info
                var model = this.RegisteredChild;
                model.Id = GetRegisteredChildId();
                model.FirstName = FirstName;
                model.MiddleName = MiddleName;
                model.LastName = LastName;
                model.Nickname = NickName;
                model.GenderId = SelectedGender!= null? SelectedGender.Id : 0;
                model.DateOfBirth = DateFormatter.ToSQLiteDateTimeString(DATE_OF_BIRTH);
                model.IsDateOfBirthApproximated = IsDateOfBirthApproximated;
                model.GradeLevelId = SelectedSchoolGrade != null ? SelectedSchoolGrade.Id : 0;
                //model.HealthStatusId = SelectedHealthStatus.Id;
                model.LivesWithId = SelectedLivesWith != null ? SelectedLivesWith.Id : 0;
                model.FavouriteLearningId = SelectedFavouriteLearning != null ? SelectedFavouriteLearning.Id : 0;
                model.PersonalityId = SelectedPersonality != null ? SelectedPersonality.Id : 0;
                model.NumberOfBrothers = NumberOfBrothers;
                model.NumberOfSisters = NumberOfSisters;
                model.DisabilityStatusId = SelectedDisability != null ? SelectedDisability.Id : 0;
                model.LocationId = SelectedLocation != null ? SelectedLocation.Id : 0;
                //model.RegistrationPlanId = SelectedRegistrationPlan.Id;
                model.DateOfRegistration = DateFormatter.ToSQLiteDateTimeString(DATE_OF_REGISTRATION); ;
                model.ChildStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "REGNEW")).Id;
                model.RecordStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "RECACTIVE")).Id;
                model.RecordSourceId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "MOBILE")).Id;
                model.SyncStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "SYNCINSERTED")).Id;
                model.IsActive = true;

                var selectedchores = GetSelectedChores();
                if (selectedchores.Count == 0)
                    ErrorMessage += $"Chores is required \n";

                var selectedfavactivities = GetSelectedFavouriteActivities();
                if (selectedfavactivities.Count == 0)
                    ErrorMessage += $"Favourite Activities is required \n";

                var validationResult = _validator.Validate(model);
                if (validationResult.IsValid && ErrorMessage.Length == 0)
                {
                    #region Enrolment Info
                    App.Database.AddOrUpdate(model);
                    #endregion

                    #region Chores Info
                    if (selectedchores.Any())
                    {
                        foreach (var item in selectedchores)
                        {
                            var childchore = new ChildrenChores
                            {
                                ChildId = GetRegisteredChildId(),
                                ChoreId = item.Id,
                                RecordStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "RECACTIVE")).Id,
                                SyncStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "SYNCREADY")).Id,
                                IsActive = true
                            };
                            App.Database.Create(childchore);
                        }
                    }
                    #endregion

                    #region Favourite Activities Info
                    if (selectedfavactivities.Any())
                    {
                        foreach (var item in selectedfavactivities)
                        {
                            var childfavactivity = new ChildrenFavouriteActivities
                            {
                                ChildId = GetRegisteredChildId(),
                                FavouriteActivityId = item.Id,
                                RecordStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "RECACTIVE")).Id,
                                SyncStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "SYNCREADY")).Id,
                                IsActive = true
                            };
                            App.Database.Create(childfavactivity);
                        }
                    }
                    #endregion

                    await Task.Delay(TimeSpan.FromSeconds(2));
                    await Navigation.PopAsync();
                    await Navigation.PushAsync(new RegistrationListingPage());
                }
                else
                {
                    ValidateMessage = GetErrorListFromValidationResult(validationResult).Replace(" id'", "");

                    if (ErrorMessage.Length > 0 || ValidateMessage.Length > 0)
                    {
                        ValidateMessage = $"{ValidateMessage}\n{ErrorMessage}";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert
                            {
                                Title = "Validation Errors",
                                Message = ValidateMessage,
                                Cancel = "OK"
                            });
                        IsBusy = false;

                    }

                }
                #endregion

            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Error",
                        Message = $"Details\n{e.Message}",
                        Cancel = "OK"
                    });
                return;
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        // Child Handler
        private Children selectedChildrenCommand;
        public Children SelectedChildrenCommand
        {
            get { return selectedChildrenCommand; }
            set
            {
                selectedChildrenCommand = value;
                OnPropertyChanged();
                if (selectedChildrenCommand == null)
                    return;

                IsBusy = true;
                Message = $"Loading . . .";
                Task.Delay(TimeSpan.FromSeconds(1));
                Navigation.PushAsync(new RegistrationEditPage(selectedChildrenCommand.Id));

                selectedChildrenCommand = null;
                IsBusy = false;
            }
        }

        // Sex Handler
        private SystemCodeDetails _selectedSex;
        public SystemCodeDetails SelectedGender
        {
            get => _selectedSex;
            set
            {
                if (this._selectedSex == value)
                {
                    return;
                }

                this._selectedSex = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> SexOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedSexOptions
        {
            get => SexOptions;
            set => SetProperty(ref SexOptions, value);
        }

        // Health Status Handler
        private SystemCodeDetails _selectedHealthStatus;
        public SystemCodeDetails SelectedHealthStatus
        {
            get => _selectedHealthStatus;
            set
            {
                if (this._selectedHealthStatus == value)
                {
                    return;
                }

                this._selectedHealthStatus = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> HealthStatusOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedHealthStatusOptions
        {
            get => HealthStatusOptions;
            set => SetProperty(ref HealthStatusOptions, value);
        }

        // Disability Handler
        private SystemCodeDetails _selectedDisability;
        public SystemCodeDetails SelectedDisability
        {
            get => _selectedDisability;
            set
            {
                if (this._selectedDisability == value)
                {
                    return;
                }

                this._selectedDisability = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> DisabilityOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedDisabilityOptions
        {
            get => DisabilityOptions;
            set => SetProperty(ref DisabilityOptions, value);
        }

        // FavouriteLearning Handler
        private SystemCodeDetails _selectedFavouriteLearning;
        public SystemCodeDetails SelectedFavouriteLearning
        {
            get => _selectedFavouriteLearning;
            set
            {
                if (this._selectedFavouriteLearning == value)
                {
                    return;
                }

                this._selectedFavouriteLearning = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> FavouriteLearningOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedFavouriteLearningOptions
        {
            get => FavouriteLearningOptions;
            set => SetProperty(ref FavouriteLearningOptions, value);
        }

        // SchoolGrade Handler
        private SystemCodeDetails _selectedSchoolGrade;
        public SystemCodeDetails SelectedSchoolGrade
        {
            get => _selectedSchoolGrade;
            set
            {
                if (this._selectedSchoolGrade == value)
                {
                    return;
                }

                this._selectedSchoolGrade = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> SchoolGradeOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedSchoolGradeOptions
        {
            get => SchoolGradeOptions;
            set => SetProperty(ref SchoolGradeOptions, value);
        }


        // Personality Handler
        private SystemCodeDetails _selectedPersonality;
        public SystemCodeDetails SelectedPersonality
        {
            get => _selectedPersonality;
            set
            {
                if (this._selectedPersonality == value)
                {
                    return;
                }

                this._selectedPersonality = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> PersonalityOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedPersonalityOptions
        {
            get => PersonalityOptions;
            set => SetProperty(ref PersonalityOptions, value);
        }

        // LivesWith Handler
        private SystemCodeDetails _selectedLivesWith;
        public SystemCodeDetails SelectedLivesWith
        {
            get => _selectedLivesWith;
            set
            {
                if (this._selectedLivesWith == value)
                {
                    return;
                }

                this._selectedLivesWith = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> LivesWithOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedLivesWithOptions
        {
            get => LivesWithOptions;
            set => SetProperty(ref LivesWithOptions, value);
        }

        // Chores Handler
        private SystemCodeDetails _selectedChore;
        public SystemCodeDetails SelectedChore
        {
            get => _selectedChore;
            set
            {
                if (this._selectedChore == value)
                {
                    return;
                }

                this._selectedChore = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> ChoreOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedChoreOptions
        {
            get { return ChoreOptions; }
            set => SetProperty(ref ChoreOptions, value);
        }
        public ObservableRangeCollection<SelectableItemWrapper<SystemCodeDetails>> _loadedChores = new ObservableRangeCollection<SelectableItemWrapper<SystemCodeDetails>>();
        public ObservableRangeCollection<SelectableItemWrapper<SystemCodeDetails>> LoadedChores
        {
            get { return _loadedChores; }
            set => SetProperty(ref _loadedChores, value);
        }
        ObservableCollection<SystemCodeDetails> GetSelectedChores()
        {
            var selected = LoadedChores
                .Where(p => p.IsSelected)
                .Select(p => p.Item)
                .ToList();
            return new ObservableCollection<SystemCodeDetails>(selected);
        }

        // FavouriteActivity Handler
        private SystemCodeDetails _selectedFavouriteActivity;
        public SystemCodeDetails SelectedFavouriteActivity
        {
            get => _selectedFavouriteActivity;
            set
            {
                if (this._selectedFavouriteActivity == value)
                {
                    return;
                }

                this._selectedFavouriteActivity = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> FavouriteActivityOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SelectableItemWrapper<SystemCodeDetails>> _loadedFavouriteActivities = new ObservableRangeCollection<SelectableItemWrapper<SystemCodeDetails>>();
        public ObservableRangeCollection<SelectableItemWrapper<SystemCodeDetails>> LoadedFavouriteActivities
        {
            get { return _loadedFavouriteActivities; }
            set => SetProperty(ref _loadedFavouriteActivities, value);
        }
        ObservableCollection<SystemCodeDetails> GetSelectedFavouriteActivities()
        {
            var selected = LoadedFavouriteActivities
                .Where(p => p.IsSelected)
                .Select(p => p.Item)
                .ToList();
            return new ObservableCollection<SystemCodeDetails>(selected);
        }

        // Location Handler
        private Locations _selectedLocation;
        public Locations SelectedLocation
        {
            get => _selectedLocation;
            set
            {
                if (this._selectedLocation == value)
                {
                    return;
                }

                this._selectedLocation = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<Locations> LocationOptions = new ObservableRangeCollection<Locations>();
        public ObservableRangeCollection<Locations> LoadedLocationOptions
        {
            get => LocationOptions;
            set => SetProperty(ref LocationOptions, value);
        }

        // Registration Plan Handler
        private SystemCodeDetails _selectedRegistrationPlan;
        public SystemCodeDetails SelectedRegistrationPlan
        {
            get => _selectedRegistrationPlan;
            set
            {
                if (this._selectedRegistrationPlan == value)
                {
                    return;
                }

                this._selectedRegistrationPlan = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> RegistrationPlanOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedRegistrationPlanOptions
        {
            get => RegistrationPlanOptions;
            set => SetProperty(ref RegistrationPlanOptions, value);
        }


        // Children Handler
        public ObservableRangeCollection<Children> RegisteredChildren { get; } = new ObservableRangeCollection<Children>();
        public ObservableCollection<Children> GetRegisteredChildren()
        {
            try
            {
                var regstatusid = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "REGNEW")).Id;
                var items = App.Database.GetTableRows($"Children", $"ChildStatusId", $"{regstatusid}");
                var registeredChildren = new ObservableCollection<Children>();
                foreach (var item in items)
                {
                    var regChild = (Children)item;
                    var regChildImgDocs = (ChildrenDocuments)App.Database.GetProfileImage($"{regChild.Id}");
                    var regChildAttachments = App.Database.GetTableRows<ChildrenDocuments>($"ChildrenDocuments", "ChildId", $"{regChild.Id}");
                    var regChildSyncStatus = (App.Database.SystemCodeDetailGetById((int)regChild.SyncStatusId)).SystemDetailCode;


                    regChild.Uploaded = regChildSyncStatus.Equals("SYNCUPLOADED") ? $"Uploaded" : $"Not Uploaded";
                    regChild.TotalAttachments =$"{regChildAttachments.Count().ToString()}/5  Documents";
                    regChild.ProfileImage = regChildImgDocs == null? $"person.png" : regChildImgDocs.FilePath.ToString();
                    //regChild.DateOfEnrolment =  String.Format("{0:dd-MM-yyyy}", regChild.DateOfRegistration);
                    registeredChildren.Add(regChild);
                }

                var filtered = registeredChildren.Where(x => x.Haystack.ToLower().Contains(filter.ToLower()));

                return registeredChildren;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }

            return null;
        }

        public int GetRegisteredChildId()
        {
            var registredChildren = App.Database.GetTableRows("ChildrenDocuments");
            return registredChildren.Count() + 1;
        }

        #endregion
    }
    #endregion

    #region Edit Registration
    public class RegistrationEditViewModel : LocalBaseViewModel
    {
        private IAppClient _appClient;
        private INavigation _navigation;
        private readonly IValidator _validator;
        private SystemCodeDetails SystemCodeDetails;
        private int? _childId;

        public RegistrationEditViewModel(INavigation navigation, int? childid) : base(navigation)
        {
            _appClient = DependencyService.Get<IAppClient>();
            _navigation = navigation;
            _validator = new RegistrationChildrenValidator();
            _childId = childid;

            LoadedSexOptions.AddRange(App.Database.SystemCodeDetailGetByCode("Gender"));
            LoadedDisabilityOptions.AddRange(App.Database.SystemCodeDetailGetByCode("DefaultResponse"));
            LoadedFavouriteLearningOptions.AddRange(App.Database.SystemCodeDetailGetByCode("Favourite Learning"));
            LoadedDisabilityOptions.AddRange(App.Database.SystemCodeDetailGetByCode("DefaultResponse"));
            LoadedSchoolGradeOptions.AddRange(App.Database.SystemCodeDetailGetByCode("Grade Level"));
            LoadedPersonalityOptions.AddRange(App.Database.SystemCodeDetailGetByCode("Child Personality"));
            LoadedLivesWithOptions.AddRange(App.Database.SystemCodeDetailGetByCode("Lives With"));
            LoadedLocationOptions.AddRange(App.Database.GetLocations());

            #region LoadedEnrolmentData
            if (!string.IsNullOrEmpty(_childId.ToString()))
            {
                var childdata = App.Database.GetTableRow("Children", "Id", _childId.ToString());
                RegisteredChild = (Children)childdata;
                FirstName = RegisteredChild.FirstName;
                MiddleName = RegisteredChild.MiddleName;
                LastName = RegisteredChild.LastName;
                NickName = RegisteredChild.Nickname;
                FullName = $"{RegisteredChild.FirstName} {RegisteredChild.MiddleName} {RegisteredChild.LastName}".ToUpper();
                SelectedGender = LoadedSexOptions.FirstOrDefault(x => x.Id == RegisteredChild.GenderId) ?? null;
                DateOfBirth = RegisteredChild.DateOfBirth;
                DATE_OF_BIRTH = Convert.ToDateTime(DateOfBirth);
                IsDateOfBirthApproximated = RegisteredChild.IsDateOfBirthApproximated;
                SelectedSchoolGrade = LoadedSchoolGradeOptions.FirstOrDefault(x => x.Id == RegisteredChild.GradeLevelId) ?? null;
                SelectedLivesWith = LoadedLivesWithOptions.FirstOrDefault(x => x.Id == RegisteredChild.LivesWithId) ?? null;
                SelectedFavouriteLearning = LoadedFavouriteLearningOptions.FirstOrDefault(x => x.Id == RegisteredChild.FavouriteLearningId) ?? null;
                SelectedPersonality = LoadedPersonalityOptions.FirstOrDefault(x => x.Id == RegisteredChild.PersonalityId) ?? null;
                NumberOfBrothers = (int)RegisteredChild.NumberOfBrothers;
                NumberOfSisters = (int)RegisteredChild.NumberOfSisters;
                SelectedDisability = LoadedDisabilityOptions.FirstOrDefault(x => x.Id == RegisteredChild.DisabilityStatusId) ?? null;
                SelectedLocation = LoadedLocationOptions.FirstOrDefault(x => x.Id == RegisteredChild.LocationId) ?? null;
                DateOfRegistration = RegisteredChild.DateOfRegistration;
                DATE_OF_REGISTRATION = Convert.ToDateTime(DateOfRegistration);

                #region LoadedDocuments
                var loadedDocuments = App.Database.GetTableRows("ChildrenDocuments", "ChildId", _childId.ToString());
                var regChildImgDocs = (ChildrenDocuments)App.Database.GetProfileImage($"{RegisteredChild.Id}");
                var regChildSyncStatus = (App.Database.SystemCodeDetailGetById((int)RegisteredChild.SyncStatusId)).SystemDetailCode;
                UploadedTag = regChildSyncStatus.Equals("SYNCUPLOADED") ? $"Uploaded" : $"Not Uploaded";
                DocumentTag = $"{loadedDocuments.Count.ToString()} Documents";
                LocationTag = $"{SelectedLocation.Name}";
                NumberOfDocuments = $" Manage Documents  {loadedDocuments.Count.ToString()}/5";
                ProfileImage = $"person.png";

                foreach (var loadedDocument in loadedDocuments)
                {
                    var docitem = (ChildrenDocuments)loadedDocument;
                    var doctype = (App.Database.SystemCodeDetailGetById((int)docitem.DocumentTypeId)).SystemDetailCode;
                    if(doctype == "PROFILE IMAGE")
                    {
                        ProfileImage = docitem.FilePath;
                    }
                }
                #endregion

                #region LoadedChoreOptions
                var chorelisting = new List<SelectableItemWrapper<SystemCodeDetails>>();
                var _chorelist = App.Database.SystemCodeDetailGetByCode("Chores").ToList();
                var _selectedchores = App.Database.GetTableRows<ChildrenChores>("ChildrenChores", "ChildId", childid.ToString());
                if (!_selectedchores.Any())
                    foreach (var item in _chorelist)
                        chorelisting.Add(new SelectableItemWrapper<SystemCodeDetails> { Item = item, IsSelected = false });
                else
                    foreach (var item in _chorelist)
                        chorelisting.Add(new SelectableItemWrapper<SystemCodeDetails> { Item = item, IsSelected = _selectedchores.Any(x => x.ChoreId == item.Id) });

                LoadedChores.AddRange(chorelisting);
                #endregion

                #region LoadedFavouriteActivities
                var favactivitylisting = new List<SelectableItemWrapper<SystemCodeDetails>>();
                var _favactivitylist = App.Database.SystemCodeDetailGetByCode("Favourite Activity").ToList();
                var _selectedfavactivities = App.Database.GetTableRows<ChildrenFavouriteActivities>("ChildrenFavouriteActivities", "ChildId", childid.ToString());

                if (!_selectedfavactivities.Any())
                    foreach (var item in _favactivitylist)
                        favactivitylisting.Add(new SelectableItemWrapper<SystemCodeDetails> { Item = item, IsSelected = false });
                else
                    foreach (var item in _favactivitylist)
                        favactivitylisting.Add(new SelectableItemWrapper<SystemCodeDetails> { Item = item, IsSelected = _selectedfavactivities.Any(x => x.FavouriteActivityId == item.Id) });

                LoadedFavouriteActivities.AddRange(favactivitylisting);
                #endregion

                AttachedDocuments = $"Attached Files {loadedDocuments.Count.ToString()}".ToUpper();
                PendingDocuments = $"Pending Files {5 - loadedDocuments.Count}".ToUpper();

            }
            #endregion
        }

        #region Declarations
        public Children RegisteredChild { get; set; }

        private int id;
        public int Id
        {
            get { return id; }
            set { SetProperty(ref id, value); }
        }

        private string firstname;
        public string FirstName
        {
            get { return firstname; }
            set { SetProperty(ref firstname, value); }
        }

        private string middlename;
        public string MiddleName
        {
            get { return middlename; }
            set { SetProperty(ref middlename, value); }
        }

        private string lastname;
        public string LastName
        {
            get { return lastname; }
            set { SetProperty(ref lastname, value); }
        }

        private string fullname;
        public string FullName
        {
            get { return fullname; }
            set { SetProperty(ref fullname, value); }
        }

        private string nickname;
        public string NickName
        {
            get { return nickname; }
            set { SetProperty(ref nickname, value); }
        }

        private int gender;
        public int Gender
        {
            get { return gender; }
            set { SetProperty(ref gender, value); }
        }

        private string dateofbirth;
        public string DateOfBirth
        {
            get { return dateofbirth; }
            set
            {
                if (this.dateofbirth == value) return;
                dateofbirth = value;
                this.OnPropertyChanged();
            }
        }

        private DateTime DOB;
        public DateTime DATE_OF_BIRTH
        {
            get { return DOB; }
            set
            {
                if (this.DOB == value) return;
                DOB = value;
                this.OnPropertyChanged();
            }
        }

        private bool isdateofbirthapproximated;
        public bool IsDateOfBirthApproximated
        {
            get { return isdateofbirthapproximated; }
            set { SetProperty(ref isdateofbirthapproximated, value); }
        }

        private int disabilitystatusid;
        public int DisabilityStatusId
        {
            get { return disabilitystatusid; }
            set { SetProperty(ref disabilitystatusid, value); }
        }

        private int numberofbrothers;
        public int NumberOfBrothers
        {
            get { return numberofbrothers; }
            set { SetProperty(ref numberofbrothers, value); }
        }

        private int numberofsisters;
        public int NumberOfSisters
        {
            get { return numberofsisters; }
            set { SetProperty(ref numberofsisters, value); }
        }

        private int favouritelearningid;
        public int FavouriteLearningId
        {
            get { return favouritelearningid; }
            set { SetProperty(ref favouritelearningid, value); }
        }

        private int gradelevelid;
        public int GradeLevelId
        {
            get { return gradelevelid; }
            set { SetProperty(ref gradelevelid, value); }
        }

        private int healthstatusid;
        public int HealthStatusId
        {
            get { return healthstatusid; }
            set { SetProperty(ref healthstatusid, value); }
        }

        private int personalityid;
        public int PersonalityId
        {
            get { return personalityid; }
            set { SetProperty(ref personalityid, value); }
        }

        private int liveswithid;
        public int LivesWithId
        {
            get { return liveswithid; }
            set { SetProperty(ref liveswithid, value); }
        }

        private int locationid;
        public int LocationId
        {
            get { return locationid; }
            set { SetProperty(ref locationid, value); }
        }

        private int registrationplanid;
        public int RegistrationPlanId
        {
            get { return registrationplanid; }
            set { SetProperty(ref registrationplanid, value); }
        }

        private string dateofregistration;
        public string DateOfRegistration
        {
            get { return dateofregistration; }
            set
            {
                SetProperty(ref dateofregistration, value);
                this.OnPropertyChanged();
            }
        }

        private string dateofenrolment;
        public string DateOfEnrolment
        {
            get { return dateofenrolment; }
            set
            {
                SetProperty(ref dateofenrolment, value);
                this.OnPropertyChanged();
            }
        }

        private DateTime REGDATE;
        public DateTime DATE_OF_REGISTRATION
        {
            get { return REGDATE; }
            set
            {
                if (this.REGDATE == value) return;
                REGDATE = value;
                var dob = DateTime.Parse(REGDATE.ToString());
                this.OnPropertyChanged();
            }
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string numberofdocuments;
        public string NumberOfDocuments
        {
            get { return numberofdocuments; }
            set { SetProperty(ref numberofdocuments, value); }
        }

        private string profileImage;
        public string ProfileImage
        {
            get { return profileImage; }
            set { SetProperty(ref profileImage, value); }
        }

        private string fulldocumenttag;
        public string FullDocumentTag
        {
            get { return fulldocumenttag; }
            set { SetProperty(ref fulldocumenttag, value); }
        }

        private string pendingdocuments;
        public string PendingDocuments
        {
            get { return pendingdocuments; }
            set { SetProperty(ref pendingdocuments, value); }
        }

        private string attacheddocuments;
        public string AttachedDocuments
        {
            get { return attacheddocuments; }
            set { SetProperty(ref attacheddocuments, value); }
        }

        private string locationtag;
        public string LocationTag
        {
            get { return locationtag; }
            set { SetProperty(ref locationtag, value); }
        }

        private string documenttag;
        public string DocumentTag
        {
            get { return documenttag; }
            set { SetProperty(ref documenttag, value); }
        }

        private string uploadedtag;
        public string UploadedTag
        {
            get { return uploadedtag; }
            set { SetProperty(ref uploadedtag, value); }
        }
        #endregion

        #region Methods

        #region Registration Management

        // Load New Registration Page
        private ICommand updateRegistrationAsyncCommand;
        public ICommand UpdateRegistrationAsyncCommand => updateRegistrationAsyncCommand ?? (updateRegistrationAsyncCommand = new Command(async () => await ExecuteUpdateRegistrationAsync()));
        private async Task ExecuteUpdateRegistrationAsync()
        {
            if (IsBusy)
            {
                return;
            }

            try
            {
                if (IsBusy)
                {
                    return;
                }


                IsBusy = true; 
                Message = "Updating ... ";
                ErrorMessage = string.Empty; 

                #region Enrolment Info
                var model = this.RegisteredChild;
                model.FirstName = FirstName;
                model.MiddleName = MiddleName;
                model.LastName = LastName;
                model.Nickname = NickName;
                model.GenderId = SelectedGender != null ? SelectedGender.Id : 0;
                model.DateOfBirth = DateFormatter.ToSQLiteDateTimeString(DATE_OF_BIRTH);
                model.IsDateOfBirthApproximated = IsDateOfBirthApproximated;
                model.GradeLevelId = SelectedSchoolGrade != null ? SelectedSchoolGrade.Id : 0;
                // model.HealthStatusId = SelectedHealthStatus.Id;
                model.LivesWithId = SelectedLivesWith != null ? SelectedLivesWith.Id : 0;
                model.FavouriteLearningId = SelectedFavouriteLearning != null ? SelectedFavouriteLearning.Id : 0;
                model.PersonalityId = SelectedPersonality != null ? SelectedPersonality.Id : 0;
                model.NumberOfBrothers = NumberOfBrothers;
                model.NumberOfSisters = NumberOfSisters;
                model.DisabilityStatusId = SelectedDisability != null ? SelectedDisability.Id : 0;
                model.LocationId = SelectedLocation != null ? SelectedLocation.Id : 0; 
                model.DateOfRegistration = DateFormatter.ToSQLiteDateTimeString(DATE_OF_REGISTRATION);
                model.SyncStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "SYNCREADY")).Id;


                var selectedchores = GetSelectedChores();
                if (selectedchores.Count == 0)
                    ErrorMessage += $"Chores is required \n";

                var selectedfavactivities = GetSelectedFavouriteActivities();
                if (selectedfavactivities.Count == 0)
                    ErrorMessage += $"Favourite Activities is required \n";

                var validationResult = _validator.Validate(model);
                if (validationResult.IsValid && ErrorMessage.Length == 0)
                {
                    #region Enrolment Info
                    App.Database.AddOrUpdate(model);
                    #endregion

                    #region Chores Info
                    if (selectedchores.Any())
                    {
                        App.Database.DeleteChoreByIds($"ChildrenChores", $"{model.Id}");

                        foreach (var item in selectedchores)
                        {
                            var childchore = new ChildrenChores
                            {
                                ChildId = model.Id,
                                ChoreId = item.Id,
                                RecordStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "RECACTIVE")).Id,
                                SyncStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "SYNCINSERTED")).Id,
                                IsActive = true
                            };

                            App.Database.Create(childchore);
                        }
                    }
                    #endregion

                    #region Favourite Activities Info
                    if (selectedfavactivities.Any())
                    {
                        foreach (var item in selectedfavactivities)
                        {
                            App.Database.DeleteFavouriteActivitiesByIds($"ChildrenFavouriteActivities", $"{model.Id}");

                            var childfavactivity = new ChildrenFavouriteActivities
                            {
                                ChildId = model.Id,
                                FavouriteActivityId = item.Id,
                                RecordStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "RECACTIVE")).Id,
                                SyncStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "SYNCINSERTED")).Id,
                                IsActive = true
                            };

                            App.Database.Create(childfavactivity);
                        }
                    }
                    #endregion

                    
                    await Task.Delay(TimeSpan.FromSeconds(2));
                    await Navigation.PopAsync();
                    await Navigation.PushAsync(new RegistrationListingPage());
                }
                else
                {
                    ValidateMessage = GetErrorListFromValidationResult(validationResult).Replace(" id'", "");

                    if (ErrorMessage.Length > 0 || ValidateMessage.Length > 0)
                    {
                        ValidateMessage = $"{ValidateMessage}\n{ErrorMessage}";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert
                            {
                                Title = "Validation Errors",
                                Message = ValidateMessage,
                                Cancel = "OK"
                            });
                        IsBusy = false;

                    }

                }
                #endregion
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Error",
                        Message = $"Details\n{e.Message}",
                        Cancel = "OK"
                    });
                return;
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
                await Task.FromResult(0);
            }
        }
        #endregion

        #region Documents Management
        // Load Documents Management Page
        private ICommand newRegistrationDocumentAsyncCommand;
        public ICommand NewRegistrationDocumentAsyncCommand => newRegistrationDocumentAsyncCommand ?? (newRegistrationDocumentAsyncCommand = new Command(async () => await ExecuteRegistrationDocumentAsync()));
        private async Task ExecuteRegistrationDocumentAsync()
        {
            if (IsBusy)
            {
                return;
            }

            try
            {
                await Navigation.PushAsync(new DocumentPage(_childId));
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Error",
                        Message = $"Details\n{e.Message}",
                        Cancel = "OK"
                    });
                return;
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        private ICommand documentListingAsyncCommand;
        public ICommand DocumentListingAsyncCommand => documentListingAsyncCommand ?? (documentListingAsyncCommand = new Command(async () => await ExecuteDocumentListingAsync()));
        private async Task ExecuteDocumentListingAsync()
        {
            if (IsBusy)
            {
                return;
            }

            IsBusy = true;
            Message = "Processing .. ";

            try
            {
                await Task.Delay(TimeSpan.FromSeconds(1));
                await Navigation.PushAsync(new DocumentListingPage(_childId));
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Error",
                        Message = $"Details\n{e.Message}",
                        Cancel = "OK"
                    });
                return;
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
                await Task.FromResult(0);
            }
        }
        #endregion

        // Child Handler
        private Children selectedChildrenCommand;
        public Children SelectedChildrenCommand
        {
            get { return selectedChildrenCommand; }
            set
            {
                selectedChildrenCommand = value;
                OnPropertyChanged();
                if (selectedChildrenCommand == null)
                    return;
                Navigation.PushAsync(new RegistrationEditPage(selectedChildrenCommand.Id));

                selectedChildrenCommand = null;
            }
        }

        private ChildrenDocuments selectedChildrenDocumentCommand;
        public ChildrenDocuments SelectedChildrenDocumentCommand
        {
            get { return selectedChildrenDocumentCommand; }
            set
            {
                selectedChildrenDocumentCommand = value;
                OnPropertyChanged();
                if (selectedChildrenDocumentCommand == null)
                    return;

                IsBusy = true;
                Message = $"Loading . . .";
                Task.Delay(TimeSpan.FromSeconds(1));
                Navigation.PushAsync(new DocumentDetailPage(selectedChildrenDocumentCommand.Id, null));

                selectedChildrenDocumentCommand = null;
                IsBusy = false;
            }
        }

        // Sex Handler
        private SystemCodeDetails _selectedSex;
        public SystemCodeDetails SelectedGender
        {
            get => _selectedSex;
            set
            {
                if (this._selectedSex == value)
                {
                    return;
                }

                this._selectedSex = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> SexOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedSexOptions
        {
            get => SexOptions;
            set => SetProperty(ref SexOptions, value);
        }

        // Health Status Handler
        private SystemCodeDetails _selectedHealthStatus;
        public SystemCodeDetails SelectedHealthStatus
        {
            get => _selectedHealthStatus;
            set
            {
                if (this._selectedHealthStatus == value)
                {
                    return;
                }

                this._selectedHealthStatus = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> HealthStatusOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedHealthStatusOptions
        {
            get => HealthStatusOptions;
            set => SetProperty(ref HealthStatusOptions, value);
        }

        // Disability Handler
        private SystemCodeDetails _selectedDisability;
        public SystemCodeDetails SelectedDisability
        {
            get => _selectedDisability;
            set
            {
                if (this._selectedDisability == value)
                {
                    return;
                }

                this._selectedDisability = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> DisabilityOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedDisabilityOptions
        {
            get => DisabilityOptions;
            set => SetProperty(ref DisabilityOptions, value);
        }

        // FavouriteLearning Handler
        private SystemCodeDetails _selectedFavouriteLearning;
        public SystemCodeDetails SelectedFavouriteLearning
        {
            get => _selectedFavouriteLearning;
            set
            {
                if (this._selectedFavouriteLearning == value)
                {
                    return;
                }

                this._selectedFavouriteLearning = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> FavouriteLearningOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedFavouriteLearningOptions
        {
            get => FavouriteLearningOptions;
            set => SetProperty(ref FavouriteLearningOptions, value);
        }

        // SchoolGrade Handler
        private SystemCodeDetails _selectedSchoolGrade;
        public SystemCodeDetails SelectedSchoolGrade
        {
            get => _selectedSchoolGrade;
            set
            {
                if (this._selectedSchoolGrade == value)
                {
                    return;
                }

                this._selectedSchoolGrade = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> SchoolGradeOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedSchoolGradeOptions
        {
            get => SchoolGradeOptions;
            set => SetProperty(ref SchoolGradeOptions, value);
        }


        // Personality Handler
        private SystemCodeDetails _selectedPersonality;
        public SystemCodeDetails SelectedPersonality
        {
            get => _selectedPersonality;
            set
            {
                if (this._selectedPersonality == value)
                {
                    return;
                }

                this._selectedPersonality = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> PersonalityOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedPersonalityOptions
        {
            get => PersonalityOptions;
            set => SetProperty(ref PersonalityOptions, value);
        }

        // LivesWith Handler
        private SystemCodeDetails _selectedLivesWith;
        public SystemCodeDetails SelectedLivesWith
        {
            get => _selectedLivesWith;
            set
            {
                if (this._selectedLivesWith == value)
                {
                    return;
                }

                this._selectedLivesWith = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> LivesWithOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedLivesWithOptions
        {
            get => LivesWithOptions;
            set => SetProperty(ref LivesWithOptions, value);
        }

        // Chores Handler
        private SystemCodeDetails _selectedChore;
        public SystemCodeDetails SelectedChore
        {
            get => _selectedChore;
            set
            {
                if (this._selectedChore == value)
                {
                    return;
                }

                this._selectedChore = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> ChoreOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedChoreOptions
        {
            get { return ChoreOptions; }
            set => SetProperty(ref ChoreOptions, value);
        }
        public ObservableRangeCollection<SelectableItemWrapper<SystemCodeDetails>> _loadedChores = new ObservableRangeCollection<SelectableItemWrapper<SystemCodeDetails>>();
        public ObservableRangeCollection<SelectableItemWrapper<SystemCodeDetails>> LoadedChores
        {
            get { return _loadedChores; }
            set => SetProperty(ref _loadedChores, value);
        }
        ObservableCollection<SystemCodeDetails> GetSelectedChores()
        {
            var selected = LoadedChores
                .Where(p => p.IsSelected)
                .Select(p => p.Item)
                .ToList();
            return new ObservableCollection<SystemCodeDetails>(selected);
        }

        // FavouriteActivity Handler
        private SystemCodeDetails _selectedFavouriteActivity;
        public SystemCodeDetails SelectedFavouriteActivity
        {
            get => _selectedFavouriteActivity;
            set
            {
                if (this._selectedFavouriteActivity == value)
                {
                    return;
                }

                this._selectedFavouriteActivity = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> FavouriteActivityOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SelectableItemWrapper<SystemCodeDetails>> _loadedFavouriteActivities = new ObservableRangeCollection<SelectableItemWrapper<SystemCodeDetails>>();
        public ObservableRangeCollection<SelectableItemWrapper<SystemCodeDetails>> LoadedFavouriteActivities
        {
            get { return _loadedFavouriteActivities; }
            set => SetProperty(ref _loadedFavouriteActivities, value);
        }
        ObservableCollection<SystemCodeDetails> GetSelectedFavouriteActivities()
        {
            var selected = LoadedFavouriteActivities
                .Where(p => p.IsSelected)
                .Select(p => p.Item)
                .ToList();
            return new ObservableCollection<SystemCodeDetails>(selected);
        }

        // Location Handler
        private Locations _selectedLocation;
        public Locations SelectedLocation
        {
            get => _selectedLocation;
            set
            {
                if (this._selectedLocation == value)
                {
                    return;
                }

                this._selectedLocation = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<Locations> LocationOptions = new ObservableRangeCollection<Locations>();
        public ObservableRangeCollection<Locations> LoadedLocationOptions
        {
            get => LocationOptions;
            set => SetProperty(ref LocationOptions, value);
        }

        // Registration Plan Handler
        private SystemCodeDetails _selectedRegistrationPlan;
        public SystemCodeDetails SelectedRegistrationPlan
        {
            get => _selectedRegistrationPlan;
            set
            {
                if (this._selectedRegistrationPlan == value)
                {
                    return;
                }

                this._selectedRegistrationPlan = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> RegistrationPlanOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedRegistrationPlanOptions
        {
            get => RegistrationPlanOptions;
            set => SetProperty(ref RegistrationPlanOptions, value);
        }

        // Children Handler
        public ObservableRangeCollection<Children> RegisteredChildren { get; } = new ObservableRangeCollection<Children>();
        public ObservableCollection<Children> GetRegisteredChildren()
        {
            try
            {
                var items = App.Database.GetTableRows("Children");
                var registeredChildren = new ObservableCollection<Children>();
                foreach (var item in items)
                {
                    var regChild = (Children)item;
                    registeredChildren.Add(regChild);
                }
                return registeredChildren;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }

            return null;
        }

        // Children Documents Handler
        public ObservableRangeCollection<ChildrenDocuments> RegisteredChildrenDocuments { get; } = new ObservableRangeCollection<ChildrenDocuments>();
        public ObservableCollection<ChildrenDocuments> GetRegisteredChildrenDocuments()
        {
            try
            {
                var items = App.Database.GetTableRows("ChildrenDocuments", "ChildId", _childId.ToString());
                var registeredChildrenDocuments = new ObservableCollection<ChildrenDocuments>();
                foreach (var item in items)
                {
                    var regChildDoc = (ChildrenDocuments)item;
                    registeredChildrenDocuments.Add(regChildDoc);
                }
                return registeredChildrenDocuments;

            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Error",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }

            return null;
        }
        #endregion
    }
    #endregion
}