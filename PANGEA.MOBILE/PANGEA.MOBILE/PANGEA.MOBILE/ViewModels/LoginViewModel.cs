﻿using FormsToolkit;
using MvvmHelpers;
using PANGEA.MOBILE.Database;
using PANGEA.MOBILE.Helpers;
using PANGEA.MOBILE.Interface;
using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.Services;
using PANGEA.MOBILE.Views;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;


namespace PANGEA.MOBILE.ViewModels
{
    public class LoginViewModel : LocalBaseViewModel
    {
        public static App current;
        private IAppClient _appClient;
        private INavigation _navigation;

        public LoginViewModel(INavigation navigation) : base(navigation)
        {
            _appClient = DependencyService.Get<IAppClient>();
            _navigation = navigation;
        }

        #region Declarations
        private string userid;
        public string UserId
        {
            get { return userid; }
            set { SetProperty(ref userid, value); }
        }

        private string username;
        public string UserName
        {
            get { return username; }
            set { SetProperty(ref username, value); }
        }

        private string userlocationid;
        public string UserLocationId
        {
            get { return userlocationid; }
            set { SetProperty(ref userlocationid, value); }
        }
        #endregion

        #region Methods
        private ICommand loginCommand;
        public ICommand LoginCommand => loginCommand ?? (loginCommand = new Command(async () => await ExecuteLoginAsync()));
        private async Task ExecuteLoginAsync()
        {
            if (IsBusy)
            {
                return;
            }
            IsBusy = true;
            Message = "Logging in, Please Wait . . .";

            Settings.Current.FirstRun = false;
            Settings.Current.UserName = UserName;
            IsLoggedIn = true;

            await Task.Delay(TimeSpan.FromSeconds(2));
            await Finish();
            IsBusy = false;
        }


        private ICommand logoutCommand;
        public ICommand LogOutCommand => logoutCommand ?? (logoutCommand = new Command(async () => await ExecuteLogoutAsync()));
        private async Task ExecuteLogoutAsync()
        {
            if (IsBusy)
                return;
            try
            {
                IsBusy = true;
                Message = "Logging Out, Please Wait . . .";

                Settings.Current.FirstRun = true;
                IsLoggedIn = false;

                await Task.Delay(TimeSpan.FromSeconds(2));
                await Navigation.PopToRootAsync();
                await Navigation.PushModalAsync(new LoginPage());
            }
            catch (Exception ex)
            {


                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Log Out",
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;
        }

        private async Task Finish()
        {
            if (Device.RuntimePlatform == Device.iOS && Settings.Current.FirstRun)
            {
                var push = DependencyService.Get<IPushNotifications>();
                if (push != null)
                    await push.RegisterForNotifications();

                await Navigation.PopModalAsync();
            }
            else
            {
                await Navigation.PopModalAsync();
            }
        } 
        #endregion
    }
}
