﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using FluentValidation.Results;
using MvvmHelpers;
using PANGEA.MOBILE.Helpers;
using PANGEA.MOBILE.Interface;
using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.Services;
using PANGEA.MOBILE.Views;
using Plugin.DeviceInfo;
using Xamarin.Forms;

namespace PANGEA.MOBILE.ViewModels
{
    public class LocalBaseViewModel : BaseViewModel
    {
        protected INavigation Navigation { get; }
        protected IToast Toast { get; } = DependencyService.Get<IToast>();

        private string _appVersion;
        public string AppVersion
        {
            get
            {
                return _appVersion;
            }
            set
            {
                SetProperty(ref this._appVersion, value, "AppVersion");
            }
        }

        private string _appName;
        public string AppName
        {
            get
            {
                return _appName;
            }
            set
            {
                SetProperty(ref this._appName, value, "AppName");
            }
        }

        private string _displayName;
        public string DisplayName
        {
            get
            {
                return _displayName;
            }
            set
            {
                SetProperty(ref this._displayName, value, "DisplayName");
            }
        }

        public static void Init(bool mock = true)
        {
            DependencyService.Register<IAppClient, AppClient>();
        }

        public LocalBaseViewModel(INavigation navigation = null)
        {
            Navigation = navigation;

            AppVersion = $"Software Build {CrossDeviceInfo.Current.AppBuild} - Version. {CrossDeviceInfo.Current.AppVersion} ";

            AppName = "PANGEA MOBILE DATA TOOL";

            DisplayName = $"{Settings.Current.UserName} ";

            UserName = $"{Settings.Current.UserName} ";


            #region Load Settings
            var SystemCodeDetail = App.Database.SystemCodeDetailGetByCode($"DefaultDeleteOnSync");
            string deleteonsync = $"DELETE ON UPLOAD";
            DeleteOnSyncStatus = SystemCodeDetail.Count > 0 ? SystemCodeDetail[0].SystemDetailCode : string.Empty;
            DeleteOnSync = (!string.IsNullOrEmpty(DeleteOnSyncStatus) & DeleteOnSyncStatus.Equals($"{deleteonsync}")) ? true : false;
            IsNotDataReady = SystemCodeDetail.Count > 0 ? false : true;
            IsDataReady = SystemCodeDetail.Count > 0 ? true : false;
            #endregion

        }
        protected string GetErrorListFromValidationResult(ValidationResult validationResult)
        {
            var errorList = validationResult.Errors.Select(x => x.ErrorMessage).ToList();
            return String.Join("\n", errorList.ToArray()); ;
        }


        #region Declarations
        public string username = string.Empty;
        public string UserName
        {
            get { return username; }
            set { SetProperty(ref username, value); }

        }

        private bool isLoggedIn;
        public bool IsLoggedIn
        {
            get { return isLoggedIn; }
            set { SetProperty(ref isLoggedIn, value); }
        }

        private bool isNotDataReady;
        public bool IsNotDataReady
        {
            get { return isNotDataReady; }
            set { SetProperty(ref isNotDataReady, value); }
        }

        private bool isDataReady;
        public bool IsDataReady
        {
            get { return isDataReady; }
            set { SetProperty(ref isDataReady, value); }
        }

        public string filter = string.Empty;
        public string Filter
        {
            get { return filter; }
            set { SetProperty(ref filter, value); }

        }


        public string message = string.Empty;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string errormessage;
        public string ErrorMessage
        {
            get { return errormessage; }
            set { SetProperty(ref errormessage, value); }
        }

        private string validateMessage = string.Empty;
        public string ValidateMessage
        {
            get
            {
                return validateMessage;
            }
            set
            {
                SetProperty(ref this.validateMessage, value, "ValidateMessage");
            }
        }

        private bool deleteOnSync;
        public bool DeleteOnSync
        {
            get { return deleteOnSync; }
            set { SetProperty(ref deleteOnSync, value); }
        }

        private string deleteOnSyncStatus;
        public string DeleteOnSyncStatus
        {
            get { return deleteOnSyncStatus; }
            set { SetProperty(ref deleteOnSyncStatus, value); }
        }


        // Cancel Registration
        private ICommand cancelRegistrationCommand;
        public ICommand CancelRegistrationCommand => cancelRegistrationCommand ?? (cancelRegistrationCommand = new Command(async () => await ExecuteCancelRegistrationAsync()));
        private async Task ExecuteCancelRegistrationAsync()
        {
            if (IsBusy)
            {
                return;
            }
            await Navigation.PopToRootAsync();
            await Navigation.PushAsync(new RegistrationListingPage());
        }
        #endregion

    }
}
