﻿using FluentValidation;
using FormsToolkit;
using MvvmHelpers;
using PANGEA.MOBILE.Database;
using PANGEA.MOBILE.Helpers;
using PANGEA.MOBILE.Interface;
using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.Views;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PANGEA.MOBILE.ViewModels
{
    public class RegistrationDocumentViewModel : LocalBaseViewModel
    {
        private IAppClient _appClient;
        private INavigation _navigation;
        private readonly IValidator _validator;
        private SystemCodeDetails SystemCodeDetails;
        private int? _childId;

        public RegistrationDocumentViewModel(INavigation navigation, int? childid) : base(navigation)
        {
            _appClient = DependencyService.Get<IAppClient>();
            _navigation = navigation;
            _validator = new RegistrationChildrenDocumentValidator();
            _childId = childid;

            LoadedDocumentTypeOptions.AddRange(App.Database.SystemCodeDetailGetByCode("Documents To Attach"));

            if (!string.IsNullOrEmpty(_childId.ToString()))
            {
                if (Settings.Current.Module == $"Registrations")
                {
                    var registrationData = App.Database.GetTableRow("Children", "Id", _childId.ToString());
                    Registration = (Children)registrationData;
                    FullName = $"{Registration.FirstName} {Registration.MiddleName} {Registration.LastName}";
                    ChildStatusId = (int)Registration.ChildStatusId;
                    EnrolStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "REGNEW")).Id;
                    UpdateStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "REGUPDATE")).Id;
                    SyncReadyStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "SYNCREADY")).Id;
                }

                if (Settings.Current.Module == $"Updates")
                {
                    var registrationData = App.Database.GetTableRow("ChildrenUpdates", "Id", _childId.ToString());
                    Updates = (ChildrenUpdates)registrationData;
                    FullName = $"{Updates.FirstName} {Updates.MiddleName} {Updates.LastName}";
                    ChildStatusId = (int)Updates.ChildStatusId;
                    EnrolStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "REGNEW")).Id;
                    UpdateStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "REGUPDATE")).Id;
                    SyncReadyStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "SYNCREADY")).Id;
                }
            }

            #region LoadDefaultDocument
            RegistrationDocument = new ChildrenDocuments();
            UpdatesDocument = new ChildrenUpdatesDocuments();
            #endregion
        }

        #region Declarations
        public Children Registration { get; set; }

        public ChildrenUpdates Updates { get; set; }

        public ChildrenDocuments RegistrationDocument { get; set; }

        public ChildrenUpdatesDocuments UpdatesDocument { get; set; }

        private int childid;
        public int ChildId
        {
            get { return childid; }
            set { SetProperty(ref childid, value); }
        }

        private int childstatusid;
        public int ChildStatusId
        {
            get { return childstatusid; }
            set { SetProperty(ref childstatusid, value); }
        }

        private int enrolstatusid;
        public int EnrolStatusId
        {
            get { return enrolstatusid; }
            set { SetProperty(ref enrolstatusid, value); }
        }

        private int updatestatusid;
        public int UpdateStatusId
        {
            get { return updatestatusid; }
            set { SetProperty(ref updatestatusid, value); }
        }

        private int syncreadystatusid;
        public int SyncReadyStatusId
        {
            get { return syncreadystatusid; }
            set { SetProperty(ref syncreadystatusid, value); }
        }


        private string fullname;
        public string FullName
        {
            get { return fullname; }
            set { SetProperty(ref fullname, value); }
        }

        private int documentid;
        public int DocumentId
        {
            get { return documentid; }
            set { SetProperty(ref documentid, value); }
        }

        private int documenttypeid;
        public int DocumentTypeId
        {
            get { return documenttypeid; }
            set { SetProperty(ref documenttypeid, value); }
        }

        private byte[] documentimage;
        public byte[] DocumentImage
        {
            get { return documentimage; }
            set { SetProperty(ref documentimage, value); }
        }

        private string documentpath;
        public string DocumentPath
        {
            get { return documentpath; }
            set { SetProperty(ref documentpath, value); }
        }

        private string documentname;
        public string DocumentName
        {
            get { return documentname; }
            set { SetProperty(ref documentname, value); }
        }

        private bool isDocumentLoaded = false;
        public bool IsDocumentLoaded
        {
            get { return isDocumentLoaded; }
            set { SetProperty(ref isDocumentLoaded, value); }
        }
        #endregion

        #region Methods
        // DocumentType Handler
        private SystemCodeDetails _selectedDocumentType;
        public SystemCodeDetails SelectedDocumentType
        {
            get => _selectedDocumentType;
            set
            {
                if (this._selectedDocumentType == value)
                {
                    return;
                }

                this._selectedDocumentType = value;

                this.OnPropertyChanged();
            }
        }
        public ObservableRangeCollection<SystemCodeDetails> DocumentTypeOptions = new ObservableRangeCollection<SystemCodeDetails>();
        public ObservableRangeCollection<SystemCodeDetails> LoadedDocumentTypeOptions
        {
            get => DocumentTypeOptions;
            set => SetProperty(ref DocumentTypeOptions, value);
        }

        // Save New Photo Page
        private ICommand saveDocumentAsyncCommand;
        public ICommand SaveDocumentAsyncCommand => saveDocumentAsyncCommand ?? (saveDocumentAsyncCommand = new Command(async () => await ExecuteSaveDocumentAsyncCommand()));
        private async Task ExecuteSaveDocumentAsyncCommand()
        {
            if (IsBusy)
            {
                return;
            }

            try
            {
                IsBusy = true;
                ErrorMessage = string.Empty;

                var model = this.RegistrationDocument;
                model.ChildId = Registration.Id;
                model.DocumentTypeId = SelectedDocumentType != null ? SelectedDocumentType.Id : 0;
                model.FilePath = DocumentPath;
                model.FileName = SelectedDocumentType != null ? $"{SelectedDocumentType.SystemDetailDescription}_{Registration.Id}" : string.Empty;
                model.FileBinary = DocumentImage;
                model.IsDocumentRequired = true;
                model.IsTranslationRequired = false;
                model.DocumentStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "DOCQCREADY")).Id;
                model.RecordStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "RECACTIVE")).Id;
                model.SyncStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "SYNCINSERTED")).Id;
                model.IsActive = true;

                var validationResult = _validator.Validate(model);
                if (validationResult.IsValid && ErrorMessage.Length == 0)
                {
                    App.Database.DeleteDocumentByIds($"ChildrenDocuments", $"{model.ChildId}", $"{model.DocumentTypeId}");

                    model.SyncStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "SYNCREADY")).Id;
                    App.Database.Create(model);

                    if(ChildStatusId == EnrolStatusId)
                    {
                        var childdocuments = App.Database.GetTableRows("ChildrenDocuments", "ChildId", Registration.Id.ToString());
                        if (childdocuments.Count == 5)
                        {
                            var regmodel = this.Registration;
                            regmodel.SyncStatusId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "SYNCREADY")).Id;
                            App.Database.AddOrUpdate(regmodel);
                        }
                    }

                    Message = "Processing .. ";
                    IsDocumentLoaded = true;
                    await Task.Delay(TimeSpan.FromSeconds(2));
                    await Navigation.PopAsync();

                    if(Settings.Current.Module == $"Registrations")
                        await Navigation.PushAsync(new RegistrationEditPage(Registration.Id));

                    if (Settings.Current.Module == $"Updates")
                        await Navigation.PushAsync(new UpdatePage(Registration.Id));
                }
                else
                {
                    ValidateMessage = GetErrorListFromValidationResult(validationResult).Replace(" id'", "");

                    if (ErrorMessage.Length > 0 || ValidateMessage.Length > 0)
                    {
                        ValidateMessage = $"{ValidateMessage}\n{ErrorMessage}";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert
                            {
                                Title = "Validation Errors",
                                Message = ValidateMessage,
                                Cancel = "OK"
                            });
                        IsBusy = false;

                    }

                }
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Error",
                        Message = $"Details\n{e.Message}",
                        Cancel = "OK"
                    });
                return;
            }
            finally
            {
                IsBusy = false;
                await Task.FromResult(0);
            }
        } 
        #endregion
    }
}
