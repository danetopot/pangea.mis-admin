﻿using System;
using System.Collections.Generic;
using System.Text;
using PANGEA.MOBILE.Models;

namespace PANGEA.MOBILE.ViewModels.API
{
    public class ApiRequestViewModel
    {
        public string Email { get; set; }
    }

    public class ApiResponseViewModel
    {
        public string Data { get; set; }
    }

    public class ApiStatusViewModel
    {
        public int? Id { get; set; }
        public string Code { get; set; }
        public int? StatusId { get; set; }
        public string Description { get; set; }
    }

    public class ApiSettupViewModel
    {
        public int? Code { get; set; }
        public string Message { get; set; }
        public List<RegistrationPlans> RegistrationPlanListings { get; set; }
        public List<UpdatePlans> UpdatePlanListings { get; set; }
        public List<ChildrenUpdates> UpdateChildrenListings { get; set; }
        public List<ChildrenUpdatesDocuments> UpdateChildrenDocumentListings { get; set; }
        public List<ChildrenChores> UpdateChildrenChoresListings { get; set; }
        public List<ChildrenFavouriteActivities> UpdateChildrenFavActivitiesListings { get; set; }
        public List<SystemCodeDetails> SystemCodeListings { get; set; }
        public List<Locations> LocationListings { get; set; }
    }

    public class RegistrationPlanListingVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string PlanStatus { get; set; }
    }

    public class UpdatePlanListingVm
    {
        public int Id { get; set; }

        public string ExerciseName { get; set; }

        public DateTime ExerciseStartDate { get; set; }

        public DateTime ExerciseEndDate { get; set; }

        public string RegistrationPlan { get; set; }

        public string PlanStatus { get; set; }
    }

    public class RegisterChildVm
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Surname { get; set; }
        public string Nickname { get; set; }
        public int? GenderId { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public bool IsDateOfBirthApproximated { get; set; }
        public string FullName => $"{FirstName} {MiddleName} {Surname}";
        public int? GradeLevelId { get; set; }
        public int HealthStatusId { get; set; }
        public int? LivesWithId { get; set; }
        public int? FavouriteLearningId { get; set; }
        public int? PersonalityId { get; set; }
        public int? NumberOfBrothers { get; set; }
        public int? NumberOfSisters { get; set; }
        public int? DisabilityStatusId { get; set; }
        public int? LocationId { get; set; }
        public int? RegistrationPlanId { get; set; }
        public DateTime? DateOfRegistration { get; set; }
        public int? ChildStatusId { get; set; }
        public int RecordStatusId { get; set; }
        public int RecordSourceId { get; set; }
        public int SyncStatusId { get; set; }
        public ICollection<ChildrenChores> ChildrenChores { get; set; }
        public ICollection<ChildrenFavouriteActivities> ChildrenFavouriteActivities { get; set; }
        public ICollection<ChildrenDocuments> ChildrenDocuments { get; set; }
        public ICollection<ChildrenLifeEvents> ChildrenLifeEvents { get; set; }
    }

    public class SystemCodeDetailVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsSelected { get; set; }
        public bool IsUserMaintained { get; set; }
    }
    public class LocationVm
    {
        public int Id { get; set; }
        public string Location { get; set; }
        public string Country { get; set; }
    }

    public enum MenuItemType
    {
        MainPage,
        HomePage, 
        PlanPage,
        RegistrationPage,
        EnrolmentPage,
        UpdatePage,
        SyncPage,
        SettingsPage,
        LoginPage,
        LogoutPage
    }

    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }
    }

    public class PlanItem
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Status { get; set; }
    }

    public class ChoreItem
    {
        public int Id { get; set; }

        public string Chore { get; set; }
        public bool IsSelected { get; set; }
    }
}
