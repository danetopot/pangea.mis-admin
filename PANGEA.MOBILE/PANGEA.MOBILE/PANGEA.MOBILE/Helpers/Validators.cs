﻿using FluentValidation;
using PANGEA.MOBILE.Models;

namespace PANGEA.MOBILE.Helpers
{

    public class RegistrationChildrenValidator : AbstractValidator<Children>
    {
        public RegistrationChildrenValidator()
        {
            RuleFor(x => x.FirstName).NotEmpty().WithMessage("FirstName is required");
            // RuleFor(x => x.MiddleName).NotEmpty().WithMessage("MiddleName is required");
            RuleFor(x => x.LastName).NotEmpty().WithMessage("LastName is required");
            // RuleFor(x => x.Nickname).NotEmpty().WithMessage("Nickname is required");
            RuleFor(x => x.GenderId).NotNull().GreaterThan(0).WithMessage("Gender is required");
            RuleFor(x => x.DateOfBirth).NotEmpty().WithMessage("DateOfBirth is required");
            // RuleFor(x => x.IsDateOfBirthApproximated).NotEmpty().WithMessage("IsDateOfBirthApproximated is required");
            RuleFor(x => x.GradeLevelId).NotNull().GreaterThan(0).WithMessage("GradeLevel is required");
            // RuleFor(x => x.HealthStatusId).NotNull().GreaterThan(0).WithMessage("HealthStatus is required");
            RuleFor(x => x.LivesWithId).NotNull().GreaterThan(0).WithMessage("LivesWith is required");
            RuleFor(x => x.FavouriteLearningId).NotNull().GreaterThan(0).WithMessage("FavouriteLearning is required");
            RuleFor(x => x.PersonalityId).NotNull().GreaterThan(0).WithMessage("Personality is required");
            RuleFor(x => x.NumberOfSisters).GreaterThanOrEqualTo(0).WithMessage("NumberOfSisters is required");
            RuleFor(x => x.NumberOfBrothers).GreaterThanOrEqualTo(0).WithMessage("NumberOfBrothers is required");
            RuleFor(x => x.DisabilityStatusId).NotNull().GreaterThan(0).WithMessage("DisabilityStatus is required");
            RuleFor(x => x.LocationId).NotNull().GreaterThan(0).WithMessage("Location is required");
            // RuleFor(x => x.RegistrationPlanId).NotNull().GreaterThan(0).WithMessage("RegistrationPlan is required");
            RuleFor(x => x.DateOfRegistration).NotEmpty().WithMessage("DateOfRegistration is required");
            //RuleFor(x => x.ChildrenChores).NotEmpty().WithMessage("Chores is required");
            //RuleFor(x => x.ChildrenFavouriteActivities).NotEmpty().WithMessage("FavouriteActivities is required");
        }
    }

    public class RegistrationChildrenUpdatesValidator : AbstractValidator<ChildrenUpdates>
    {
        public RegistrationChildrenUpdatesValidator()
        {
            RuleFor(x => x.FirstName).NotEmpty().WithMessage("FirstName is required");
            // RuleFor(x => x.MiddleName).NotEmpty().WithMessage("MiddleName is required");
            RuleFor(x => x.LastName).NotEmpty().WithMessage("LastName is required");
            // RuleFor(x => x.Nickname).NotEmpty().WithMessage("Nickname is required");
            RuleFor(x => x.GenderId).NotNull().GreaterThan(0).WithMessage("Gender is required");
            RuleFor(x => x.DateOfBirth).NotEmpty().WithMessage("DateOfBirth is required");
            // RuleFor(x => x.IsDateOfBirthApproximated).NotEmpty().WithMessage("IsDateOfBirthApproximated is required");
            RuleFor(x => x.GradeLevelId).NotNull().GreaterThan(0).WithMessage("GradeLevel is required");
            // RuleFor(x => x.HealthStatusId).NotNull().GreaterThan(0).WithMessage("HealthStatus is required");
            RuleFor(x => x.LivesWithId).NotNull().GreaterThan(0).WithMessage("LivesWith is required");
            RuleFor(x => x.FavouriteLearningId).NotNull().GreaterThan(0).WithMessage("FavouriteLearning is required");
            RuleFor(x => x.PersonalityId).NotNull().GreaterThan(0).WithMessage("Personality is required");
            RuleFor(x => x.NumberOfSisters).GreaterThanOrEqualTo(0).WithMessage("NumberOfSisters is required");
            RuleFor(x => x.NumberOfBrothers).GreaterThanOrEqualTo(0).WithMessage("NumberOfBrothers is required");
            RuleFor(x => x.DisabilityStatusId).NotNull().GreaterThan(0).WithMessage("DisabilityStatus is required");
            RuleFor(x => x.LocationId).NotNull().GreaterThan(0).WithMessage("Location is required");
            // RuleFor(x => x.RegistrationPlanId).NotNull().GreaterThan(0).WithMessage("RegistrationPlan is required");
            RuleFor(x => x.DateOfUpdate).NotEmpty().WithMessage("DateOfUpdate is required");
            //RuleFor(x => x.ChildrenChores).NotEmpty().WithMessage("Chores is required");
            //RuleFor(x => x.ChildrenFavouriteActivities).NotEmpty().WithMessage("FavouriteActivities is required");
            RuleFor(x => x.MajorLifeEvent).NotEmpty().WithMessage("MajorLifeEvent is required");
        }
    }

    public class RegistrationChildrenDocumentValidator : AbstractValidator<ChildrenDocuments>
    {
        public RegistrationChildrenDocumentValidator()
        {
            RuleFor(x => x.DocumentTypeId).NotEmpty().WithMessage("DocumentType is required");
            RuleFor(x => x.FileName).NotEmpty().WithMessage("Upload is required");
            //RuleFor(x => x.FileBinary).NotEmpty().WithMessage("FileBinary is required");
            //RuleFor(x => x.FilePath).NotEmpty().WithMessage("FilePath is required");
        }
    }

    public class UpdateChildrenDocumentValidator : AbstractValidator<ChildrenUpdatesDocuments>
    {
        public UpdateChildrenDocumentValidator()
        {
            RuleFor(x => x.DocumentTypeId).NotEmpty().WithMessage("DocumentType is required");
            RuleFor(x => x.FileName).NotEmpty().WithMessage("Upload is required");
            //RuleFor(x => x.FileBinary).NotEmpty().WithMessage("FileBinary is required");
            //RuleFor(x => x.FilePath).NotEmpty().WithMessage("FilePath is required");
        }
    }

    public class RegistrationChildrenLifeEventValidator : AbstractValidator<ChildrenLifeEvents>
    {
        public RegistrationChildrenLifeEventValidator()
        {
            RuleFor(x => x.LifeEvent).NotEmpty().WithMessage("LifeEvent is required");
        }
    }

    public class SettingsValidator : AbstractValidator<SystemCodeDetails>
    {
        public SettingsValidator()
        {
            RuleFor(x => x.SystemCode).NotEmpty().WithMessage("SystemCode is required");
            RuleFor(x => x.SystemDetailCode).NotEmpty().WithMessage("SystemDetailCode is required");
            RuleFor(x => x.SystemDetailDescription).NotEmpty().WithMessage("SystemDetailDescription is required");
        }
    }
}
