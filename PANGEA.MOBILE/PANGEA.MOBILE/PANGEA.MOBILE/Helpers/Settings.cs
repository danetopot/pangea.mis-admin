﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using MvvmHelpers;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace PANGEA.MOBILE.Helpers
{
    public class Settings : INotifyPropertyChanged
    {
        private static Settings settings;
        public static Settings Current
        {
            get
            {
                return settings ?? (settings = new Settings());
            }
        }

        public static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        private const string ModuleKey = "ModuleKey";
        private readonly string _moduleDefault = "Enrol";
        public string Module
        {
            get
            {
                return AppSettings.GetValueOrDefault(ModuleKey, this._moduleDefault);
            }

            set
            {
                if (AppSettings.AddOrUpdateValue(ModuleKey, value))
                {
                    OnPropertyChanged(nameof(ModuleKey));
                }
            }
        }

        private const string AccessTokenKey = "AccessTokenKey";
        private readonly string tokenDefault = string.Empty;
        public string AccessToken
        {
            get => AppSettings.GetValueOrDefault(AccessTokenKey, this.tokenDefault);

            set
            {
                if (AppSettings.AddOrUpdateValue(AccessTokenKey, value))
                {
                    OnPropertyChanged(string.Empty);
                }
            }
        }

        private const string UserNameKey = "UserNameKey";
        private readonly string _usernameDefault = "Admin";
        public string UserName
        {
            get
            {
                return AppSettings.GetValueOrDefault(UserNameKey, this._usernameDefault);
            }

            set
            {
                if (AppSettings.AddOrUpdateValue(UserNameKey, value))
                {
                    OnPropertyChanged(nameof(UserName));
                }
            }
        }

        private const string DatabaseIdKey = "DatabaseIdKey";
        private static readonly int _databaseIdDefault = 0;
        public static int DatabaseId
        {
            get
            {
                return AppSettings.GetValueOrDefault(DatabaseIdKey, _databaseIdDefault);
            }

            set
            {
                AppSettings.AddOrUpdateValue(DatabaseIdKey, value);
            }
        }


        private const string PositionKey = "PositionKey";
        private readonly string positionDefault = string.Empty;
        public string Position
        {
            get
            {
                return AppSettings.GetValueOrDefault(PositionKey, this.positionDefault);
            }

            set
            {
                if (AppSettings.AddOrUpdateValue(PositionKey, value))
                {
                    OnPropertyChanged(string.Empty);
                }
            }
        }


        private bool _isConnectedDefault = false;
        public bool IsConnected
        {
            get
            {
                return _isConnectedDefault;
            }

            set
            {
                if (_isConnectedDefault == value)
                    return;
                _isConnectedDefault = value;
                OnPropertyChanged(string.Empty);
            }
        }


        private const string FirstRunKey = "FirstRunKey";
        private static readonly bool FirstRunDefault = true;
        public bool FirstRun
        {
            get
            {
                return AppSettings.GetValueOrDefault(FirstRunKey, FirstRunDefault);
            }

            set
            {
                if (AppSettings.AddOrUpdateValue(FirstRunKey, value))
                    OnPropertyChanged(string.Empty);
            }
        }

        private const string DefaultLanguageKey = "DefaultLanguage";
        private static readonly string DefaultLanguageDefault = string.Empty;
        public string DefaultLanguage
        {
            get
            {
                return AppSettings.GetValueOrDefault(DefaultLanguageKey, DefaultLanguageDefault);
            }

            set
            {
                if (AppSettings.AddOrUpdateValue(DefaultLanguageKey, value))
                    OnPropertyChanged(string.Empty);
            }
        }

        private const string DefaultLocationKey = "DefaultLocation";
        private static readonly string DefaultLocationDefault = string.Empty;
        public string DefaultLocation
        {
            get
            {
                return AppSettings.GetValueOrDefault(DefaultLocationKey, DefaultLocationDefault);
            }

            set
            {
                if (AppSettings.AddOrUpdateValue(DefaultLocationKey, value))
                    OnPropertyChanged(string.Empty);
            }
        }

        private const string DefaultDeleteOnSyncKey = "DefaultDeleteOnSync";
        private static readonly string DefaultDeleteOnSyncDefault = string.Empty;
        public string DefaultDeleteOnSync
        {
            get
            {
                return AppSettings.GetValueOrDefault(DefaultDeleteOnSyncKey, DefaultDeleteOnSyncDefault);
            }

            set
            {
                if (AppSettings.AddOrUpdateValue(DefaultDeleteOnSyncKey, value))
                    OnPropertyChanged(string.Empty);
            }
        }

        public static int UpdateDatabaseId()
        {
            return DatabaseId++;
        }

        public void FinishHack(string id)
        {
            AppSettings.AddOrUpdateValue("minihack_" + id, true);
        }

        public bool IsHackFinished(string id)
        {
            return AppSettings.GetValueOrDefault("minihack_" + id, false);
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
