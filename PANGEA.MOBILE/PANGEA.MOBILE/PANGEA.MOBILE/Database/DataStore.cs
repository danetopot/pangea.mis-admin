﻿using PANGEA.MOBILE.Interface;
using PANGEA.MOBILE.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Xamarin.Forms;

namespace PANGEA.MOBILE.Database
{
    public class DataStore
    {
        private readonly SQLiteConnection _database;
        private readonly string nameSpace = "PANGEA.MOBILE.Models.";

        public DataStore()
        {
            _database = DependencyService.Get<ISQLite>().GetConnection();

            this._database.CreateTable<Children>();
            this._database.CreateTable<ChildrenUpdates>();
            this._database.CreateTable<ChildrenChores>();
            this._database.CreateTable<ChildrenFavouriteActivities>();
            this._database.CreateTable<ChildrenDocuments>();
            this._database.CreateTable<ChildrenUpdatesDocuments>();
            this._database.CreateTable<RegistrationPlans>();
            this._database.CreateTable<UpdatePlans>();
            this._database.CreateTable<Locations>();
            this._database.CreateTable<SystemCodeDetails>();
        }

        public virtual void AddOrUpdate<TEntity>(TEntity entity) where TEntity : class
        {
            this._database.InsertOrReplace(entity);
        }

        public virtual void Create<TEntity>(TEntity entity) where TEntity : class
        {
            this._database.Insert(entity);
        }

        public virtual void Delete<TEntity>(TEntity entity) where TEntity : class
        {
            this._database.Delete(entity);
        }

        public virtual void Delete<TEntity>(int primarykey) where TEntity : class
        {
            this._database.Delete<TEntity>(primarykey);
        }

        public virtual void Manage<TEntity>(TEntity entity) where TEntity : class
        {
        }

        public virtual void Update<TEntity>(TEntity entity) where TEntity : class
        {
            this._database.Update(entity);
        }

        #region Abstract Get
        public SystemCodeDetails SystemCodeDetailGetById(int id)
        {
            var data = this._database.Query<SystemCodeDetails>($"SELECT * FROM [SystemCodeDetails] WHERE [Id] = {id}").Single();
            return data;
        }

        public SystemCodeDetails SystemCodeDetailGetByCode(int? overridevalue, string code)
        {
            var data = this._database.Query<SystemCodeDetails>($"SELECT * FROM [SystemCodeDetails] WHERE [SystemDetailCode] = '{code}'").Single();
            return data;
        }

        public SystemCodeDetails SystemCodeGetByCode(string code)
        {
            var data = this._database.Query<SystemCodeDetails>($"SELECT * FROM [SystemCodeDetails] WHERE [SystemCode] = '{code}'").Single();
            return data;
        }

        public List<SystemCodeDetails> SystemCodeDetailGetByCode(string category)
        {
            var data = this._database.Query<SystemCodeDetails>($"SELECT * FROM [SystemCodeDetails] WHERE [SystemCode] = '{category}'");
            return data;
        }

        public List<SystemCodeDetails> SystemCodeDetailsGetAll()
        {
            var data = this._database.Query<SystemCodeDetails>($"SELECT * FROM [SystemCodeDetails]").ToList();
            return data;
        }

        public object GetTableRow(string tableName, string column, string value)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column + " = '" + value + "'";
            return _database.Query(map, query, obj).Single();
        }

        public List<object> GetTableRows(string tableName, string column, string value)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column + " = '" + value + "'";
            return _database.Query(map, query, obj).ToList();
        }
        public List<T> GetTableRows<T>(string tableName, string column, string value)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column + " = '" + value + "'";
            return _database.Query(map, query, obj).Cast<T>().ToList();
        }

        public List<T> GetTableRows<T>(string tableName, string column1, string value1, string column2, string value2)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column1 + " = '" + value1 + "' AND " + column2 + " = '" + value2 + "'";
            return _database.Query(map, query, obj).Cast<T>().ToList();
        }

        public List<T> GetTableRows<T>(string tableName)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + "";
            return _database.Query(map, query, obj).Cast<T>().ToList();
        }


        public T GetTableRow<T>(string tableName, string column, string value)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column + " = '" + value + "'";
            return _database.Query(map, query, obj).Cast<T>().Single();
        }


        public List<object> GetTableRows(string tableName)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + "";
            var tablerows = _database.Query(map, query, obj);

            return tablerows != null? _database.Query(map, query, obj).ToList() : null;
        }

        public object GetProfileImage(string value, bool isupdate = false)
        {
            var document = this._database.Query<SystemCodeDetails>($"SELECT Id FROM [SystemCodeDetails] WHERE [SystemDetailCode] = 'PROFILE IMAGE'").Single();
           
            if (isupdate)
            {
                var profileImageU = this._database.Query<ChildrenUpdatesDocuments>($"SELECT FilePath FROM ChildrenUpdatesDocuments WHERE ChildId = '" + value + "' AND DocumentTypeId = " + document.Id + "");
                if (profileImageU.Count > 0)
                    return profileImageU.Single();
            }
            else
            {
                var profileImageR = this._database.Query<ChildrenDocuments>($"SELECT FilePath FROM ChildrenDocuments WHERE ChildId = '" + value + "' AND DocumentTypeId = " + document.Id + "");
                if (profileImageR.Count > 0)
                    return profileImageR.Single();
            }

            return null;
        }

        public List<Locations> GetLocations()
        {
            return this._database.Query<Locations>($"SELECT * FROM Locations"); ;
        }

        public Locations GetLocationById(string value)
        {
            return this._database.Query<Locations>($"SELECT * FROM Locations WHERE Id = '" + value + "'").Single();
        }

        public Locations GetLocationByCode(string value)
        {
            return this._database.Query<Locations>($"SELECT * FROM Locations WHERE Code = '" + value + "'").Single();
        }

        public object DeleteTableRowById(string tableName, string columnName, string value)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "DELETE FROM [" + tableName + "] WHERE [" + columnName + "] = '" + value + "'";
            return _database.Query(map, query, obj).Single();
        }

        public object DeleteTableRowByIds(string tableName, string columnName, string value)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "DELETE FROM [" + tableName + "] WHERE [" + columnName + "] = '" + value + "'";
            return _database.Query(map, query, obj).ToList();
        }

        public object DeleteDocumentByIds(string tableName, string childid, string documenttypeid)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "DELETE FROM [" + tableName + "] WHERE DocumentTypeId = '" + documenttypeid + "' AND ChildId = '" + childid + "'";
            return _database.Query(map, query, obj).ToList();
        }

        public object DeleteChoreByIds(string tableName, string childid)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "DELETE FROM [" + tableName + "] WHERE ChildId = '" + childid + "'";
            return _database.Query(map, query, obj).ToList();
        }

        public object DeleteMLEByIds(string tableName, string childid, string lifeevent)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "DELETE FROM [" + tableName + "] WHERE ChildId = '" + childid + "' AND LifeEvent = '" + lifeevent + "'";
            return _database.Query(map, query, obj).ToList();
        }

        public object DeleteFavouriteActivitiesByIds(string tableName, string childid)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "DELETE FROM [" + tableName + "] WHERE ChildId = '" + childid + "'";
            return _database.Query(map, query, obj).ToList();
        }

        public object DeleteAppSettings(string tableName, string value)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "DELETE FROM SystemCodeDetails WHERE [SystemCode] = '" + value + "'";
            return _database.Query(map, query, obj).ToList();
        }


        #endregion

    }

}
