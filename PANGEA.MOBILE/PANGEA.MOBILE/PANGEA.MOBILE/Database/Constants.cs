﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PANGEA.MOBILE.Database
{
    class Constants
    {
        public static string BaseApiAddress => "https://dcad-62-8-71-72.ngrok.io/";
        public static string BaseSiteAddress => "https://dcad-62-8-71-72.ngrok.io/";


        /* This should be mapped to Legacy Server */
        //public static string BaseApiAddressV2 => "https://3ad9-102-217-64-9.ngrok.io/";
        //public static string BaseSiteAddressV2 => "https://3ad9-102-217-64-9.ngrok.io/";
    }
}
