﻿using PANGEA.MOBILE.ViewModels;
using PANGEA.MOBILE.ViewModels.API;
using System.Threading.Tasks;

namespace PANGEA.MOBILE.Interface
{
    public interface IToast
    {
        void SendToast(string message);
        object MakeText(SyncViewModel syncViewModel, string v, object @long);
    }

}
