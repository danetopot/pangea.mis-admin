﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PANGEA.MOBILE.Interface
{
    public interface IPushNotifications
    {
        bool IsRegistered { get; }

        void OpenSettings();

        Task<bool> RegisterForNotifications();
    }
}
