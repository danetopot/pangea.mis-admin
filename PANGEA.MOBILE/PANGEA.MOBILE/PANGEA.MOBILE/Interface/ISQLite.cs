﻿using SQLite;

namespace PANGEA.MOBILE.Interface
{
    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}
