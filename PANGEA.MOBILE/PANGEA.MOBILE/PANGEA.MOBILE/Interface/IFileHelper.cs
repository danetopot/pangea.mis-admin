﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PANGEA.MOBILE.Interface
{
    public interface IFileHelper
    {
        string GetLocalFilePath(string filename);
    }
}
