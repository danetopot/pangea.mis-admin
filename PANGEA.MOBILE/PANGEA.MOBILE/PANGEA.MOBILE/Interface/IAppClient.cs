﻿using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.ViewModels.API;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PANGEA.MOBILE.Interface
{
    public interface IAppClient
    {
        Task LoginAsync(string userId);
        Task LogoutAsync(string userId);
        Task<ApiSettupViewModel> GetSettingsAsync();
        Task<ApiStatusViewModel> PostRegistrationAsyc(
            Children childinfo,
            List<ChildrenChores> childchoresinfo,
            List<ChildrenFavouriteActivities> childfavactivitiesinfo,
            List<ChildrenDocuments> childdocumentsinfo,
            LocalDeviceInfo deviceInfo);

        Task<ApiStatusViewModel> PostUpdateAsyc(
            ChildrenUpdates childinfo,
            List<ChildrenChores> childchoresinfo,
            List<ChildrenFavouriteActivities> childfavactivitiesinfo,
            List<ChildrenUpdatesDocuments> childdocumentsinfo,
            LocalDeviceInfo deviceInfo);

    }
}
