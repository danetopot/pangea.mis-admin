﻿using PANGEA.MOBILE.Helpers;
using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.ViewModels;
using PANGEA.MOBILE.ViewModels.API;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PANGEA.MOBILE.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : ContentPage
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        private bool isRunning = false;
        private MainPage RootPage { get => Application.Current.MainPage as MainPage; }
        private List<HomeMenuItem> menuItems;
        private LocalBaseViewModel vm;

        public MenuPage()
        {
            InitializeComponent();
            BindingContext = vm = new LocalBaseViewModel(Navigation);

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        public void LoadData()
        {
            menuItems = new List<HomeMenuItem>
                {
                    new HomeMenuItem {Id = MenuItemType.HomePage, Title="Home" },
                    new HomeMenuItem {Id = MenuItemType.PlanPage, Title="Plans" },
                    new HomeMenuItem {Id = MenuItemType.RegistrationPage, Title="Registrations" },
                    new HomeMenuItem {Id = MenuItemType.UpdatePage, Title="Updates" },
                    new HomeMenuItem {Id = MenuItemType.SyncPage, Title="Data Transfer" },
                    new HomeMenuItem {Id = MenuItemType.SettingsPage, Title="Settings" },
                    new HomeMenuItem {Id = MenuItemType.LogoutPage, Title="Logout" },
                };

            ListViewMenu.ItemsSource = menuItems;
            ListViewMenu.SelectedItem = menuItems[0];
            ListViewMenu.ItemSelected += async (sender, e) =>
            {
                if (e.SelectedItem == null)
                {
                    return;
                }

                var id = (int)((HomeMenuItem)e.SelectedItem).Id;
                Settings.Current.Module = ((HomeMenuItem)e.SelectedItem).Title;
                await RootPage.NavigateFromMenu(id);
            };

            // Set UserName
            vm.UserName = $"{Settings.Current.UserName} "; 

        }
    }
}