﻿using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace PANGEA.MOBILE.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DocumentListingPage : ContentPage
    {
        private RegistrationEditViewModel vm;
        private List<ChildrenDocuments> registeredChildrenDocuments;
        private ChildrenDocuments registeredChildDocument;
        private int? ChildId = null;
        private int? DocumentId = null;
        private int? DocumentTypeId = null;
        private string DocumentName = string.Empty;
        private string DocumentPath = string.Empty;

        public DocumentListingPage(int? id)
        {
            InitializeComponent();
            BindingContext = vm = new RegistrationEditViewModel(Navigation, id);
            ChildId = id;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private void LoadData()
        {
            registeredChildrenDocuments = new List<ChildrenDocuments>(vm.GetRegisteredChildrenDocuments());
            ListViewDocuments.ItemsSource = registeredChildrenDocuments;
            vm.AttachedDocuments = $"Attached Files { registeredChildrenDocuments.Count }".ToString().ToUpper();
            vm.PendingDocuments = $"Pending Files { 5 - registeredChildrenDocuments.Count }".ToString().ToUpper();
        }

        private async void BtnRemoveRegistrationDocument_Clicked(object sender, EventArgs e)
        {
            vm.IsBusy = true;
            if (!string.IsNullOrEmpty(DocumentId.ToString()))
            {

                vm.Message = $"Processing . . .";
                App.Database.DeleteDocumentByIds($"ChildrenDocuments", $"{ChildId}", $"{DocumentTypeId}");
                LoadData();
                await Task.Delay(TimeSpan.FromSeconds(1));
            }

            
            vm.Message = $"Found Nothing To Process. Exiting ..";
            await Task.Delay(TimeSpan.FromSeconds(1));
            DocumentId = null;
            vm.IsBusy = false;

            #region Old Code
            /*
                if (!string.IsNullOrEmpty(DocumentId.ToString()))
                {
                    bool confirm = await DisplayAlert("Confirm", "Delete " + DocumentName + "?", "Yes", "No");
                    if (confirm)
                    {
                        App.Database.DeleteDocumentByIds($"ChildrenDocuments", $"{ChildId}", $"{DocumentTypeId}");
                        LoadData();
                    }
                }
                else
                {
                    await DisplayAlert("Alert", "Please Select Document", "OK");
                }
                */
            #endregion

        }

        private async void BtnViewRegistrationDocument_Clicked(object sender, EventArgs e)
        {
            vm.IsBusy = true;
            if (!string.IsNullOrEmpty(DocumentId.ToString()))
            {

                vm.Message = $"Processing . . .";
                await Task.Delay(TimeSpan.FromSeconds(1));
                await Navigation.PushAsync(new DocumentDetailPage(DocumentId, null));
            }

            vm.Message = $"Found Nothing To Process. Exiting ..";
            await Task.Delay(TimeSpan.FromSeconds(1));
            DocumentId = null; 
            vm.IsBusy = false;

            #region Old Code
            /*
                if (!string.IsNullOrEmpty(DocumentId.ToString()))
                {
                    await Navigation.PushAsync(new DocumentDetailPage(DocumentId));
                }
                else
                {
                    await DisplayAlert("Alert", "Please Select Document", "OK");
                }

                DocumentId = null;
                */
            #endregion
        }

        private void ListViewDocuments_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                var selectedDoc = (ChildrenDocuments)e.SelectedItem;
                DocumentId = selectedDoc.Id;
                DocumentName = selectedDoc.DocumentName;
                DocumentTypeId = selectedDoc.DocumentTypeId;
                DocumentPath = selectedDoc.FilePath;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}