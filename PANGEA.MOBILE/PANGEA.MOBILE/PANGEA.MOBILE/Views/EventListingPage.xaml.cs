﻿using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.ViewModels;
using PANGEA.MOBILE.ViewModels.API;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PANGEA.MOBILE.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EventListingPage : ContentPage
    {
        private UpdateEventViewModel vm;
        private List<ChildrenLifeEvents> registeredChildrenLifeEvents;
        private int? childid;

        public EventListingPage(int? childid)
        {
            InitializeComponent();
            BindingContext = vm = new UpdateEventViewModel(Navigation, childid, null);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private void LoadData()
        {
            registeredChildrenLifeEvents = new List<ChildrenLifeEvents>(vm.GetRegisteredChildrenLifeEvents());
            ListViewEvents.ItemsSource = registeredChildrenLifeEvents;
            vm.AttachedMLEs = $"Current Major Life Events    { registeredChildrenLifeEvents.Count }".ToString().ToUpper();
        }
    }
}