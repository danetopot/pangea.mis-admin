﻿using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.ViewModels;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PANGEA.MOBILE.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistrationListingPage : ContentPage
    {
        private RegistrationViewModel vm;
        private List<Children> registeredChildren;

        public RegistrationListingPage()
        {
            InitializeComponent();
            BindingContext = vm = new RegistrationViewModel(Navigation);
            LoadData();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private void LoadData()
        {
            registeredChildren = new List<Children>(vm.GetRegisteredChildren());
            IsBusy = true;
            if (!string.IsNullOrEmpty(vm.Filter))
            {
                var filteredChildren = vm.GetRegisteredChildren().Where(x => x.Haystack.ToLower().Contains(vm.Filter.ToLower()));
                registeredChildren = filteredChildren.ToList();
            }

            ListViewRegistrations.ItemsSource = registeredChildren;

            #region LoadIsDataReady
            var settingsData = App.Database.GetTableRows("SystemCodeDetails");
            var locationsData = App.Database.GetTableRows("Locations");
            vm.IsNotDataReady = true;
            vm.IsDataReady = false;
            if (settingsData.Count > 0 && locationsData.Count > 0)
            {
                vm.IsNotDataReady = false;
                vm.IsDataReady = true;
            }
            #endregion

            IsBusy = false;
        }

        private void Filter_TextChanged(object sender, TextChangedEventArgs e)
        {
            LoadData();
        }
    }
}