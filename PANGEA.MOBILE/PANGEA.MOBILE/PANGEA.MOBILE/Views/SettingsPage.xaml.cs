﻿using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.ViewModels;
using PANGEA.MOBILE.ViewModels.API;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PANGEA.MOBILE.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingsPage : ContentPage
    {
        private SettingsViewModel vm;

        public SettingsPage()
        {
            InitializeComponent();
            BindingContext = vm = new SettingsViewModel(Navigation);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private void LoadData()
        {
            try
            {
               // Load Default Settings
               var defaultlanguages = App.Database.SystemCodeDetailGetByCode($"DefaultLanguage");
                if (defaultlanguages.Count > 0)
                    vm.DefaultLanguage = defaultlanguages[0].SystemDetailDescription.ToUpper();
                DefaultLanguageLabel.Text = vm.DefaultLanguage;

                var defaultlocation = App.Database.SystemCodeDetailGetByCode($"DefaultLocation");
                if (defaultlocation.Count > 0)
                    vm.DefaultLocation = defaultlocation[0].SystemDetailDescription.ToUpper();
                DefaultLocationLabel.Text = vm.DefaultLocation;

                var defaultdataserver = App.Database.SystemCodeDetailGetByCode($"DefaultDataServer");
                if (defaultdataserver.Count > 0)
                    vm.DefaultDataServer = defaultdataserver[0].SystemDetailDescription.ToUpper();
                DefaultDataServerLabel.Text = vm.DefaultDataServer;

                var defaultdeleteonsync = App.Database.SystemCodeDetailGetByCode($"DefaultDeleteOnSync");
                if (defaultdeleteonsync.Count > 0)
                    vm.DeleteOnSync = defaultdeleteonsync[0].SystemDetailCode == "DELETE ON UPLOAD" ? true : false;
                    vm.DeleteOnSyncStatus = defaultdeleteonsync[0].SystemDetailCode == "DELETE ON UPLOAD" ? $"DELETE ON UPLOAD".ToUpper() : $"KEEP ON UPLOAD".ToUpper();
                DefaultDeleteOnSyncLabel.Text = vm.DeleteOnSyncStatus;


                vm.LoadedLocationOptions.AddRange(App.Database.GetLocations());
                vm.LoadedLanguageOptions.AddRange(App.Database.SystemCodeDetailGetByCode("Languages"));
                vm.LoadedDataServerOptions.AddRange(App.Database.SystemCodeDetailGetByCode("Servers"));

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void RefreshView_Refreshing(object sender, EventArgs e)
        {
            SettingsRefreshView.IsRefreshing = true;
            LoadData();
            SettingsRefreshView.IsRefreshing = false;
        }
    }
}