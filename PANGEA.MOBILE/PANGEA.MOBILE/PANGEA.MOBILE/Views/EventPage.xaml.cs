﻿using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.ViewModels;
using Plugin.FilePicker;
using System;
using System.Collections.ObjectModel;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace PANGEA.MOBILE.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EventPage : ContentPage
    {
        private UpdateEventViewModel vm;

        public EventPage(int? childid)
        {
            InitializeComponent();
            BindingContext = vm = new UpdateEventViewModel(Navigation, childid, null);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }
    }
}