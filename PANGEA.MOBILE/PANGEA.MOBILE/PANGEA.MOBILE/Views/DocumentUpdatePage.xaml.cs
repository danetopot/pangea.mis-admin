﻿using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.ViewModels;
using Plugin.FilePicker;
using System;
using System.Collections.ObjectModel;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PANGEA.MOBILE.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DocumentUpdatePage : ContentPage
    {
        private UpdateDocumentViewModel vm;

        public DocumentUpdatePage(int? childid)
        {
            InitializeComponent();
            BindingContext = vm = new UpdateDocumentViewModel(Navigation, childid);

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private async void CaptureDocument_Clicked(object sender, EventArgs e)
        {
            vm.IsBusy = true;
            vm.Message = $"Processing . . .";
            vm.IsDocumentLoaded = true;

            var photo = await Plugin.Media.CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions() { });


            if (photo != null)
                DocumentImage.Source = ImageSource.FromStream(() => { return photo.GetStream(); });
            vm.DocumentPath = photo.Path;
            vm.DocumentImage = ConvertImageToByteArray(photo.Path);
            vm.IsBusy = false;
            vm.Message = string.Empty;
        }

        private async void UploadDocument_Clicked(object sender, EventArgs e)
        {
            vm.IsBusy = true;
            vm.Message = $"Processing . . .";
            vm.IsDocumentLoaded = true;

            var photo = await CrossFilePicker.Current.PickFile();

            if (photo != null)
                DocumentImage.Source = ImageSource.FromStream(() => { return photo.GetStream(); });
            vm.DocumentPath = photo.FilePath;
            vm.DocumentImage = photo.DataArray;
            vm.IsBusy = false;
            vm.Message = string.Empty;
        }


        public static byte[] ConvertImageToByteArray(string imagePath)
        {
            byte[] imageByteArray = null;
            FileStream fileStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
            using (BinaryReader reader = new BinaryReader(fileStream))
            {
                imageByteArray = new byte[reader.BaseStream.Length];
                for (int i = 0; i < reader.BaseStream.Length; i++)
                    imageByteArray[i] = reader.ReadByte();
            }
            return imageByteArray;
        }
    }
}