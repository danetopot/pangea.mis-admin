﻿using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.ViewModels;
using PANGEA.MOBILE.ViewModels.API;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PANGEA.MOBILE.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlanListingPage : TabbedPage
    {
        private PlanViewModel vm;

        public PlanListingPage()
        {
            InitializeComponent();
            BindingContext = vm = new PlanViewModel(Navigation);

        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private void LoadData()
        {
            var regPlansList = vm.GetRegistrationPlans();
            var registrationPlans = new List<RegistrationPlans>(vm.GetRegistrationPlans());
            ListViewRegistrationPlans.ItemsSource = registrationPlans;

            var updatePlansList = vm.GetRegistrationPlans();
            var updatePlans = new List<UpdatePlans>(vm.GetUpdatePlans());
            ListViewUpdatePlans.ItemsSource = updatePlans;
        }

    }
}