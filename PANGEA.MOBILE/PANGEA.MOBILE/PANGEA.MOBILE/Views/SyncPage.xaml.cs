﻿using PANGEA.MOBILE.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PANGEA.MOBILE.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SyncPage : TabbedPage
    {
        private SyncViewModel vm;

        public SyncPage()
        {
            InitializeComponent();
            BindingContext = vm = new SyncViewModel(Navigation);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            vm.LoadData();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }
    }
}