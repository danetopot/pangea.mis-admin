﻿using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.ViewModels;
using Plugin.FilePicker;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PANGEA.MOBILE.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EventDetailPage : ContentPage
    {
        private UpdateEventViewModel vm;
        private List<ChildrenLifeEvents> registeredChildrenLifeEvents;
        private int? EventId = 0;

        public EventDetailPage(int? eventid)
        {
            InitializeComponent();
            BindingContext = vm = new UpdateEventViewModel(Navigation,null, eventid);

            #region LoadEvent
            var eventData = (ChildrenLifeEvents) App.Database.GetTableRow("ChildrenLifeEvents", "Id", $"{(int)eventid}");
            vm.UpdateLifeEvent = eventData;
            vm.LifeEvent = eventData.LifeEvent;
            vm.LifeEventId = eventData.Id;
            #endregion
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private async void BtnRemoveLifeEvent_Clicked(object sender, EventArgs e)
        {
            vm.IsBusy = true;
            if (!string.IsNullOrEmpty(vm.LifeEventId.ToString()))
            {

                vm.Message = $"Processing . . .";
                App.Database.DeleteTableRowByIds($"ChildrenLifeEvents", $"Id", $"{vm.LifeEventId}");
                await Navigation.PopAsync();
                await Task.Delay(TimeSpan.FromSeconds(1));
            }


            vm.Message = $"Found Nothing To Process. Exiting ..";
            await Task.Delay(TimeSpan.FromSeconds(1));
            vm.LifeEventId = null;
            vm.IsBusy = false;



        }
    }
}