﻿using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.ViewModels;
using PANGEA.MOBILE.ViewModels.API;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PANGEA.MOBILE.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UpdateListingPage : ContentPage
    {
        private int? childid;
        private UpdateViewModel vm;
        private List<ChildrenUpdates> updateChildren;

        public UpdateListingPage()
        {
            InitializeComponent();
            BindingContext = vm = new UpdateViewModel(Navigation, null);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private void LoadData()
        {
            updateChildren = new List<ChildrenUpdates>(vm.GetUpdateChildren());
            IsBusy = true;
            if (!string.IsNullOrEmpty(vm.Filter))
            {
                var filteredChildren = vm.GetUpdateChildren().Where(x => x.Haystack.ToLower().Contains(vm.Filter.ToLower()));
                updateChildren = filteredChildren.ToList();
            }

            ListViewUpdates.ItemsSource = updateChildren;

            #region LoadIsDataReady
            var settingsData = App.Database.GetTableRows("SystemCodeDetails");
            var locationsData = App.Database.GetTableRows("Locations");
            vm.IsNotDataReady = true;
            vm.IsDataReady = false;
            if (settingsData.Count > 0 && locationsData.Count > 0)
            {
                vm.IsNotDataReady = false;
                vm.IsDataReady = true;
            }
            #endregion

            IsBusy = false;
        }

        private void Filter_TextChanged(object sender, TextChangedEventArgs e)
        {
            LoadData();
        }
    }
}