﻿using PANGEA.MOBILE.Helpers;
using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.ViewModels;
using Plugin.FilePicker;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PANGEA.MOBILE.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DocumentDetailPage : ContentPage
    {
        private RegistrationDocumentViewModel vm;
        private int? DocumentId = 0;
        private string DocumentPath = string.Empty;

        public DocumentDetailPage(int? documentid, int? childid)
        {
            InitializeComponent();
            BindingContext = vm = new RegistrationDocumentViewModel(Navigation, childid);

            #region LoadDocument
            if (Settings.Current.Module == $"Registrations") 
            {
                vm.DocumentId = (int)documentid;
                var documentData = App.Database.GetTableRow("ChildrenDocuments", "Id", $"{vm.DocumentId}");
                vm.RegistrationDocument = (ChildrenDocuments)documentData;
                vm.ChildId = (vm.RegistrationDocument).ChildId;
                vm.DocumentId = (vm.RegistrationDocument).Id;
                vm.DocumentTypeId = (vm.RegistrationDocument).DocumentTypeId;
                vm.DocumentPath = (vm.RegistrationDocument).FilePath;
                vm.DocumentName = (vm.RegistrationDocument).DocumentName;
            }

            if (Settings.Current.Module == $"Updates") 
            {
                vm.DocumentId = (int)documentid;
                var documentData = App.Database.GetTableRow("ChildrenUpdatesDocuments", "Id", $"{vm.DocumentId}");
                vm.UpdatesDocument = (ChildrenUpdatesDocuments)documentData;
                vm.ChildId = (vm.UpdatesDocument).ChildId;
                vm.DocumentId = (vm.UpdatesDocument).Id;
                vm.DocumentTypeId = (vm.UpdatesDocument).DocumentTypeId;
                vm.DocumentPath = (vm.UpdatesDocument).FilePath;
                vm.DocumentName = (vm.UpdatesDocument).DocumentName;
            }
            #endregion
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private async void BtnRemoveRegistrationDocument_Clicked(object sender, EventArgs e)
        {
            vm.IsBusy = true;
            if (!string.IsNullOrEmpty(DocumentId.ToString()))
            {
                if (Settings.Current.Module == $"Registrations")
                {
                    vm.Message = $"Processing . . .";
                    App.Database.DeleteDocumentByIds($"ChildrenDocuments", $"{vm.ChildId}", $"{vm.DocumentTypeId}");
                    await Navigation.PopAsync();
                    await Task.Delay(TimeSpan.FromSeconds(1));
                }

                if (Settings.Current.Module == $"Updates")
                {
                    vm.Message = $"Processing . . .";
                    App.Database.DeleteDocumentByIds($"ChildrenUpdatesDocuments", $"{vm.ChildId}", $"{vm.DocumentTypeId}");
                    await Navigation.PopAsync();
                    await Task.Delay(TimeSpan.FromSeconds(1));
                }
                
            }

            vm.Message = $"Found Nothing To Process. Exiting ..";
            await Task.Delay(TimeSpan.FromSeconds(1));
            DocumentId = null;
            vm.IsBusy = false;

            #region Old Code
            /*
                bool confirm = await DisplayAlert("Confirm", "Delete " + vm.DocumentName + "?", "Yes", "No");
                if (confirm)
                {
                    App.Database.DeleteDocumentByIds($"ChildrenDocuments", $"{vm.ChildId}", $"{vm.DocumentTypeId}");
                    await Navigation.PopAsync();
                }
                */ 
            #endregion



        }
    }
}