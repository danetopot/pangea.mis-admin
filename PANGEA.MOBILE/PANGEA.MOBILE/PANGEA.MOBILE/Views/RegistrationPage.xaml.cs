﻿using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.ViewModels;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PANGEA.MOBILE.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistrationPage : ContentPage
    {
        private RegistrationViewModel vm;

        public RegistrationPage(Guid? id)
        {
            InitializeComponent();
            BindingContext = vm = new RegistrationViewModel(Navigation);
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            //LoadData();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        public void LoadData()
        {
            /*
            /* Load Chores Options*/
            var items = App.Database.SystemCodeDetailGetByCode("Chores");
            var collection = new ObservableCollection<SystemCodeDetails>();
            foreach (var item in items)
            {
                var choreitem = (SystemCodeDetails)item;
                collection.Add(choreitem);
            }
            ListViewChores.ItemsSource = collection;
            ListViewChores.ItemSelected += async (sender, e) 
                =>
            {
                if (e.SelectedItem == null)
                {
                    return;
                }

                var id = (int)((SystemCodeDetails)e.SelectedItem).Id;
            };


            /* Load Favourite Activity Options*/
            items = App.Database.SystemCodeDetailGetByCode("Favourite Activity");
            collection = new ObservableCollection<SystemCodeDetails>();
            foreach (var item in items)
            {
                var choreitem = (SystemCodeDetails)item;
                collection.Add(choreitem);
            }
            ListViewFavActivities.ItemsSource = collection;
            ListViewFavActivities.ItemSelected += async (sender, e) =>
            {
                if (e.SelectedItem == null)
                {
                    return;
                }

                var id = (int)((SystemCodeDetails)e.SelectedItem).Id;

                // await Navigation.PushAsync(new RegistrationEditPage());
            };

        }
    }
}