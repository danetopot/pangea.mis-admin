﻿using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PANGEA.MOBILE.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistrationEditPage : ContentPage
    { 
        private RegistrationEditViewModel vm;
        private int? childid;
        public RegistrationEditPage(int? id)
        {
            InitializeComponent();
            BindingContext = vm = new RegistrationEditViewModel(Navigation, id);
            childid = id;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private void BtnManageDocuments_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new DocumentListingPage(childid));
        }

        private void LoadData()
        {
            #region LoadedDocuments
            var loadedDocuments = App.Database.GetTableRows("ChildrenDocuments", "ChildId", childid.ToString());
            vm.DocumentTag = $"{loadedDocuments.Count.ToString()} Documents";
            vm.NumberOfDocuments = $" Manage Documents  {loadedDocuments.Count.ToString()}/5";
            vm.ProfileImage = $"person.png";


            foreach (var loadedDocument in loadedDocuments)
            {
                var docitem = (ChildrenDocuments)loadedDocument;
                var doctype = (App.Database.SystemCodeDetailGetById((int)docitem.DocumentTypeId)).SystemDetailCode;
                if (doctype == "PROFILE IMAGE")
                {
                    vm.ProfileImage = docitem.FilePath;
                }
            }
            #endregion
        }

        private async void BtnDeleteRegistration_Clicked(object sender, EventArgs e)
        {
            var model = (Children)App.Database.GetTableRow($"Children", $"Id", $"{childid}");

            bool confirm = await DisplayAlert("Confirm", "Delete " + model.FullName + "?\n\nRecord will be permanently deleted from the phone/tablet.", "Yes", "No");
            if (confirm)
            {
                App.Database.DeleteTableRowByIds($"ChildrenDocuments", $"ChildId", $"{model.Id}");

                App.Database.DeleteTableRowByIds($"ChildrenChores", $"ChildId", $"{model.Id}");

                App.Database.DeleteTableRowByIds($"ChildrenFavouriteActivities", $"ChildId", $"{model.Id}");

                App.Database.DeleteTableRowByIds($"Children", $"Id", $"{model.Id}");

                await Navigation.PopAsync();
                await Navigation.PushAsync(new RegistrationListingPage());
            }
        }
    }
}