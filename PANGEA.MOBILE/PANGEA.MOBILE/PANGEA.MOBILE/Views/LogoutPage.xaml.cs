﻿using PANGEA.MOBILE.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PANGEA.MOBILE.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LogoutPage : ContentPage
    {
        private LoginViewModel vm;
        public LogoutPage()
        {
            InitializeComponent();
            BindingContext = vm = new LoginViewModel(Navigation);
        }


        private async void BtnLogout_Clicked(object sender, System.EventArgs e)
        {
            //Navigation.InsertPageBefore(new LoginPage(), this);
            //await Navigation.PopAsync();
        }
    }
}