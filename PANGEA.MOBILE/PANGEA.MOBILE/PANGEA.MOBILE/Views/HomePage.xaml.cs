﻿using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.ViewModels;
using PANGEA.MOBILE.ViewModels.API;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
namespace PANGEA.MOBILE.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {
        private bool isRunning = false;
        private LocalBaseViewModel vm;

        private MainPage RootPage { get => Application.Current.MainPage as MainPage; }

        private List<HomeMenuItem> menuItems;

        public HomePage()
        {
            InitializeComponent();
            BindingContext = vm = new LocalBaseViewModel(Navigation);

            #region This is for New Pangea Web API
            /*
            var regPlans = (App.Database.GetTableRows("RegistrationPlans")).ToList();
            var plansData = $"Plans {regPlans.Count()}";
            var registrationsData = $"Registrations {0}";
            var enrolmentsData = $"Enrolments {0}/{0}";
            var updatesData = $"Updates {0}/{0}";

            menuItems = new List<HomeMenuItem>
            {
                new HomeMenuItem {Id = MenuItemType.HomePage, Title=plansData },
                new HomeMenuItem {Id = MenuItemType.HomePage, Title=registrationsData },
                new HomeMenuItem {Id = MenuItemType.HomePage, Title=enrolmentsData },
                new HomeMenuItem {Id = MenuItemType.HomePage, Title=updatesData },
            }; 
            */
            #endregion
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
        }

        private void LoadData()
        {
            string plansData = $"Plans 0";
            string registrationsData = $"Enrolments 0";
            string updatesData = $"Updated 0";
            string pendingSyncData = $"Pending Sync - Enrolments(1) Updates(0)";
            var registredChildren = App.Database.GetTableRows("Children").ToList();
            if(registredChildren.Count > 0)
            {
                int regUpdateId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "REGUPDATE")).Id;
                int pendingSyncId = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "SYNCINSERTED")).Id;
                var pendingChildren = App.Database.GetTableRows("Children", "SyncStatusId", $"{pendingSyncId}").ToList();
                var pendingUpdates = App.Database.GetTableRows("Children", "ChildStatusId", $"{regUpdateId}").ToList(); ;

                var registrationPlans = App.Database.GetTableRows("RegistrationPlans").ToList();

                plansData = $"Plans {registrationPlans.Count}";
                registrationsData = $"Enrolments {registredChildren.Count}";
                updatesData = $"Updated {pendingUpdates.Count}/{pendingUpdates.Count}";
                //pendingSyncData = $"Pending Sync {pendingChildren.Count}"; 
            }

            menuItems = new List<HomeMenuItem>
            {
                new HomeMenuItem {Id = MenuItemType.HomePage, Title=plansData },
                new HomeMenuItem {Id = MenuItemType.HomePage, Title=registrationsData },
                new HomeMenuItem {Id = MenuItemType.HomePage, Title=updatesData },
                //new HomeMenuItem {Id = MenuItemType.HomePage, Title=pendingSyncData },
            };

            ListViewMenu.ItemsSource = menuItems;
            ListViewMenu.ItemSelected += async (sender, e) =>
            {
                ListViewMenu.SelectedItem = null;
            };


            #region LoadIsDataReady
            var settingsData = App.Database.GetTableRows("SystemCodeDetails");
            var locationsData = App.Database.GetTableRows("Locations");
            vm.IsNotDataReady = true;
            if (settingsData.Count > 0 && locationsData.Count > 0)
            {
                vm.IsNotDataReady = false;
            }
            #endregion
        }
    }
}