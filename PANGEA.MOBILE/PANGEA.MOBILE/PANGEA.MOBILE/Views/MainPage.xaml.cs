﻿using FormsToolkit;
using PANGEA.MOBILE.Database;
using PANGEA.MOBILE.Helpers;
using PANGEA.MOBILE.ViewModels;
using PANGEA.MOBILE.ViewModels.API;
using PANGEA.MOBILE.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PANGEA.MOBILE.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : FlyoutPage
    {
        private bool _isRunning = false;
        private LocalBaseViewModel vm;
        Dictionary<int, NavigationPage> MenuPages = new Dictionary<int, NavigationPage>();

        public MainPage()
        {
            InitializeComponent();
            BindingContext = vm = new LocalBaseViewModel(Navigation);
            FlyoutLayoutBehavior = FlyoutLayoutBehavior.Popover;

            MenuPages.Add((int)MenuItemType.HomePage, (NavigationPage)Detail);

        }

        public async Task NavigateFromMenu(int id)
        {
            if (!MenuPages.ContainsKey(id))
            {
                switch (id)
                {
                    case (int)MenuItemType.MainPage:
                        MenuPages.Add(id, new NavigationPage(new MainPage()));
                        break;
                    case (int)MenuItemType.HomePage:
                        MenuPages.Add(id, new NavigationPage(new HomePage()));
                        break;
                    case (int)MenuItemType.PlanPage:
                        MenuPages.Add(id, new NavigationPage(new PlanListingPage()));
                        break;
                    case (int)MenuItemType.RegistrationPage:
                        MenuPages.Add(id, new NavigationPage(new RegistrationListingPage()));
                        break;
                    case (int)MenuItemType.UpdatePage:
                        MenuPages.Add(id, new NavigationPage(new UpdateListingPage()));
                        break;
                    case (int)MenuItemType.SyncPage:
                        MenuPages.Add(id, new NavigationPage(new SyncPage()));
                        break;
                    case (int)MenuItemType.SettingsPage:
                        MenuPages.Add(id, new NavigationPage(new SettingsPage()));
                        break;
                    case (int)MenuItemType.LogoutPage:
                        MenuPages.Add(id, new NavigationPage(new LogoutPage()));
                        break;

                }
            }

            var newPage = MenuPages[id];

            if (newPage != null && Detail != newPage)
            {
                Detail = newPage;

                if (Device.RuntimePlatform == Device.Android)
                    await Task.Delay(100);

                IsPresented = false;
            }
            if (newPage == null)
                return;

            // if we are on the same tab and pressed it again.
            if (Detail == newPage)
            {
                await newPage.Navigation.PopToRootAsync();
            }

        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            if (Settings.Current.FirstRun)
            {
                MessagingService.Current.SendMessage(MessageKeys.NavigateLogin);
            }

            _isRunning = true;


        }

        
    }
}






































