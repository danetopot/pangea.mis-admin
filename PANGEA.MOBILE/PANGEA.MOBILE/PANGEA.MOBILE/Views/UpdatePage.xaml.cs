﻿using PANGEA.MOBILE.Models;
using PANGEA.MOBILE.ViewModels;
using PANGEA.MOBILE.ViewModels.API;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PANGEA.MOBILE.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UpdatePage : ContentPage
    {
        private UpdateViewModel vm;
        private int? childid;

        public UpdatePage(int? id)
        {
            InitializeComponent();
            BindingContext = vm = new UpdateViewModel(Navigation, id);
            childid = id;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LoadData();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private void LoadData()
        {
            #region LoadedDocuments
            var documentstatusid = ((SystemCodeDetails)App.Database.GetTableRow($"SystemCodeDetails", $"SystemDetailCode", "DOCQCREADY")).Id;
            var loadedDocuments = App.Database.GetTableRows<ChildrenUpdatesDocuments>("ChildrenUpdatesDocuments", "ChildId", $"{childid}", "DocumentStatusId", $"{documentstatusid}");
            vm.DocumentTag = $"{loadedDocuments.Count.ToString()} Documents";
            vm.NumberOfDocuments = $"Manage Documents  {loadedDocuments.Count.ToString()}/5";
            vm.ProfileImage = $"person.png";


            foreach (var loadedDocument in loadedDocuments)
            {
                var docitem = (ChildrenUpdatesDocuments)loadedDocument;
                var doctype = (App.Database.SystemCodeDetailGetById((int)docitem.DocumentTypeId)).SystemDetailCode;
                if (doctype == "PROFILE IMAGE")
                {
                    vm.ProfileImage = docitem.FilePath;
                }
            }
            #endregion
        }

        private void BtnManageDocuments_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new DocumentUpdateListingPage(childid));
        }
    }
}