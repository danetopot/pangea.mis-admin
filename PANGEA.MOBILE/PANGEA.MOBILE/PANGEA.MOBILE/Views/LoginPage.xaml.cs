﻿using PANGEA.MOBILE.Database;
using PANGEA.MOBILE.Helpers;
using PANGEA.MOBILE.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace PANGEA.MOBILE.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        private LoginViewModel vm;
        public LoginPage()
        {
            InitializeComponent();
            BindingContext = vm = new LoginViewModel(Navigation);
        }

        protected override bool OnBackButtonPressed()
        {
            return Settings.Current.FirstRun || base.OnBackButtonPressed();
        }
        private async void OnTermsClicked(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri(Constants.BaseSiteAddress + "/public/terms-and-conditions/"));
        }
    }
}