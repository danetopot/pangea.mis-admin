﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace PANGEA.MOBILE.Controls
{
    public sealed class StandardDateChooser : DatePicker
    {
        public static readonly BindableProperty EnterTextProperty = BindableProperty.Create(propertyName: "Placeholder", 
            returnType: typeof(string), 
            declaringType: typeof(StandardDateChooser), 
            defaultValue: default(string));
        public string Placeholder { get; set; }
    }
}
