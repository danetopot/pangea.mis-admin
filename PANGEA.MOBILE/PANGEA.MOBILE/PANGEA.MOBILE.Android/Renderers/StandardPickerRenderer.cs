﻿using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Support.V4.Content;
using PANGEA.MOBILE.Controls;
using PANGEA.MOBILE.Droid.Renderers;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(StandardPicker), typeof(StandardPickerRenderer))]

namespace PANGEA.MOBILE.Droid.Renderers
{
    public class StandardPickerRenderer : PickerRenderer
    {
        public StandardPickerRenderer(Context context) : base(context)
        {
        }

		StandardPicker element;

		protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
		{
            base.OnElementChanged(e);

            if (e.OldElement == null)
            {
                var nativeEditText = (global::Android.Widget.EditText)Control;
                var shape = new ShapeDrawable(new Android.Graphics.Drawables.Shapes.RectShape());
                shape.Paint.Color = Android.Graphics.Color.White;
                shape.Paint.SetStyle(Paint.Style.Stroke);
                nativeEditText.Background = shape;
            }
        }
	}
}