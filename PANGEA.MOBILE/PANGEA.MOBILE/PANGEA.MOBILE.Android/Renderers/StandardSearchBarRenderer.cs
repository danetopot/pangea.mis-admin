﻿using Android.Content;
using Android.Graphics.Drawables;
using PANGEA.MOBILE.Controls;
using PANGEA.MOBILE.Droid.Renderers;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(StandardSearchBar), typeof(StandardSearchBarRenderer))]

namespace PANGEA.MOBILE.Droid.Renderers
{
    public class StandardSearchBarRenderer : SearchBarRenderer
    {
        public StandardSearchBarRenderer(Context context) : base(context)
        {
        }

        public StandardSearchBar ElementV2 => Element as StandardSearchBar;

        protected override void OnElementChanged(ElementChangedEventArgs<SearchBar> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                var plateId = Resources.GetIdentifier("android:id/search_plate", null, null);
                var plate = Control.FindViewById(plateId);
                plate.SetBackgroundColor(Android.Graphics.Color.Transparent);
            }
        }
    }
}