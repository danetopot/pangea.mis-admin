﻿using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Graphics.Drawables.Shapes;
using Android.Support.V4.Content;
using PANGEA.MOBILE.Controls;
using PANGEA.MOBILE.Droid.Renderers;
using System;
using System.ComponentModel;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(StandardDateChooser), typeof(StandardDateChooserRenderer))]

namespace PANGEA.MOBILE.Droid.Renderers
{
    public class StandardDateChooserRenderer : DatePickerRenderer
    {
        public StandardDateChooser ElementV2 => Element as StandardDateChooser;

        public StandardDateChooserRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null)
            {
                var nativeEditText = (global::Android.Widget.EditText)Control;
                var shape = new ShapeDrawable(new Android.Graphics.Drawables.Shapes.RectShape());
                shape.Paint.Color = Xamarin.Forms.Color.White.ToAndroid();
                shape.Paint.SetStyle(Paint.Style.Stroke);
                nativeEditText.Background = shape;

               
                if (!string.IsNullOrWhiteSpace(ElementV2.Placeholder))
                {
                    Control.Text = ElementV2.Placeholder;
                }


                this.Control.TextChanged += (sender, arg) => {
                    var selectedDate = arg.Text.ToString();
                    if (selectedDate == ElementV2.Placeholder)
                    {
                        Control.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    }
                };
            }
        }
    }
}