﻿using PANGEA.MOBILE.Interface;
using SQLite;
using System.IO;
using Xamarin.Forms;

[assembly: Dependency(typeof(PANGEA.MOBILE.Droid.Helpers.SQLite))]

namespace PANGEA.MOBILE.Droid.Helpers
{
    public class SQLite : ISQLite
    {
        public SQLiteConnection GetConnection()
        {
            string sqliteFilename = "PANGEAMobiv2021.db3";
            string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string path = Path.Combine(documentsPath, sqliteFilename);
            return new SQLiteConnection(path);
        }
    }
}