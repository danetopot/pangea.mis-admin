﻿using PANGEA.MOBILE.Interface;
using System.IO;
using Xamarin.Forms;

[assembly: Dependency(typeof(PANGEA.MOBILE.Droid.Helpers.FileHelper))]

namespace PANGEA.MOBILE.Droid.Helpers
{
    public class FileHelper : IFileHelper
    {
        public string GetLocalFilePath(string filename)
        {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            return Path.Combine(path, filename);
        }
    }
}